# Automatic Short Answer Grading in an Online Quiz System

This thesis researches Automatic Short Answer Grading (ASAG) with the goal
of building an automatic grading module for short-text student responses to quiz
questions for an online Learning Management System (LMS) called Stembord.
The analyses and research inform the design and implementation of the module
which uses supervised training and a multi-layer perceptron model to grade short-
answers. To complete the research and development process, this module is then
integrated as an external API service and called by the Stembord application to
perform ASAG.

## Information

1. **Author**: John Faucett
2. **Supervisor**: Prof. Dr. Jörg Lohscheller

## Final Version

The final version of the thesis is in the root directory. It was compiled and rendered on the 6th of January 2019 and has not and will not be updated after that date (since the printed version was also submitted on that day).

Final Version: [masters-thesis-final-06.01.2019.pdf](./masters-thesis-final-06.01.2019.pdf)

## Linux Installation Details

To install the required libraries on Linux run:

```bash
sudo apt-get install texlive-full
sudo apt-get install aspell
```

You will also need a version of python >= 3.0 on your computer.

To compile the thesis into a PDF file run:

```bash
pdflatex vorlage.tex
```

## Mac Installation Details

Ensure you have [homebrew](https://brew.sh/index_de) installed, then run:

```bash
brew cask install mactex
```
