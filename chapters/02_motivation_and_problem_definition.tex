\chapter[Motivation and Problem Definition]{Motivation and Problem Definition} \label{Motivation_and_Problem_Definition_Chapter}


\begin{quotation}
``I learned very early the difference between knowing the name of something and knowing something.''

\rightline{{\rm --- Richard Feynman}}
\end{quotation}

Up until this point important background information about Automatic Short Answer Grading, Learning Management Systems and particularly Stembord has been expounded upon in order to provide a clear backdrop for understanding the problem definition and key motivations presented in this chapter. Elucidating the problem of ASAG, informing and developing the problem definition and clearly specifying the motivation for tackling this problem are the main subjects of this chapter.

\section{Problem Definition}

Short-answer question items are the core components of ASAG and consist of three key parts: a question prompt, one or more model answers written by teachers and a student response. These three components are demonstrated in an example in Fig. \ref{prob_def_threeComponentsOfASAG}.

\begin{figure}
	\centering
		\includegraphics[scale=0.54]{images/02_motivation_and_problem_definition/Text_Example_Question_with_Model_Answer.png}
	\caption[Core Components of ASAG]{Example showing the core components of short-answer question items: the prompt, teacher created model answer and student response.}
	\label{prob_def_threeComponentsOfASAG}
\end{figure}

More formally, for the purposes of this thesis, the problem of Automatic Short Answer Grading is defined as follows:

\begin{definition}
Given a set $M$ of reference short-text responses such that $\lvert M \rvert \geq 1$ and a short-text student response $S$, develop a function $f(M,S)$ such that $0 \leq f(M,S) \leq 1$. Where $f(M,S) = 1$ means $S$ is semantically identical to $M$ and $f(M,S) = 0$ means $S$ is completely unrelated to $M$. Values ranging between $0$ and $1$ represent a gradient of increasing shared meaning between $S$ and $M$ as $f(M,S)$ approaches $1$.
\end{definition}

Developing the function $f(M,S)$ is the main goal of this thesis. Concrete examples of the three components given differing student responses and consequent scores are shown in Fig. \ref{prob_def_asagScoreAssignment}.

\begin{figure}
	\centering
		\includegraphics[scale=0.54]{images/02_motivation_and_problem_definition/ASAG_Scoring_Problem_Definition2.png}
	\caption[ASAG function assigned scores]{Demostration of how the function $f(M,S)$ should work. The three possible scoring situations for student responses are shown. \textbf{A} shows a correct student response which consequently receives a perfect score ($1.0$). \textbf{B} shows a false answer which receives a failing score ($0.0$). And \textbf{C} shows a student response that is only partially correct. It is consequently assigned a score somehwere in-between ($0.5$).}
	\label{prob_def_asagScoreAssignment}
\end{figure}

The reason the range of the function $f(M,S)$ is $[0,1]$, is that any output of such a function can easily be scaled to any grade value. For instance, if a teacher assigns 20 points to a short-answer question and $f(M,S)$ calculates a similarity score of $0.7$ between model-answer $M$ and student-response $S$, then the student can straightforwardly receive $14$ points ($0.7\cdot 20 = 14$). If the original point total were $10$ or $5$, the score would be just as simple to calculate, except that for $5$ some rounding would need to take place if the desired output score is an integer value, which is usually the case.

There are many other ways to define the problem of assigning a score to a short text response submitted by a student. Indeed, in some of the experiments in this thesis the set $M$ is modified to be, for instance, a set of patterns written by teachers. Nonetheless, the structure of the problem stays the same and the range of its output on a continuous scale remains in essence equivalent. This is not to say this is the only correct approach or that other comparably effective approaches are not possible. This definition principally serves merely as a formal grounding for understanding the problem and the main task undertaken in this thesis.

\section{Motivation}

Why is developing such a function $f(M,S)$ useful and what holes in the current knowledge landscape does such research help to fill? Some of the answers to this question have been variously hinted upon in previous sections, however, the purpose of this section is to concretize those claims. Broadly speaking, the motivation for this research comes from two areas. The first, is the field of ASAG itself in which many open questions still remain, and the second comes from Stembord which in some ways sheds new light on old problems and unearths novel motivational questions and constraints upon the research endeavour. 

\subsection{Motivation from ASAG}

Much research on ASAG has hitherto been conducted analyzing specific techniques, whether supervised or unsupervised, with the explicit goal of improving correlations to human graders \cite{burrows_gurevych_stein_2014}. This means there is a lot of work on technical and statistical approaches to automatic grading of short-text responses, and this is certainly a positive attribute of the field. However, the author has not been able to find detailed analyses on the structure and topology of short-text responses in the context of automatic grading and even less on the relationships between \textit{(model-answer, student-response)} pairs. There could be many reasons for this, first and foremost, inadequacy and failures on the part of the author in his process of literary research. Another possibility is that these structural analyses have been conducted but either there is not much information which can be gleened from them or such information is so rudimentary that it is implicitly assumed in most published papers and ASAG researchers simply know their collegues already have this basic topological knowledge and write with an implicit assumption of this basis. Whatever the causes, one reason for carrying out this research is to answer the following questions:

\begin{enumerate}
\item What are typical structures that emerge in short-text responses during Short Answer Grading, particulary those relevant for automation of the grading process?
\item How do structures within short-text responses influence a human grader's behaviour?
\item Are their systematic relationships between model answers and student responses and if so what are they?
\item Are their systematic mistakes students tend to make when responding and if so what are they?
\item Can these structures, relationships, mistakes and grading behaviors be ordered in structured taxonomies to provide additional insight?
\end{enumerate}

Knowing the answers to - or having preliminary insights into - any of these questions could provide useful information for further research as well as new ideas for current technical and statistical approaches to ASAG, not to mention serve as a helpful resource for discussing common short-answer response patterns and behaviours.

But this is certainly not the end. Most research until now has focused either on techniques or on isolated systems with little if any afterthought about how the ASAG solution can be integrated into a full LMS or learning context. This is not a criticism of the current research, indeed, isolating problems and testing specific hypotheses in a controlled and manageable setting is paramount to making progress. It is only mentioned here, because it is a unique perspective offered by this thesis, since it looks at ASAG from a more holistic vantage point by building a module and integrating that module into the Stembord LMS. This holistic view provides some additional motivations and angles from which to inspect the problem.

\subsection{Motivation from Stembord}

In the introductory chapter, the needs of teachers in the context of Stembord's current architecture were explained. Chief among these is that the effort required to construct learning objects be kept to a minimum. Therefore, an informed research should focus on discovering effective ASAG techniques which are user friendly and do not require large amounts of technical expertise or laborious and time consuming efforts on the part of teachers. Most approaches which meet these requirements are largely unsupervised in nature. Therefore, the majority of this thesis focuses on unsupervised short answer grading techniques and could be viewed as attempting to answer the question: what trade-offs exist among various unsupervised ASAG techniques?

Also, since Stembord coexist in a market shared by many other LMSs, it is important to have some form of a baseline for assessing the effectiveness of a standard approach to offering ASAG within an LMS, insofar as such an approach exists.

Finally, because of the multilingual goals of Stembord, a major question is which techniques are applicable and effective in multiple languages and to what degree. Answering this question is important for an ASAG module being developed in the context of a multilingual software application but often irrelevant in the research literature. Even so, it may provide for unique insights.

These motivations, contributed by and arising from Stembord, are summarized below:

\begin{enumerate}
\item What are trade-offs among (unsupervised) ASAG techniques?
\item What is the effectiveness of a standard ASAG solution within another LMS on the market?
\item How do ASAG methods compare when it is necessary that they function across multiple languages?
\end{enumerate}

\subsection{Definition Specific Motivations}

Finally, of the main research questions mentioned above, subproblems within the version of ASAG given in this chapter's definition proffer up their own motivational questions, the most pertinent of which are:

\begin{enumerate}
\item How does increasing the number of model answers $M$ affect the ability of an ASAG solution?
\item What is the best ASAG method for extracting meaning from the words of a sentence?
\item How do knowledge based measures of word meaning using \textit{WordNet} compare to word embedding based measures and what are the key differences and trade-offs?
\item What is the best unsupervised i.e. algorithmic approach to ASAG that does not rely on machine learning?
\item What machine learning model is best for supervised ASAG using discoveries made in this thesis?
\end{enumerate}

This represents a comprehensive list of core questions which are explored in the coming pages in this thesis, however, they are all derived from the main guiding force which posited is: how can one design a system to automatically grade short-text responses, build this module in software and integrate it into Stembord?

