\section{Statistical Evaluation Measures}

This section details statistical techniques applied for evaluating experiment results as well as the reasoning behind the particular choice of technique in the context of ASAG.

In the dataset there are gold standard scores which have been assigned to each student response and in the experiments the developed models attempt to predict these scores given each student response item. This means that after running an unsupervised approach, there is an array of size $n$, ($n = 2442$ in the case of v2.0 of the dataset) containing tuples of the form $(s_{true}, s_{predicted})$ for each student response in the dataset.
This does not hold for the final experiment, which is unsupervised and uses five-fold cross validation to score the results.

The following subsections outline various approaches to understanding what the list of score tuples can tell about the underlying model used to run the experiment.

\subsection{Predictions: Accuracy, Precision, Recall and F1}

In order to conceptualize the various metrics in this section it is helpful to have a reference confusion matrix of a binomial experiment, for instance, an ASAG problem where the model predicts simply \textit{pass} or \textit{fail}. This confusion matrix for this is shown in Fig. \ref{stats_accuracy_scores}.

\begin{figure}
	\centering
		\includegraphics[scale=1]{images/04_material_and_method/precision_accuracy_recall_confmatrix.png}
	\caption{A confusion matrix demonstrating a model's predicted values (Predicted Class) vs. the true values (Actual Class).}
	\label{stats_accuracy_scores}
\end{figure}

The \textbf{True Positives (TP)} are correctly predicted positive values. For instance, when the model runs, these are values for which the tuple $(s_{true}, s_{predicted})$ is scored as $(pass, pass)$. Accordingly, \textbf{True Negatives (TN)} are correctly predicted negative values, having tuples of the form $(fail, fail)$. \textbf{False Negatives (FN)} are values for which the gold standard value is positive i.e. \textit{pass} but the model predicted \textit{fail}, these have the tuple form $(pass, fail)$. Finally, there are the \textbf{False Positives (FP)} which make up those instances where the true value was negative yet the model predicted positive i.e. $(fail, pass)$.

\paragraph{Accuracy}

Now, having an understanding of the possible prediction scenarios the model can make, the evaluation metrics can be explained. The first metric anyone would likely think of is accuracy, and for good reason, it is a useful metric that is easily interpreted. Accuracy is simply the ratio between the number of correctly predicted observations to the total observations. Or to put it in the terminology of the previous paragraph, it is the ratio of True Positives (TP) plus True Negatives (TN) to all observations, shown as follows:

\begin{equation}
Accuracy = \frac{TP + TN}{TP + FP + TN + FN}
\label{stats_accuracyScore}
\end{equation}

Accuracy describes how well the model does at predicting correct values, however, it is not always the best metric for the problem. Imagine a model which predicts cancer susceptibility and recommends tissue sampling based off of this. The model could have $90\%$ accuracy, however, it is still difficult to evaluate this measure, because the $10\%$ error could be either a FP, meaning the model predicted cancer and there was none, which is likely acceptable, or much more gravely, this $10\%$ could be made up predominately of FN values, where the model predicts benign tissue and the patient has cancer - a much more deleterious scenario.

Tackling these issues are the next measurements, which are also important in the evaluation of ASAG methods in this paper for reasons which are given as the measurements are described.

\paragraph{Precision}

Precision is the ratio of true predicted positive values to total predicted positives.

\begin{equation}
Precision = \frac{TP}{TP + FP}
\label{stats_precisionScore}
\end{equation}

Precision answers the question: of all the students where the model predicted positive i.e. \textit{pass} how many actually passed? It is a useful measure when the FP costs are high, for instance, in spam detection identifying an email as spam when the email is benign can incur high costs i.e. the user may not be able to receive an important email. In the context of ASAG, low precision values means that the model is scoring student responses highly which in the real world should actually receive lower scores.

Since most of the current use-cases for Stembord are for low-stakes assessment, i.e. quizzes, homework items and tutorials, precision, though giving more information about a model is not as important to this paper's ASAG task as recall.

\paragraph{Recall}

Recall is the ratio of correctly predicted positive values to all true positive values.

\begin{equation}
Recall = \frac{TP}{TP + FN}
\label{stats_recallScore}
\end{equation}

Recall answers the question: of all the students that passed the question, how many did the model actually recognize? This is an important metric for this thesis's research approach. The models should strive for high recalls, since it can be very frustrating from a user perspective if a student knows the answer, responds to the item correctly, and yet the model assigns a low score for that student. This situation needs to be minimized and recall is the measurement which can guide understanding of this attribute.

\paragraph{F1 Score}

Finally, there is the F1 score which is the harmonic mean of precision and recall, which means it takes information from both into account. 

\begin{equation}
F1 = 2\cdot\frac{precision\cdot recall}{precision + recall}
\label{stats_f1Score}
\end{equation}

Because the datasets being used in this paper are imbalanced, the F1 measure, which is a stricter measure and somewhat controls for class imbalance by weighting the average - although mainly when there are many TN values - is considered a better metric than accuracy for analyzing the ASAG problem and is presented instead of accuracy in most experiment results.

\subsection{Predicted Error Differences: Mean Absolute Error (MAE)}

Returning the tuples of the form $(s_{true}, s_{predicted})$, one can also look at how far off, on average, each prediction, made by a model, is from the true value. Because the predicted scores and gold standard scores are discretized numeric values this makes sense. It is also, in general, an important piece of information when evaluating models. For instance, when predicting scores on the scale from $0$ to $5$, it makes a significant difference whether the model is off on average by $1$ point, $0.1$ points or $2$ points. 

The Mean Absolute Error (MAE) metric can indicate this important differences between various proposed automatic scoring solutions. The MAE iterates through a list of tuples taking the absolute value of the difference between the predicted score and the actual score. It then takes the average of all these differences. The equation is
%
\begin{equation}
MAE = \frac{1}{n}\sum_{j=1}^{n}\lvert y_{j} - \hat{y}_{j}\rvert
\label{MatMeth_meanAbsoluteError}
\end{equation}
%
where $n$ is the length of the list containing the $(s_{true}, s_{predicted})$ tuples and $y_{j}$ is the $j$-th true score and $\hat{y}_{j}$ is the $j$-th predicted score.

\subsection{Prediction Agreement: Cohen's Kappa and Pearson's R}

Pearson's R and Cohen's Kappa are ubiquitously scattered throughout the ASAG literature, the reasons for why both of these measurements are highly touted are given in the following sections on each method. 

\paragraph{Pearson Correlation Coefficient (PCC or Pearson's R)}

Pearson's R is defined as the ratio of the covariance of two variables $X$ and $Y$ to the product of their standard deviations. 

\begin{equation}
PCC = \frac{cov(X,Y)}{\sigma_{X}\cdot\sigma_{Y}}
\label{stats_pearsonsR}
\end{equation}

It measures the linear correlation of two variables and ranges from $-1$ to $1$, where $-1$ indicates that the variables are inversely correlated i.e. when the true score is $5$ the model predicts $0$ and when the true score is $0$ the model predicts $5$. Whereas a correlation of $1$ means the variables are directly correlated i.e. the prediction tuples look like $(0,0),(1,1), (3,3), (5,5)$. A PCC of around zero means that the variables have no correlation with one another.

This translates to understanding an ASAG model by looking at how highly the predicted scores and actual scores are correlated, thus superior models will have PCC values which move away from $0$ and approach $1$, whereas poorer models will hover around zero.

\paragraph{Cohen's Kappa}

As discussed at various points in this thesis, grades are highly subjective and the grades of any two human graders will only coincide a certain percentage of the time \cite{kohn_1999}. Thus, ultimately, a comparison needs to be made between the predictions of the computer model and those of a real human judge, this is where inter-rater agreement comes into play. The core question is: given a certain level of agreement between a computer model and a human judge as it pertains to the score of a set of response items, how significant is that agreement? Further, how much does that agreement differ from what would be expected from two human judges or random chance? These are the questions that Cohen's Kappa attempts to address. It calculates a value that expresses the level of agreement between two annotators (in this case a human judge and a computer model) on a classification problem controlling for random probability of agreement \cite{cohen_1960}. It is defined as
%
\begin{equation}
\kappa = (p_{o} - p_{e})/(1 - p_{e})
\label{stats_cohensKappa}
\end{equation}
%
where $p_{o}$ is the observed agreement ratio (the accuracy) and $p_{e}$ is the expected agreement when both annotators assign labels randomly \cite{cohen_1960}.

In a large case-study for C-Rater, the kappa score agreement for c-rater/human on short text responses was $.77$ \cite{leacock_2004}. According to Fleiss, this represents an excellent level agreement, Fleiss further states ``values greater than 0.75 or so may be taken to represent excellent agreement beyond chance, values below 0.40 or so may be taken to represent poor agreement beyond chance, and values between 0.40 and 0.75 may be taken to represent fair to good agreement beyond chance'' \cite{fleiss_2003}. These values roughly coincide with those of the upper bounds calculations given previously for a well-performing model.