\section{Framing Performance} \label{Upper_and_Lower_Bounds}

In order to interpret the results of the experiments, it is important to have a frame of reference, a yardstick as it were, with which to inspect and reflect upon the results. This frame of reference needs to be defined for lower and upper boundaries, that is, for the worst performance that can be expected, typically, referred to as a baseline, and for the best performance, which in the case of ASAG is human level performance.

\subsection{Lower Bounds}

One measure of poor performance a grader could give would be to look at a student response and then randomly pick a grade and assign it to that response. This is the monkey banging on a keyboard approach and randomly assigning a score to a student response. Fig. \ref{MatMeth_lowerBoundsAccuracy}  shows the results of running the final dataset through a random scorer; these results provide a sanity-check and worst-case baseline for evaluating experiment results.

\begin{figure}
	\centering
		\includegraphics[scale=0.42]{images/04_material_and_method/lower_bounds_accuracy_measures.png}
	\caption{A monkey randomly picking grades and assigning them to student responses would have accuracy measurements of around $17\%$ for continuous grading and $50\%$ for pass/fail grading on the dataset.}.
	\label{MatMeth_lowerBoundsAccuracy}
\end{figure}

In addition to the accuracy statistics in Fig. \ref{MatMeth_lowerBoundsAccuracy}, both Pearson's R and Cohen's Kappa are zero as is to be expected for random sampling and the mean absolute error is $1.63$ and $0.29$ for the regular grading and pass/fail grading respectively.

\subsection{Upper Bounds}

The upper bounds represents the level at which a well-performing model could be reasonably expected to automatically score student responses on this dataset. The key attributes of such an ideal solution are that it would be roughly equivalent to a human grader. Further, this grader should be a domain expert on the topic as well as unbiased, that is, the human expert must not know or be capable of associating students with their respective scores. This last attribute mitigates against some of the problems with grading as already discussed in the introductory chapter. To calculate the statistics for such a model, it seems reasonable to choose the author's own hand-graded performance measured against the gold standard scores to determine this upper boundary. The author of this thesis is a professional software engineer and thus a domain expert for topics in introductory computer science classes, having even taught such a class once himself. Additionally, he has no way of being biased against the students in the dataset since he does not know them or know their names or anything else about them. So, in order to calculate the measurements for an upper performance boundary, the author's hand-graded scores were compared to the gold standard scores and these upper limit measurements were calculated for the 6-class score assignment problem, 3-class assignments as well as pass/fail grading. The results are shown in Tab. \ref{MatMeth_upperBounds}.

\begin{table}
\begin{small}
\centering
    \begin{tabular}{ | l | l | l | l |}
    \hline
    \textbf{Measure} & \textbf{6-Class Grading} & \textbf{3-Class Grading} & \textbf{2-Class Grading}\\
    \hline
    \textbf{Accuracy} & $47.21\%$ & $72.69\%$ & $81.84\%$ \\
    \hline
    \textbf{Precision} & $57.09\%$ & $78.32\%$ & $82.63\%$ \\
	\hline
    \textbf{Recall} & $47.21\%$ & $72.69\%$ & $81.84\%$ \\
    \hline
    \textbf{F1} & $45.95\%$ & $73.85\%$ & $78.06\%$ \\
	\hline
    \textbf{R} & $0.70$ & $0.65$ & $0.42$ \\
	\hline
    \textbf{Kappa} & $0.26$ & $0.44$ & $0.35$ \\
    \hline
    \end{tabular}
    \caption{Comparing the author's scores vs. the other human graders' scores on the dataset. Shows key measurements on grading for 6-Classes, 3-Classes (\textit{Wrong}, \textit{Partially\_Correct}, \textit{Correct}) and 2-Classes (\textit{Pass},\textit{Fail}).}
    \label{MatMeth_upperBounds}
\centering
\end{small}
\end{table}

Tab. \ref{MatMeth_upperBounds} shows the upper bounds of expected performance from a good model and represents values which are considered at the upper end of potential performance for any given model using this dataset.
