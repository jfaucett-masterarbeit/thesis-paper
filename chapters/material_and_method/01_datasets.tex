\section{Datasets} \label{Section_Datasets}

\subsection{Mohler Dataset Version 1.0}

The Mohler dataset is used as the basis for running all the experiments. It was released publicly over two years in two separate versions and publications. The first version, version 1.0, is only used in the first experiment. It is the public dataset which Mohler and Mihalcea published along with their paper, \textit{``Text-to-text Semantic Similarity for Automatic Short Answer Grading''} \cite{Mohler_2009}. Version 1.0 (v1.0) contains $21$ short-answer question prompts of which $17$ are also present in version 2.0 (v2.0). The reason v1.0 was chosen over 2.0 for the first experiment is that the former only contains $21$ unique model answers and the experiment design calls for a high degree of teacher effort creating model answers, therefore, the lower question count of $21$ provides lower time requirements for experiment participants.

Both datasets contain questions from an introductory computer science course for undergraduate students offered at the University of North Texas. Assignments were taken by students using the WebCT online learning environment and the student responses to the short-answer questions were collected via this online tool. For version 1.0, there were 30 students enrolled in the class who also took each assignment, 3 assignments in total were given with 7 questions per assignment for a grand total of ($30 \times 3 \times 7 = 630$) student responses \cite{Mohler_2009}.

The student responses were graded independently by two human judges on a scale from $0$ (false or incorrect) to $5$ (completely correct). Both human judges were graduate students in the computer science department. These two scores were then averaged to create the final score for each student response item in the dataset. The inter-annotator correlation on the data set was $.6443$ using Pearson's correlation coefficient \cite{Mohler_2009}.

The dataset is publicy available in raw text format.\footnote{At the time of this writing, the raw datasets can be downloaded from Dr. Mihalcea's website at \url{https://web.eecs.umich.edu/\~mihalcea/downloads.html\#saga}} This raw text data was downloaded, parsed and converted into a comma-separated values (CSV) file containing the structure shown in Tab. \ref{MatMeth_v1csvTable}.

\begin{table}
\begin{small}
\centering
    \begin{tabular}{ | c | c | c | p{3cm} | p{3.5cm} | p{3.5cm} | c |}
    \hline
    \textbf{AID} & \textbf{QID} & \textbf{SID} & \textbf{Prompt} & \textbf{Model Answer} & \textbf{Student Answer} & \textbf{Score} \\
	\hline
	1 & 1 & 6 &  What does a function… & The name of the func… & It includes the name of the prog… & 4.5 \\
	\hline
	1 & 1 & 5 &  What does a function… & The name of the func… & It has specific information abou… & 3.0 \\
	\hline
	1 & 1 & 8 &  What does a function… & The name of the func… & The function signature includes … & 5.0 \\
	\hline
	1 & 1 & 3 &  What does a function… & The name of the func… & A function signature consists of… & 4.5 \\
	\hline
	1 & 1 & 4 &  What does a function… & The name of the func… & It includes the name of the func… & 5.0 \\
	\hline
    \end{tabular}
    \caption{Structure of the CSV File for version 1.0 of the Mohler Dataset. Shows the columns: \textbf{AID} -  assignment ID, \textbf{QID} -  question ID, \textbf{SID} -  student ID, \textbf{Prompt} -  question prompt, \textbf{Model Answer} -  the gold standard answer written by the educator, \textbf{Student Response} -  the student's answer to the question and \textbf{Score} -  the averaged score of two human graders.}
    \label{MatMeth_v1csvTable}
\centering
\end{small}
\end{table}

The histogram and ECDF distribution of the student scores can be seen in Fig. \ref{MatMeth_fig_v1_hist_ecdf}, which visually displays the distribution of scores across v1.0 of the dataset. This dataset is skewed in favor of high scores, in fact, any model which simply predicted a perfect score of 5 would be correct $45.4\%$ of the time. This aspect of the dataset can be seen more distinctly in the ECDF (Empirical Cumulative Distribution Function) shown in the right of Fig. \ref{MatMeth_fig_v1_hist_ecdf}, which demonstrates that less than $40\%$ of the responses make up the scores from $0$ to $4$, another way of conceptualizing this is that of $11$ possible grades (0-5 inclusive at steps of 0.5), $3$ grades ($4$, $4.5$, and $5$) make up over $60\%$ of the dataset's responses, heavily skewing the dataset towards higher grades. However, this aspect of the data is also demonstrated in version 2.0 of the Mohler dataset and, as the author of this paper proposes, can be explained to some extent by the manner in which graders approach the short-answer grading task.

\begin{figure}
	\centering
		\includegraphics[scale=0.33]{images/04_material_and_method/dataset_v1_hist_ecdf2.png}
	\caption{The histogram and ECDF distribution of averaged student scores across v1.0 of the dataset.}
	\label{MatMeth_fig_v1_hist_ecdf}
\end{figure}


However, the skewed distribution provides an additional reason to emphasize the development of a system which stores student responses and builds a quality dataset going forward. It is also one of the reasons specific examples from each experiment are examined. This is done in order to elucidate problems or features and to develop intuition behind a method which may not be immediately visible by simply comparing correlation coefficients, MSEs or various other statistical and technical measurements.

Finally, because Stembord emphasizes self-study courses for its users, it is important to evaluate methods for ASAG which either pass or fail a student response. This is because self-study courses do not have teachers and only need to detect whether a student has entered a correct response or not and provide the student with some degree of feedback. To build this dataset, all scores below a threshold of $2.5$ were assigned a failing grade of $0$ and scores such that $score \geq 2.5$ where assigned a success grade of $1.0$. The threshold value of $2.5$ was decided upon based on a detailed analysis of the underlying datasets, especially the augmented Mohler version 2.0 dataset. Fig. \ref{MatMeth_v1_binary_class} shows the histogram of the dataset after converting the scores to the binary classes.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/04_material_and_method/v1_en_pf_hist_bw.png}
	\caption{The histogram distribution of the fail (0) and pass (1) scores across v1.0 of the dataset.}
	\label{MatMeth_v1_binary_class}
\end{figure}

This concludes the description of the first version of the dataset. The key takeaways are that the scores are skewed to the left, with most student responses receiving a passing and high grade and that a threshold of $2.5$ is used to divide human grader scores into failing and passing grades. The reasoning for these choices is discussed in the next section along with a detailed analyses of v2.0 of the dataset.

\subsection{Mohler Dataset Version 2.0} \label{Subsection_Datasets_v2}

In 2011, in their paper \textit{``Learning to Grade Short Answer Questions using Semantic Similarity Measures and Dependency Graph Alignments''}, Mohler et al. released a second extended version of the original dataset. \cite{mohler_bunescu_mihalcea_2011}. This dataset expands the student submitted responses to cover 80 questions across ten assignments and two examinations in a course on data structures at the same university. Although 31 students were enrolled in the class, some did not answer all the question items which results in the final dataset containing only 2442 items instead of the expected $80 \times 31 = 2480$. Just as in the original dataset, version 2.0 uses the average scores given by two human judges, both teaching assistants, to define the gold standard labels \cite{mohler_bunescu_mihalcea_2011}.

Although this dataset is almost four times larger than version 1.0, it still exemplifies the same left skewedness over the distribution of the scores. These distributional properties of the raw dataset are illustrated in Fig. \ref{MatMeth_v2_hist_and_ecdf}.

\begin{figure}
	\centering
		\includegraphics[scale=0.33]{images/04_material_and_method/dataset_v2_hist_ecdf.png}
	\caption{The distribution of averaged student scores across v2.0 of the Mohler dataset shown by the Histogram and ECDF respectively.}
	\label{MatMeth_v2_hist_and_ecdf}
\end{figure}

Besides having more questions and response pairs, version 2.0 of the Mohler dataset is, structurally speaking, the same as version 1.0, that is, it has the same columns and attributes.

\subsection{Annotation and Extension of Mohler Dataset Version 2.0} \label{Subsection_Dataset_Annotation}

The extended, adapted and annotated version of the second Mohler dataset is used in all the experiments except those which use Mohler v1.0 and for this reason a detailed analysis of its structure as well as the corrective measures which were undertaken in order to ensure its robustness are disclosed in the following paragraphs.

\subsubsection{Discoveries made by Hand-Grading the Dataset from Scratch}

First, in order to find answers to motivational questions about emergent structures in short-text responses and how these structures can influence human grading behavior, the author hand-graded all of the 2442 student responses to gain an intuitive understanding of the grading process. The coefficient of these scores is $0.74$, that is, the author's grades correlate $0.74$ with the averaged scores of the two other human graders. After completing the original grading process, the author randomly selected $n=100$ questions and graded them a second time in order to see how well the author's second attempt at grading correlated with his first one; this value was $0.80$. Thus, in this small $n=1$ task, a disagreement on assigned scores was shown to exist between the author at time $t_{1}$ and the same individual at a later time $t_{2}$. These results are consistent with some of the degrading arguments put forth by Kohn and discussed in the opening chapter \cite{kohn_1999}.

\paragraph{Grader Behavior and Student Response Classes}

Overall, the process of grading proved to be difficult and time consuming. However, some parts of the process were easy. For instance, student responses which were highly similar to the model answer and correct were easy to assign a consistently high grade. Also those responses which were completely unrelated to the topic or where the student had not even written a response were easy to assign a $0$ or bad score. These situations can be thought of as representing low effort extremes on the \textit{(model-answer, student-response)} relationship landscape and these edge range decisions were simple. However, as soon as student answers began to deviate from the two patterns, difficulties emerged. One of the hardest problems to arise, was determining a consistent numeric value to give to a student's short-text answer, since in many cases student responses were quite similar to one another, yet minor differences caused significantly different grades. This led the author to quickly develop a classification system, whereby student responses could delegated to classes and then consistently graded according to their classification. 

The classes were assigned labels which were derived over the process of the grading experience itself and were called: \textit{wrong\_unrelated}, \textit{wrong\_related}, \textit{partially\_correct}, \textit{correct\_indirect} and \textit{correct\_complete}. A description of each of these labels can be found in Tab. \ref{MatMeth_gradingClasses}. The main goal behind this classification effort was to improve the author's grading consistency. 

\begin{table}
\begin{small}
\centering
    \begin{tabular}{ | l | p{6cm} | }
    \hline
    \textbf{Label} & \textbf{Description} \\
    \hline
	\textit{wrong\_unrelated (WU)} & Student either has no response or his response does not relate at all to the question. \\
    \hline
	\textit{wrong\_related (WR)} & Student's response is on the correct topic just completely wrong. \\
    \hline
	\textit{partially\_correct (PC)} & The response is on topic but not fully correct for any number of reasons. \\
    \hline
	\textit{correct\_indirect (CI)} & Response is fully correct but requires significant inference and external knowledge to infer this fact. \\
    \hline
	\textit{correct\_complete (CC)} & Response is fully correct, well worded, and easily interpretable. \\
    \hline
    \end{tabular}
    \caption{Custom labels applied to student responses in order to differentiate them into classes and aid in consistent scoring.}
    \label{MatMeth_gradingClasses}
\centering
\end{small}
\end{table}

Once the entire dataset of student responses was assigned these class labels, the averaged score distributions from the human graders as well as the author when grouped by the classes could be cross-compared to see what patterns emerged. Fig. \ref{MatMeth_scoreDist_with_5labels} shows the compared results of this grouping.

\begin{figure}
	\centering
		\includegraphics[scale=0.26]{images/04_material_and_method/score_distribution_within_custom_5labels.png}
	\caption{The green dots are the mean, the red lines the median. Notice how the author's scores seperate and differentiate the classes and remove much of the overlapping inconsistency. Almost all scores classified as wrong (WU or WR) have a score $\leq 2.5$. This is where the failure threshold is derived for false answers in binary classification on the dataset. Also an upper threshold of $4$ looks like a good choice as a low cut-off for \textit{correct} scores.}
	\label{MatMeth_scoreDist_with_5labels}
\end{figure}

As can be seen clearly from the author's scores shown Fig. \ref{MatMeth_scoreDist_with_5labels}, it makes sense to combine \textit{correct\_indirect} and \textit{correct\_complete} labels into one \textit{correct} label since their score distributions are so similar. The same is also done for \textit{wrong\_unrelated} and \textit{wrong\_related} since although their score distributions are different, their mean and median scores are below $1$ which approximately as good as failing in this dataset (for instance, a student might receive a one if his answer is completely wrong, but he does write some correct information roughly related to the topic for which the grader grants a ``sympathy'' point). This results in three categories, which forms the foundation for three class classification. The resulting frequency counts per class can be seen in Fig. \ref{MatMeth_mergedLabels} for three classes and the binary classification problem. The same hand-grading analysis was used to divide the data along binary classification lines as well by using the discovered threshhold of score values below $2.5$ which as can be seen in Fig. \ref{MatMeth_scoreDist_with_5labels} are almost always labeled as \textit{wrong}.

\begin{figure}
	\centering
		\includegraphics[scale=0.30]{images/04_material_and_method/merged_labels_counts2.png}
	\caption{Distribution of student grades when grouping the data into a three class and binary classification problem based on the thresholds of $x < 2.5$ for \textit{wrong} responses and $x \geq 4$ for \textit{correct} responses.}
	\label{MatMeth_mergedLabels}
\end{figure}

\paragraph{The Grading Process and Score Distribution}

The score distribution, in the author's opinion, has two main causes, at least for these datasets. The first, is that most short-answer responses are on topic and indeed approximately correct, for example, if a question is asking about stacks the responses will tend to have something to do with stacks and not be completely unrelated (\textit{wrong\_unrelated} only makes up $20\%$ or 1 out of every 5 wrong answer responses). The second is that the grader has a model answer and measures a student response against this ideal. This process lends itself to a sort of grading that removes points as a student response differs more and more from the model answer. It is like a sculptor removing negative space from a block. The grader compares the response to the model answer and if everything lines up the student gets a perfect score, however, for errors this score is then reduced or chiseled away by some amount depending on the subjectively interpretted severity of the mistake. These two factors explain some of the skewedness in the score distribution, in the author's opinion. It would be very interesting to see if this pattern reamerges across other short-answer topics and domains across more diverse and larger datasets.


In summary, the manual work of scoring student responses helped to clear up many aspects of the dataset, which may or may not generalize to other ASAG problems. It was especially helpful in discovering an emergent pattern of three classes, which can be further combined into two classes (pass/fail) based on discovered cut-off threshold values. From the author's perspective, grading student responses is tedious and time consuming in all but the edge cases. Maintaining consistency across different student responses and the same student responses at different points in time are two of the most difficult problems. Additionally, one pattern emerged from the scores in the dataset, namely, that three classes can be used to ease grading and improve consistency, at least from the grader's perspective, of the students' responses. Finally, the coupling of on topic student responses and model-answer based grading was given as a possible explanation for the left skewedness of the datasets. 

\subsubsection{Student Response Structures} \label{Subsubsection_Extra_Annotations}

During the process of grading, open questions were at the forefront. Some of these were, for instance, questions such as what were internal structures and patterns that kept reappearing in student responses or how did certain structures influence grading behavior. In order to analyse and attempt to answer some of these questions, the author further annotated student responses within each of the label categories in an attempt to dissect why certain responses were demoted or received less than a perfect score and even in some cases why otherwise incorrect responses received some points at all. Ultimately, there were eleven labels which student responses could receive. These labels which were used to annotate student responses in the dataset are displayed in Tab. \ref{MatMeth_gradeReasonLabels} which shows the label, the description of the label, and the grading behavior that the author took for a student response receiving the particular label. Its also important to note that student responses could have zero or more labels associated with them.

\begin{table}
\centering
	\begin{small}
    \begin{tabular}[t]{ | l | >{\raggedright}p{5cm} | p{4cm} |}
    \hline
    \textbf{Label} & \textbf{Description} & \textbf{Grading Behavior} \\
    \hline
	\textbf{Missed Concept} & Student failed to get one or more important concepts & Knock off multiple or all points. \\
    \hline
	\textbf{Partially Missed Concept} & Student missed a part of a concept or idea. & Knock off one to a few points. \\
    \hline
	\textbf{Extra Info} & Student provided a significant amount of extra information which is factually correct but unnecessary. & Do nothing. Rarely grant a point or two if student otherwise missed everything. \\
    \hline
	\textbf{Lack of Knowledge} & Student answer is worded in such a way that it is obvious the student does not comprehend the material. & Knock off multiple points. \\
    \hline
	\textbf{Misinterpretation} & The student misread the question prompt or interpreted it in a way which was not intended leading to a completely false answer. & Give a few points if interpretation and response are both reasonable. \\
    \hline
	\textbf{Not Enough Info} & Student's answer could be correct but it doesn't provide enough description and clarity to be sure. & Knock off one to a few points. \\
    \hline
	\textbf{Misspelling} & Response contains one or more significant misspelling or grammatical errors. & Do nothing or knock off a point or two if the errors are extensive. \\
    \hline
	\textbf{Confused Wording} & Response is worded so poorly it is very difficult to ascertain its correctness. & Knock off a few to multiple points. \\
    \hline
	\textbf{Indirect Wording} & Response is correct but wording is significantly different than model response and possibly requires inferential knowledge by the reader. & Do nothing. \\
	\hline
	\textbf{No Response} & Student did not answer the question or made virtually no attempt at it. & Give zero points. \\
    \hline
	\textbf{False Assertion} & Student states something which is completely false. & Take off a few to multiple points depending on severity. \\
    \hline
    \end{tabular}
	\end{small}
    \caption{Annotated labels used to understand structural patterns in student responses and grading behavior.}
    \label{MatMeth_gradeReasonLabels}
\centering
\end{table}

One quite interesting structural aspect of the dataset that emerged after applying this annotation process, is that $54.55\%$ of the \textit{partially\_correct} student responses contain either the label \textit{Missed Concept} ($28.89\%$) or \textit{Partially Missed Concept} ($25.66\%$). This means over half of the most difficult to grade student responses contained structural evidence that a student had either fully or partially missed an important concept and that partial and full misses were roughly about as common. This number jumps up even higher to $61.3\%$ for student responses classified as \textit{wrong}. Thus, missing concepts whether partial or complete are by far the largest factors for determining the placement and scoring of a student response. This provides some indication for the importance of researching the ability to detect and determine  concept presence and absence in student responses.

Another interesting structural insight is that the \textit{False Assertion} label can be found in $27.7\%$ of \textit{wrong} student responses compared to $11.6\%$ of \textit{partially\_correct} responses and a mere $4.3\%$ of \textit{correct} responses. This appears to provide some evidence that making false assertions in a response has a more severe negative impact on grading than simply missing a concept or a part thereof.

Finally, , $30.4\%$ of \textit{partially\_correct} responses contained the \textit{Not Enough Info} label compared to only $6.3\%$ of \textit{wrong} responses. This indicates that almost 1 in 3 student responses do not provide enough information to clearly articulate to a human grader that they know and understand the piece of information requested in the question prompt and that when this occurs the response often receives partial credit instead of being categorically written off as false.

A full and detailed listing of the statistical distributions of each of the labels across the dataset can be found in the appendix in Tab. \ref{dataset_labels}. The author thinks this information is very useful for guiding future research and further study of short-answer responses.

\subsubsection{Dataset Pruning}

Finally, the author examined the lengths and relationships between the \textit{(model-answer, student-response)} pairs and determined there were, broadly speaking, two types. The first type, were questions which are typically associated with ASAG, that is \textit{(model-answer, student-response)} pairs of varying lengths all above four or five words. The second type, were questions having very short model answers containing just one or two words and often student responses just as short. These usually represented questions such as \textit{``What are the two main methods in a stack?''} which have a model answer such as \textit{``push and pop''}. Some had model answers as short as one or two words and many of these questions were very easy for students, contained almost all correct responses and, in the author's opinion, don't represent quality ASAG data. Therefore, it was decided to exclude these very short answer type of questions. From an implementation perspective, using another approach such as a Fill-in-the-Blank question type to model these types of question items in quizzes seems more appropriate. A visual example of the these pairs is shown in Fig. \ref{MatMeth_lengthComparisons}.

\begin{figure}
	\centering
		\includegraphics[scale=0.45]{images/04_material_and_method/length_comparisons.png}
	\caption{
		\textbf{(A)} The textual contents of the very short model answers. 
		\textbf{(B)} The location of the very short model answers in the scatter plot of median answer lengths; these answers are good candidates for a fill-in-the-blank question type.}
	\label{MatMeth_lengthComparisons}
\end{figure}


After removing these questions the dataset obtained its final size of $n=2010$. Finally, in order to assess the usefulness of having multiple model answers during the experiments, extra model answers in two forms where also added as columns to the dataset. The first were positive model answers, that is, model answers which simply had different phrasings or wordings but should otherwise serve as a quality model answer. In addition to this type, a negative model answer was also added. This answer contained prototypically bad answers or responses which might often occur but would be deemed wrong by the human grader.

\subsection{Translated Datasets}

Since evaluating the generalizability of ASAG methods across languages is one of the purposes of this research, the main dataset (version 2.0) was translated from the original English to both German and Spanish. This was done using the google translate API, but since the author of this paper also speaks Spanish and German, some minor corrections were made to individual translations in order to touch up mistakes made by the automatic translation process. However, not all of the translations were rigorously hand editing and this could certainly be a source of error. Fig. \ref{MatMeth_translated_Dataset_ToGerman} shows an excerpt from the dataset after translation into German.

\begin{table}
\centering
	\begin{small}
    \begin{tabular}{ | c | c | c | >{\raggedright}p{3cm} | >{\raggedright}p{3.5cm} | >{\raggedright}p{3.5cm} | c |}
    \hline
    \textbf{AID} & \textbf{QID} & \textbf{SID} & \textbf{Prompt} & \textbf{Model Answer} & \textbf{Student Answer} & \textbf{Score} \\
	\hline
	1 & 1 & 6 &  Was macht eine Funkt… & Der Name der Funktion… & Es enthält den Namen des Program… & 4.5 \\
	\hline
	1 & 1 & 5 &  Was macht eine Funkt… & Der Name der Funktion… & Es enthält die spezifischen Info… & 3.0 \\
	\hline
	1 & 1 & 8 &  Was macht eine Funkt… & Der Name der Funktion… & Die Funktionssignatur enthält de… & 5.0 \\
	\hline
    \end{tabular}
	\end{small}
    \caption{Example showing an excerpt from the CSV file translated into German for version 2.0 of the Mohler Dataset.}
    \label{MatMeth_translated_Dataset_ToGerman}
\centering
\end{table}

The German and Spanish language versions of the datasets are used in places throughout the experiments in order to asses how and to what degree a particular methodological approach to ASAG generalizes and can be applied to other languages.


\subsection{Other Datasets Considered}

The Mohler datasets are not the only datasets available for some form of short-answer evaluation, however, they are the best the author could find for the specific problem of Automatic Short Answer Grading as defined in this thesis i.e. for assigning a numeric value to a short answer response item.

The SemEval-2013 Task 7 challenge contains a more well-balanced dataset as well as reference answers and student responses, so on the surface it looks like it would be a more apt choice. Unfortunately, the judgments or gold standard labels in this case are classifications in 5 categories: \textit{Correct}, \textit{Partially\_correct\_incomplete}, \textit{Contradictory}, \textit{Irrelevant}, and \textit{Non\_domain} \cite{dzikovska_2013}. These categories, unlike those created in the process of hand-grading in this thesis, do not translate well to a system for assigning numeric scores to short-answer questions. For instance, what value should a \textit{Partially\_correct\_incomplete} item have? Should all \textit{Correct} labeled items receive a perfect score? Are all contradictory questions wrong or are some partially correct? Without being able to group and analyze class labels according to numerically assigned grades it is extremely difficult to determine how they should be interpreted from a grading standpoint. For this reason, the SemEval 2013 Task 7 dataset is not used.

Another dataset considered was the one provided by the Kaggle hosted competition ``Automated Student Assessment Prize, Phase Two – Short Answer Scoring.'' sponsored by The William and Flora Hewlett Foundation \cite{hp_foundation_sas}. This dataset contains human grades for short-answer response items, as well as being large (17,000 training items) and diverse, ranging over disciplines from English Language Arts to Science \cite{hp_foundation_sas}. It too appears initially to be a more than adequate dataset, however, on further inspection there are no reference answers for student responses so this dataset turns out to approach the problem of ASAG without any input from teachers and instead looks solely at a student response for determining a score. This is an approach which though interesting, is ultimately a method this paper decides not to evaluate. For this reason, the HP Dataset was not used.

Finally, there may well be other more well-balanced datasets online of which the author is not aware and did not discover in his research, though he did attempt an exhaustive search both in the literature and online.