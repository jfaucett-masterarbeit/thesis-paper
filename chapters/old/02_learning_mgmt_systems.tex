\chapter{Stembord: an Intelligent Learning System}

\begin{quotation}
``Some people call this artificial intelligence, but the reality is this technology will enhance us. So instead of artificial intelligence, I think we'll augment our intelligence.''

\rightline{{\rm --- Ginni Rometty, CEO, IBM}}
\end{quotation}

This chapter forays into the depths of computational learning systems. Over that journey an exploration of vital concepts such as intelligent tutoring systems, learning management systems, and the role of artificial intelligence in education is conducted in order to elucidate the functionality of Stembord, an intelligent learning system, argue for the necessity of the chosen methods, and characterize the constraints imposed by the Stembord system on current and future development of the ASAG module.

\section{Definition and Function of Learning Management Systems (LMS)}

In the 1950s, long before computers began to clutter the pockets and desks of society, computational technology was already being applied in education \cite{watson_2007}. A Learning Management System (LMS) is one approach of many to this application of computers in the educational domain. Aside from LMS, there are many other generic terms in the literature such as computer-based instruction (CBI), computer-assisted instruction (CAI) and computer-assisted learning (CAL) which all outline various programs for drilling exercises or individualizing instruction \cite{watson_2007}. The distinction between these terms and LMS is that an LMS goes beyond instructional content and offers management, tracking, personalized instruction and integration across the entire system \cite{watson_2007}. It is a framework that handles every facet of the learning process in an organization or institution. It delivers the instructional content, assesses and identifies individual goals and training. It tracks individual and classroom progress and collects and presents interpretable data so that students and educators, through clearer information can better understand the various states of knowledge and skill acquisition by users of the system \cite{watson_2007}.

There is much confusion in the literature as to the precise nature of an LMS and its distinguishing characteristics from other similar technologies such as Content Management Systems (CMS), for example Blackboard, and Learning Content Management Systems (LCMS). The key insight is that an LMS manages the learning process as a whole, it provides the rules, whereas a CMS and LCMS are devoted to the content and learning objects (LO) themselves such as creating lessons and tutorials - they provide the content \cite{watson_2007}.

Some of the key features which most LMS solutions provide are listed below \cite{coates_2005}.

\begin{enumerate}
\item asynchronous and synchronous communication (chat, e-email, instant messaging, discussion forums)
\item content development and delivery (Learning Objects, links to internet resources)
\item formative and summative assessment (submission, testing, collaborative work and feedback)
\item class and user management (registration, enrollment, timetables, managing student activities)
\end{enumerate}

These features demonstrate the broad reaching scope of LMSs as these systems are designed to support the productivity and effectiveness of all stakeholders in the educational context; students, teachers and administrators alike. 

\subsection{Learning Objects}

Learning objects (LO) are the smallest unit of content within a CMS or LCMS. They are powerful because they offer reusability across systems and contexts \cite{watson_2007}. An example of a learning object is a lesson on ``Taking the square root'' or a quiz on ``The biological organization of the cell''. Each of these LOs can be used within a classroom, but also shared and used by other teachers and educators within their classrooms, or even be studied solo by a student simply interested in learning about taking the square roots of numbers or understanding the structural organization of a cell. Overall, there are four properties of LOs.

\begin{enumerate}
\item \textbf{Reusability} : for example, a lesson once created can be used in a course forever.
\item \textbf{Generativity} : a lesson or quiz can be used to create new instructional content.
\item \textbf{Adaptability} : a lesson can be given to a student based on his skill level.  
\item \textbf{Scalability} : a LO can scale to meet the needs of small or large audiences without an increase in costs.
\end{enumerate}

It is important to note that LOs are not just lessons but can be quizzes, interactive visualizations, and even full exams, that is, they are modular structural units of teaching and learning. They are the building blocks, the grammar which provides the combinatorial productions of the language of learning systems.

\subsection{ASAG's relationship and role in LMSs}

In the LMS, LCMS or CMS systems on the market, ASAG is, to the best of my knowledge, ubiquitously introduced as a further question type which teachers have for designing their LO quizzes and exams. Curricula designers create a question of type \textit{short answer} and then have an options menu for configuring the question. Some systems such as Moodle allow the teacher to create a list of regular expressions which are then matched against student input to calculate a score \cite{moodle_asag_2018}. Blackboard, the most widely used CMS system for education in the US, as well as other open source solutions such as OpenOLAT don't provide for ASAG, however, Blackboard still requires teachers to enter a reference answer in order to give students formative feedback \cite{blackboard_asag_2018,watson_2007}. From a software design perspective, most of these production systems are closed source which makes it impossible to assess how they incorporate an ASAG module into the complete architectural design of the entire system, however, Moodle is open-source and includes the \textit{short answer} module as a further question type in a submodule designated for all question types within the main software repository.

\subsection{Reasons for LMS usage and adoption uptake}

Learning Management Systems have seen rapid adoption across the globe despite the complexities, costs and risks involved in the adoption process \cite{coates_2005}. When an institution endeavors to adopt a LMS, it is not partaking in a low-risk decision making process. This resolution involves institutional and technological forecasting, restructuring of complicated administrative, educational and technological issues, a consideration of the interests of a deluge of disparate stakeholders, and often a reconsideration of institutional policies and procedures, not to mention new guidelines for accountability and control \cite{coates_2005}.

Despite all of this, many universities have decided to adopt LMS solutions. Clearly, these institutions see potential for increased productivity along some dimensions otherwise they would not be taking the risks and undergoing the hurdles in order to adopt these systems. Of these dimensions or reasons for adoption, the most commonly listed are access, cost and quality although it is certainly possible to narrow down to a plethora of more specific factors \cite{coates_2005}.

A detailed assessment of the varied reasons for LMS adoption is tangential to the subject of this thesis, however, Coates identified several factors such as a means of delivering large-scale resource-based learning programs, flexible course delivery, increased teaching efficiency, reduction in course management overheads, enriched student learning, automatic and adaptive assessment, and competitive pressure between institutions to meet demands - just to name a few salient components \cite{coates_2005}.

Regardless of the causes, LMS have permeated the educational domain and it appears they will stay for a while, transforming and interacting with the academic landscape into the future.

\section{Intelligent Tutoring Systems}

The goals of Intelligent Tutoring Systems (ITS) are similar to those of an LMS: to provide services which support learning, however, for an ITS these services are specifically tailored for the tutoring context \cite{nkambou_2013}. Research in the field began in the 1960s and '70s with the term ``Intelligent Tutoring Systems'' eventually being coined by Sleeman and Brown in 1982, followed in the same decade by the first ITS conference in 1988. One motivator for the field was research published by Bloom in 1984 which demonstrated that individual tutoring is twice as effective as group teaching \cite{bloom_1984, nkambou_2013}, and in the first two decades of its incipient rise, ITS was further driven by visionaries who dreamed that every child would ``have access to ... the personal services of a tutor as well informed as Aristotle'' (Suppes, quoted in Nwana 1990). Into the 90s ITS had begun to establish itself as an engineering design field with scientific foundations to its research \cite{nkambou_2013}.

In order to model tutoring, ITS use a four-component architecture which can be seen in Fig. \ref{s2_fig1}. The fourth component is the user interface and will be ignored in this section since its not useful in understanding how intelligent systems function as a whole.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/four_component_arch.png}
	\caption{The four-component architecture view of ITS.}
	\label{s2_fig1}
\end{figure}

The \textbf{domain model} contains the knowledge base and reasoning faculties. It stores the concepts and rules as well as problem-solving strategies within the domain to be learned and is expected to adapt the explanation of the reasoning to the learner. Knowledge elements in the domain model can be linked together as lessons or more loosely designed as dynamic curricula and there are many different structures for storing this information such as semantic networks, frames, ontologies and production rules \cite{nkambou_2013}.

The \textbf{student model} stores knowledge about the student's affective state, his current knowledge on the topic and his evolution through the learning process. The student model must gather explicit and implicit data about the learner, synthesize that data to create representations of the student's knowledge and state in the learning process, and it must account for the data by performing diagnosis and selecting optimal pedagogical strategies for the presentation of subsequent learning materials to the student \cite{nkambou_2013}.

Lastly, the \textbf{tutoring model} interacts with the student and domain models and determines which actions and tutoring strategies to take. It must make decisions about if and when to intervene, and how and when it should deliver learning content. These can be Socratic dialogues, feedback, hints, but also simulations and visualizations \cite{nkambou_2013}.

\section{Stembord Project}

Stembord is a combination of an LMS and an ITS. Its goal is to synergize the concepts of ITS and LMS into a system which provides learners and educators with resources optimized for their educational needs and adapted to their cognitive states. The following section is a short piece of prose written to give the reader insight into Stembord's long-term vision.

\subsection{Stembord Project Vision for the Future}

The year is 2035, and Dave is about to graduate from high school at 16. There's nothing abnormal about this, about 15\% of the students each year have already completed enough curricula requirements to begin studying at a university. Dave has been able to achieve this in large part because he has someone special. Every year since Dave was a child he has been tutored by Hal. Hal has a slightly stern personality, but he can be lovable and even funny at times. In fact, and Dave knows this, Hal has a personality adapted specifically for Dave. Its a personality designed to motivate Dave and encourage him, to keep him from slacking and give him a desire to achieve. Hal also knows exactly how to describe new materials in a way that clarifies for Dave how the subject works, if Hal had to describe the same topic to someone else he would very likely use a somewhat different approach. Not only does Hal adapt his teaching strategies for Dave, but because Dave is dyslexic Hal presents materials in an accessible way optimized to alleviate dyslexic learning problems. Hal also has amazing visualization and clarification skills. Dave still remembers how Hal once took him on a VR journey in a 3D world where Dave could manipulate quadratic functions in real-time in order to model the shape, peak, and times of rockets' trajectories. Or how once for a literature class, Hal read him out loud The Canterbury Tales in the exact Middle English of the 14th century, he also created vocabulary lists for Dave of the Middle English words Dave didn't know, and after some training, took Dave into a simulation of the 14th century where Dave could speak to and question Chaucer's characters all in Middle English. 

Hal even knows when Dave hasn't studied a piece of material in a while and gives him a helpful reminder and often impromptu customized quizzes which are designed to reinforce and ingrain in Dave's long-term memory subject matter that was covered perhaps months or even years before.

The breadth and depth of knowledge that Hal has makes him the perfect tutor. For Dave, he can have a one-on-one dialog with an expert in whatever subject he is studying from physics to anthropology, and because Hal knows so much and has a synthesis of all this knowledge - Hal is constantly consuming and synthesizing the research from every paper in every field each and every day - Dave can use Hal to help him when conducting research reports and doing statistical analyses. If Dave forgets which statistical test to run, Hal can not only tell him which is appropriate in a given scenario but why and also offer guided instruction so Dave doesn't need Hal's help as much in the future.  Hal gives Dave, Dave's parents, and his teachers and school administrators formative and summative feedback about Dave's progress in a way designed to counter bias and not only increase Dave's own knowledge and skills but increase his teachers' and parents abilities to help him along his learning pathway.

Of course, Dave isn't the only one who has access to Hal, Hal's a rather democratic fellow and there's plenty of him to go around since he is after all, a program. Everyone has access to Hal, from the oldest senior citizen to the youngest preschooler. But the fact that he's so spread out or that he's a program doesn't bother Hal one bit. In fact, its been years since he has had to say, ``I'm sorry Dave, I'm afraid I can't do that'' in response to a question Dave or anyone else has posed. 

\subsection{Immediate and Long-Term goals of Stembord}

The previous section provides a narrative tour of the long-term goals of Stembord from a single student's perspective, but students are not the only stakeholders Stembord is concerned with. Others are parents, teachers, as well as school administrators, each of which would benefit enormously from systems designed with the tenets of cognitive psychology to promote transparency, engagement, mental growth and reduce bias. However, in order to achieve the long-term goals previously outlined, many diverse building blocks are necessary. Chief among these is a need for quality LOs such as quizzes, guides, and interactive simulations in every domain from physics to literature, not to mention labeled knowledge bases for handling one-on-one student learning sessions between a Stembord AI tutor, ``Hal'' and the student. Before any query can be made by an AI tutor, there must exists a database over which to query. But Stembord also needs to build student models so that it can discern the affective and progress states of each student, querying and adapting lesson plans and learning pathways to the skill-sets, faculties and needs of each student dynamically.

In order to achieve these far reaching goals, Stembord has decided to focus initially on the educator side of the problem by building tools which allow teachers to design eloquent and semantically labeled LOs and combine them to construct online courses, exams and guided tutorials. From an ITS view, one could see this as a focus on building a broad and deep domain model backed by a robust knowledge base first. Only later, is the goal to then progressively add student modeling, interactive natural language dialogue, the tutor model, as well as school administration capabilities such as enrollment, scheduling, etc. None of these later features are currently implemented in Stembord, however, any new module, particularly the ASAG module, must take these long-term architectural goals into account in order to be an effective component in the end-product into the future.

\subsection{Stembord Architecture and System Design}

Detailing the full architectural design of Stembord is beyond the scope of this thesis, instead, subsystems which are important for building the ASAG module are outlined in this section. These are the system for managing Learning Objects (LOs) called lessons in Stembord and the classroom management systems. The quiz and ASAG modules which have been developed for this thesis are both described in the chapter on module implementation and architecture.

The main components of Stembord are shown in Fig. \ref{s2_fig2}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/users_roles_and_orgs.png}
	\caption{The Users, Roles and Organization structural components of Stembord.}
	\label{s2_fig2}
\end{figure}

Every user of the system can be either a \textit{Student}, a \textit{Teacher} or both, just like in the real-world. This state is modeled by assigning a User a role having a \textit{role\_name} attribute of \textit{teacher}. All users on the platform have access privileges and the abilities of students. Student is the default role.  An Organization, for example a University, has many users, some of them are students, some are teachers, and some are both. Users with a \textit{teacher} role can create Classrooms and Lessons for an Organization. A Classroom has many lessons but lessons can also be unassociated with a classroom and simply belong to the Organization, a typical example of this scenario would be a single lecture or tutorial that a teacher would like to give. Lessons and Classrooms can be made publicly accessible and then any Student on the Stembord platform, even those which don't belong to the organization, can study the material for themselves and any Teacher can copy the lesson or classroom, change it to fit his or her needs and then reuse it to teach a class or give a lecture. At the time of this writing, this is the fundamental structure of Stembord.

\subsection{A Closer Look at Classrooms in Stembord}

Classrooms have a lot of associated metadata for several reasons. First, teachers need to be able to discover learning objects, classrooms and resources on the platform so that they can reuse these resources for their own teaching tasks and without this metadata it would be difficult to find desired resources. Secondly, having more descriptive metadata about learning objects and classrooms builds a more helpful knowledge base for long-term development of the tutoring AI. This metadata is shown in Fig. \ref{s2_fig3}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/classroom_metadata.png}
	\caption{The classroom metadata structure.}
	\label{s2_fig3}
\end{figure}

The \textit{language} field stores the the ISO 639-1 language code of the classroom, for an English language classroom it would have the value \textit{``en''} and for a German language classroom the value would be \textit{``de''}. The \textit{subject} field stores the main domain of the classroom, for instance, a classroom on ``Introduction to Programming in Java'' has a subject domain of ``Computer Science'' whereas a course on ``Linear Algebra'' has a subject domain of ``Mathematics''. The \textit{title} and \textit{description} fields give the name of the classroom i.e. ``Linear Algebra'' and a textual description of the course. Finally, the \textit{tags} array stores a list of tags - short text strings - which provide more detail about the classroom. An example tag list for ``Linear Algebra'' could be \textit{matrix transformations}, \textit{eigenvectors}, \textit{linear systems} and \textit{vector spaces} for instance. All of the fields and values are useful for refining user queries and providing both teachers and students with appropriate information.

Classrooms themselves are structured around units. Each unit represents a contained piece of material to be covered over a period of time, for instance one unit could be a week dedicated to the topic of cell structure in a biology course. Each unit contains a series of lessons (i.e. learning objects), that is, quizzes, guided tutorials, texts and videos. Units are initially inaccessible by students, but the teachers can toggle the availability of units in order to progressively allow students to gain access and work on classroom materials. Optionally, teachers can also make all units available from the outset. The choice depends on the preferences of the teacher and his or her needs for the class. Figure \ref{s2_fig4} demonstrates the relational structure between Classrooms, Units and Lessons without listing all the attributes of each class.


\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/classroom_structure.png}
	\caption{The relational structure between Classrooms, Units and Lessons.}
	\label{s2_fig4}
\end{figure}

Finally, important for the development of the ASAG module is the fact that classrooms come in two flavors, \textit{normal} and \textit{self-study}. Normal classrooms are exactly what one would expect, they have a teacher and a set of students, they start at a particular point in time, last for a semester or so and come to an end. Self-study classrooms on the other hand have no termination and no teachers. These represent MOOCs for which any student can register and work through the classroom units and lessons at his or her own pace. Having self-study classrooms means that an ASAG module must also work when no teacher is available to correct or grade student responses to short text items.  

\subsection{A detailed perspective on Lessons in Stembord}

The lessons module is the locus of core functionality on the Stembord platform, more precisely, it is the module which handles LOs such as quizzes and a myriad of learning content objects, for instance, instructional videos, texts, questions and interactive media. Figure \ref{s2_fig5} demonstrates the class structure of a section of this module. 

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/lesson_structure.png}
	\caption{The structure of LOs on Stembord. Lessons are made up of nodes}
	\label{s2_fig5}
\end{figure}

Lessons belong to an Organization and can be associated with a Classroom through a join table making them independent of but usable by the Classroom system. Lessons have mostly the same metadata as Classrooms and for the same reasons. They have a UUID for globally unique access and boolean field \textit{public} which when true allows other Teachers on the platform to copy the Lesson and other Students to see, read and consume it.

All Lessons are made up of a collection of nodes which are linked to one-another in an ordered list fashion. Every node has content, which is a JSON formatted string holding the information in a structured machine processable format for rendering in the frontend web application. Nodes also store points which can range from $0$ to $1000$ and can be used by teachers for low-stakes grading purposes. Each node has a \textit{NodeType} which designates what data is stored on the node and how the node behaves. There are currently node types for text, question and video. A text node type can not only render any formatted text but also most latex, embedded images and URL links which makes it flexible as a content type for creating LOs. When a node of type question is created an associated \textit{NodeQuestion} object is instantiated and a corresponding entry is stored in the database and associated with the node. This \textit{NodeQuestion} object can store a question prompt, an explanation of the correct answer, and a JSON formatted content text which varies by question type. The currently available question types are \textit{multiple choice}, \textit{true/false} and \textit{fill in the blank}. This thesis researches and develops a module for a fourth question type \textit{short answer}. 

The classes in Fig. \ref{s2_fig5} are sufficient to store, display and consume LOs such as guided tutorials, quizzes and lecture videos, however, in order to provide educators with summative and formative assessment capabilities other structures are necessary. These are shown in Fig. \ref{s2_fig6}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/grading_system.png}
	\caption{The Grading System in Stembord - StudentSessions, StudentSessionNodes and StudentSessionGrades}
	\label{s2_fig6}
\end{figure}

When a Student starts working on a Lesson, Stembord creates a StudentSession object whose state is set to \textit{in-progress}. As the student then works through the Lesson and completes nodes such as watching a video node, reading a text node, and completing some question nodes, StudentSessionNodes are created which store the responses of the student for that particular node in the session. Once the Student has finished the Lesson the StudentSession object's state is set to \textit{finished} and the object is visible to teachers in an interactive grade editor. Teachers can then review and adjust the student's points on a per node bases if they desire, and when they are finished, they grade the student by clicking a labeled button in the editor which creates a StudentSessionGrade object and stores the student's final grade for the LO in question.

When calculating the points a student receives for a question node, the student's response is currently evaluated within the backend web-application of Stembord. The ASAG module developed for this thesis externalizes this call by providing an external API service designed specifically for processing and grading node questions. The details of this interaction are detailed in the chapter on development and implementation of the module itself.

\section{Summary}

This chapter has presented Stembord, a system which has the long-term goal of combining LMS and ITS technology to produce an intelligent learning platform for students and teachers. Currently, Stembord is more aligned with the functionality of a LCMS than with its far-reaching goals, however, these long-term objectives constrain and inform the research and development of the ASAG module, more so even, than the current state of the Stembord architecture.

Now that the necessary background work has been laid, in the next chapter, key motivating questions, the problem definition and technical measurements for assessing the research problem can finally be explicated. 