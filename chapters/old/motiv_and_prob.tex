
The evaluation metrics make up the core conceptual space motivating experiment choice and direction, however, they are not the only motivators. Those relevant to the research portion are reiterated below along with additional points of interests, this time without the clarifications, notice the only absent member of the original metrics is \textit{Builds Unbiased Dataset} since this is almost purely a software engineering and design problem.


\begin{enumerate}
\item Automatic Scoring
\item Provides Formative Feedback
\item Efficient
\item Requires Minimal Technical Expertise and Effort
\item Builds Knowledge Base
\item Works for Self-Study and Teacher-Guided Classrooms
\item Method is effective across languages
\end{enumerate}

In general, the experiments look at how, if at all, each of these requirements are met and to what degree as well as the trade-offs between items. Broadly, these requirements inform the experimental questions in that they are the goals and the experiments are the searches within a large solution space. Any solution should automatically score a student response, be efficient in time and space, provide helpful and intuitively understandable feedback, require minimal expert knowledge, build an internal Knowledge Base, and work when there is a teacher for a classroom and when there is no teacher. Trying to meet these requirements engenders experiments which look at the effectiveness of reasoning and inference, understanding the nature of similarity metrics between sentences and between word pairs, as well as looking at the role syntax plays in two otherwise word-pair for word-pair semantically equivalent sentences. However, over the course of some of these experiments and beyond the goals outlined above several specific questions arise in experiment categories related to text formulation, these are:

\begin{enumerate}
\item How many reference sentences should be used?
\item What are the trade-offs of using full reference sentences vs. reference concepts i.e. short phrases denoting specific ideas which a student needs to address?
\end{enumerate}

These are mentioned and examined in the appropriate experiments in later sections. 

Ultimately, due to the multilingual nature of Stembord, analyzing how the experimental methods compare across languages is a specifically important question.

\section{Parameters and Constraints for the Research Problem}

There are many constraints the Stembord system sets on the ASAG research problem, and of these, seven have been crystallized into evaluation components for the analysis of the research results conducted in developing the ASAG module. These salient attributes of the desired ASAG system are listed in descending priority below.

\begin{enumerate}
\item \textbf{Automatic Scoring}: the ASAG student response must receive an automatically assigned score by the system.
\item \textbf{Builds Unbiased Dataset}: the current implementation should store the right amount of information, in a robust format, in as unbiased a manner as possible in order to forge a dataset for testing and improving upon the ASAG module developed in this thesis in the future.
\item \textbf{Provides Formative Student and Teacher Feedback}: it should be transparent both for teachers and students as to why a student receives a particular score on an ASAG question.
\item \textbf{Efficient}: the ASAG algorithm should have a practical time and space complexity such that it can work in a production system. Algorithms with exponential or otherwise impractical growth rates should provide large warning signs and hint at the lack of effectivenss of the proposed method. An additional beneficial attribute would be if the proposed technique were able to run within the memory available on a single machine.
\item \textbf{Minimal Technical Expertise Required}: the easier it is technically for teachers and domain knowledge experts to develop ASAG questions the better. This can be qualitatively measured by the number of steps teachers need to complete per ASAG question item, the cognitive complexity of completing a step, for instance, selecting an option vs. writing multiple concepts and connecting them, and where possible the amount of time spent per step.
\item \textbf{Builds Knowledge Base}: the data which teachers enter for quizzes, lessons and specifically the ASAG module itself would ideally be usable for building out the platform's knowledge base in areas such as question types, model answers and concepts related to questions in particular domains.
\item \textbf{Works for Self-Study and Teacher-Guided Classrooms}: method should work both for courses which have an active teacher involved as well as those who do not.
\end{enumerate}

Some of these may be mutually exclusive, for instance, the attributes \textit{Builds Knowledge Base} and \textit{Minimal Technical Expertise Required} could work against each another, or it could be the case that providing formative student and teacher feedback decreases the efficiency and increases expertise required. Nonetheless these items represent a desired vector of goals which can be used to evaluate each ASAG experiment and weigh the merits and detriments of the method in question.



Concept Maps and IE techniques were also attempted and are appealing for a number of reasons. First, concept maps draw from a Knowledge Base, providing perhaps an effective motivation for teachers to build a KB which can later be used for reasoning and inference throughout the Stembord platform. Secondly, IE methods as well as concept maps can attempt to look at underlying ideas and concepts expressed in text. This seems to perhaps be an aspect of short answer grading from a human psychological perspective i.e. did the student express the concept ``a function has zero or more arguments'' and if so, then he should receive a high score, if not then a failing score and if he garbled the concept, perhaps stating that ``a function has zero or one arguments'', he should then receive a mid-range score, but one perhaps higher than absolute zero. At any rate, evaluating the effectiveness of reasoning and inference, and taking into account such aspects as word negation and syntactic sentence order via IE techniques as it pertains to ASAG is the motivation for this range of experiments.