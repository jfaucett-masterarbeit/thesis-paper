\chapter[Motivation and Problem Definition]{Motivation and Problem Definition}


\begin{quotation}
``I learned very early the difference between knowing the name of something and knowing something.''

\rightline{{\rm --- Richard Feynman}}
\end{quotation}

Up until this point important background information about Automatic Short Answer Grading, Learning Management Systems and particularly Stembord has been expounded upon in order to provide a clear backdrop for understanding the problem definition and key motivations presented in this chapter. Elucidating and clarifying the motivating questions and using them to inform and develop the problem definition and, additionally, a set of technical measurements for evaluating experimental results are the main purposes of this chapter.

\section{Motivating Questions}

Any ASAG module must, at bare minimum, be capable of automatically scoring student responses, that is, of associating a numerical value with a student response item. In this thesis, however, the ASAG problem is not examined in isolation but also in the context of integration as a functional module within the online LMS known as Stembord. This fact alters, in many ways, various aspects of the traditional research problem. First, it increases the importance of understanding the topology of short answers themselves because a living software ecosystem with realtime users who are teachers and students and have real-world stakes in grading outcomes necessitates a clearly articulated language for discussing those grades. Secondly, and building on the first, not only is a clear language for identifying the structure of short answer responses needed, but also some means is required for illuminating these structural aspects of teacher grades and student responses to the stakeholders in the system, so that, for instance, a student easily understands why he received a particular score and a teacher has insight into why a group of students consistently responded poorly to a particular question item.

These needs are derived from two domains of influence and lead to questions which are either inherent to the problem of ASAG or motivated by the Stembord platform.

\subsection{Questions Inherent to ASAG}

\paragraph{Questions related to Structure and Topology of Short Answers}

The first question which forms when examining ASAG for any length of the time regards the structure and topology of short answer responses as well as their relationships to the broader context of question prompts and model answers. An example posited question may be: what does a prototypical short answer response look like? Are there structural patterns in student responses which correlate to received grades? Are there systematic ways in which mistakes are made both by graders and students? These questions are driven by the desire to understand the structure and nature of short answer responses much like a biologist might study the anatomy of a cell without a specific goal other than to increase understanding of the cell's functioning and behaviour under certain conditions.

\paragraph{Questions related to terminology for Discussing Structure and Topology}

Building on topological and structural understanding, the next set of questions concerns whether discovered patterns and structures can be named and labeled to provide insight, allow for meaningful discussion, and inform future research directions. For instance, given a short answer question which the student either does not know the answer to or only has vague knowledge of, he may respond with \textit{```I do not know'''}, \textit{```I think it might be a query'''} or he may simply leave the entry blank. In these cases, should there be different terms for the blank entry and the entries with textual content? Does insight gained depend on the textual content? If we call these answers, \textit{wrong\_admission}, \textit{wrong\_made\_attempt}, \textit{wrong\_no\_content} does that provide meaningful naming not only for human researchers studying this problem but also do these names correlate with teacher scoring or other meaningful structural entities in some way?

\paragraph{Questions related to assigning a numerical score to Student Responses}

When assigning a score to a student response, what gradation does that score need? Can it be discrete and simple i.e. in the range of $[0,1]$ in discrete $0.25$ intervals or does the computed score need to be continuous? Does it make more sense for the scores to be represented as probabilities over mutually exclusive catagories such as $wrong$, $partially\_correct$, and $correct$ or should varous classifications be made and an average score computed from a diverse set of scores for grammatical correctness, concept coverage, etc.? This thesis does explore some of the variety of scoring options expoused here, however, a focus is made on continuous scoring in the range $[0,1]$ because this is by far the most common methodology employed in the literature. Nonetheless, knowing that there are a vast array of scoring options available can be used to inform future research directions and think about results from a different perspective, since problems inherent to continuous scoring, for instance, may be alleviated or spread to other areas when the scoring is changed to a discrete interval function.

\paragraph{Questions related to Complexity}

Once methods are employed to model numerical score assignment, a host of questions arises as to how these methods compare to one another. Are they efficient in time and space complexity? Are they easy or difficult to implement both in an isolated environment and in a LMS like Stembord? Are these methods practical solutions to the problem or theoretical approaches which will not scale in the real world?


\subsection{Questions resulting from Stembord}

\paragraph{Questions related to Feedback Generation}

In previous sections the importance of formative feedback has been iterated many times, but even though feedback is vital to communicating scores effectively to all stakeholders involved, it has been too often, in the author's own opinion, neglected in the literature on ASAG. Some of the questions that arise from Stembord related to feedback and short answers are: What types of feedback are most useful to students? What types of feedback are allowed by particular choices of automatic grading algorithms? Does it make sense to have different feedback for self-study courses, which have no teacher, as opposed to those which do have teachers?

\paragraph{Questions releated to Minimizing Teacher Effort and Expertise Required}

Core to this line of exploration is the question: What is the relationship between teacher effort and expertise required and effectiveness of the implemented ASAG strategy? For instance, must teacher's be knowledgeable about linguistics and natural language processing in order to construct effective model answers for students or does simple domain knowledge suffice? Further, how much time is required by teachers to train or prepare the data for a model or particular ASAG method and does increased time correlate highly with model effectiveness? Is teacher expertise necessary at all? Must teachers work as a supervised trainer grading a subset of the answers first and if so why?

\subsection{Summary of Key Questions}

Of all of various research questions mentioned above, concretely the following are explored in this thesis:

\begin{enumerate}
\item How does increasing the number of model answers affect the ability of an ASAG solution?
\item How well does a particular ASAG method generalize to languages other than English?
\item What is the best method for extracting meaning from the words of a sentence?
\item How do knowledge based measures of word meaning using \textit{WordNet} compare to word embeddings using \textit{Word2Vec} or \textit{FastText}, what are the key differences and trade-offs?
\item What is the best unsupervised i.e. algorithmic approach to ASAG that does not rely on machine learning?
\item What machine learning model is best for supervised ASAG using discoveries made in this thesis?
\item What are perhaps additional annotional features which improve the performance of the machine learning model but may require extra human input?
\end{enumerate}

\section{Problem Definition}

Short-answer question items make up the core components of ASAG and consist of three key parts: a question prompt, one or more model answers written by educators and a student response. These are visually displayed in Fig. \ref{prob_def_threeComponentsOfASAG}.

\begin{figure}
	\centering
		\includegraphics[scale=0.6]{images/02_motivation_and_problem_definition/Text_Example_Question_with_Model_Answer.png}
	\caption[Core Components of ASAG]{Example showing the three core components of short-answer question items: The prompt, teacher created model answer and student response.}
	\label{prob_def_threeComponentsOfASAG}
\end{figure}

More formally, for the purposes of this thesis, the problem of Automatic Short Answer Grading is defined as follows:

\begin{definition}
Given a set $M$ of reference short text responses such that $\lvert M \rvert \geq 1$ and a short-text student response $S$, develop a function $f(M,S)$ such that $0 \leq f(M,S) \leq 1$. Where $f(M,S) = 1$ means $S$ is semantically identical to $M$ and $f(M,S) = 0$ means $S$ is completely unrelated to $M$. Values ranging between $0$ and $1$ represent a gradient of increasing shared meaning between $S$ and $M$ as $f(M,S)$ approaches $1$.
\end{definition}

Concrete examples of the three components given differing student responses and consequent scores are shown in Fig. \ref{prob_def_asagScoreAssignment}.

\begin{figure}
	\centering
		\includegraphics[scale=0.6]{images/02_motivation_and_problem_definition/ASAG_Scoring_Problem_Definition.png}
	\caption[ASAG function assigned scores]{Demostration of how the function $f(M,S)$ should work. The three possible scoring situations for student responses are shown. \textbf{A} shows a correct student response which consequently receives a perfect score ($1.0$). \textbf{B} shows a false answer which gets a score of $0.0$. And \textbf{C} shows a student response that is only partially correct. It consequently receives a score of $0.5$.}
	\label{prob_def_asagScoreAssignment}
\end{figure}

The reason the range of the function $f(M,S)$ is $[0,1]$, is that any output of such a function can easily be scaled to any grade value. For instance, if a teacher assigns 20 points to a short-answer question and $f(M,S)$ calculates a similarity score of $0.7$ between model-answer $M$ and student-response $S$ then the student can straightforwardly receive $14$ points i.e. $0.7\cdot 20 = 14$.

There are many other ways to define the problem of assigning a score to a short text response submitted by a student. Indeed, in some of the experiments in this thesis the set $M$ is modified to be, for instance, a set of patterns written by teachers. Nonetheless, the structure of the problem stays the same and the range of its output on a continuous scale remains in essence equivalent. This is not to say this is the correct approach or that other approaches are not possible, it should mainly serve as the formal grounding for understanding the problem.

\section{Experiment Categories}

The experiments conducted for this thesis can be classified into five categories which are listed below and are informed by the motivating questions inherent to ASAG, arising from the constraints of Stembord and further specified by the problem definition itself.

\subsection{Regular Expressions}

Using a list of regular expressions for ASAG is a method already implemented in some LMS software, for example Moodle offers this functionality \cite{moodle_asag_2018}. Since this is a option that is already being offered on the market, it is important to evaluate its effectiveness against a dataset, not only for baseline purposes, but also to develop an initial understanding of the problem space. That is the purpose of this category which only contains one experiment.

\subsection{Lexical Structure}

These are various measurement techniques which only look at the surface i.e. lexical structure of sentences for comparison purposes. This category of experiments is important for understanding how effective methods can be which make no attempt at understanding the semantics of the sentences and only draw on syntax to the level of n-grams.

\subsection{Syntactical Structure}

The syntactial experiments look at the relationships between words to attempt to gain insight and solve problems with meaning from a relational perspective. For instance, the phrases \textit{queues are implement as arrays} and \textit{arrays are implemented as queues} although they have the exact same words have completely different meanings due solely to the syntactical relationship changes between those words. These experiements attempt to analyze and extract this information for grading purposes.

\subsection{Semantics}

Lexical overlap methods, syntactical structures and regular expressions do not attempt to infer deep meaning. None of these techniques look at the underlying conceptual domains which most humans associate with meaning. When comparing two sentences an intuitive way to think about them is to compare the meanings of each of their words, if the word meanings are close to one another the sentences probably mean close to the same thing, if the meanings are far apart then probably not. Trying to understand the effectiveness of bringing in the semantics of words into the comparison of two sentences is the subject of these experiments. These all use BoW (Bag-of-Words) methods - which means they ignore the role of syntax - and are broken down into two broad categories which are Knowledge Base methods using a human-made semantic graph library like WordNet and Vector Space Models which use statistical properties of language to automatically learn the meanings of words.

\subsection{Machine Learning}

The experiment categories up to Machine Learning, attempt to isolate a particular portion of the problem of ASAG and examine how effective and to what degree techniques employed for that particular aspect of the problem are. The set of machine learning experiments, however, attempts to use the knowledge gained from the isolated experiments to combine and create hybrid approaches that perform better across the entire problem.

\paragraph{Additionally Attempted Experiments}

In addition to the main experimental categories just outlined, some time was spent on an implementation of the paper ``Wisdom of Students: A Consistent Automatic Short Answer Grading Technique'' by Roy et al, which is a method for automatically grading student responses without a reference answer or any teacher input, which failed to reproduce the results cited \cite{roy_dandapat_nagesh_2016}. This is referenced where appropriate throughout this text. Additionally, Concept Maps and IE techniques were also explored but quickly exploded the scope of this thesis. They are, however, discussed in some more depth in the final chapter.

\section{Technical Evaluation Sections (TES) for Evaluating Experiment Results}

In order to be able to better compare the results of the variety of experiments within each category carried out in this thesis, a unifying, consistent and structured way of evaluating the results is needed. In order to aid reader comprehension and analytical comparisons, the same measurements are examined in every experiment for evaluation and analysis purposes. They are informed by the motivating questions and problem definition and attempt to provide consistent structure for both qualitative and quantitative analysis of experiment results as one springs from experiment to experiment.  

\subsection{TES.I - Effectiveness at Automatic Grade Assignment}

This section always analyzes the effectiveness of the given experiment at automatically scoring the student response items. It uses charts and graphs and extracted examples to attempt to understand why a particular experiment gave its results and attempts to draw conclusions and insights based on those results.

\subsection{TES.II - Time and Space Complexity}

This section looks at the experiment from both an algorithmic as well as an implementation perspective. In it there is an analysis and discussion of the algorihtmic trade-offs of the proposed approach to ASAG as well as a detailed look at particular difficulties or lack thereof for an implementation within a LMS.

\subsection{TES.III - Amount of Teacher Expertise and Effort Required}

This section examines how much teacher effort is needed in order to carry out the proposed approach. It is also forward looking in that, in some cases, it offers ways the method could also be improved from teacher input or increased teacher training or expertise. Some experiments use methods which are fully automatic and require no teacher expertise or effort, for those this section is left out.

\subsection{TES.IV - Fulfillment of Desired Characteristics for Stembord}

This measurement includes a qualitative analysis of the degree to which the experiment's approach provides informative feedback, how well it builds or augments a useful knowledge base, as well as the method's effectiveness across classroom types within Stembord.

In general, all of the above four Technical Evaluation (TE) sections for an experiment can occur, however, if for some reason a TE section is not relevant for a particular experiment then is left out, since there is no reason to list a section only to state that is not relevant for the given experiment. 