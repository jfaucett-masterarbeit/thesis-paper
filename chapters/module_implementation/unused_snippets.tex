\subsubsection{A Closer Look at Classrooms in Stembord}

Classrooms have a lot of associated metadata for several reasons. First, teachers need to be able to discover learning objects, classrooms and resources on the platform so that they can reuse these resources for their own teaching tasks and without this metadata it would be difficult to find desired resources. Secondly, having more descriptive metadata about learning objects and classrooms builds a more helpful knowledge base for long-term development of the tutoring AI. This metadata is shown in Fig. \ref{s2_fig3}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/classroom_metadata.png}
	\caption{The classroom metadata structure.}
	\label{s2_fig3}
\end{figure}

The \textit{language} field stores the the ISO 639-1 language code of the classroom, for an English language classroom it would have the value \textit{``en''} and for a German language classroom the value would be \textit{``de''}. The \textit{subject} field stores the main domain of the classroom, for instance, a classroom on ``Introduction to Programming in Java'' has a subject domain of ``Computer Science'' whereas a course on ``Linear Algebra'' has a subject domain of ``Mathematics''. The \textit{title} and \textit{description} fields give the name of the classroom i.e. ``Linear Algebra'' and a textual description of the course. Finally, the \textit{tags} array stores a list of tags - short text strings - which provide more detail about the classroom. An example tag list for ``Linear Algebra'' could be \textit{matrix transformations}, \textit{eigenvectors}, \textit{linear systems} and \textit{vector spaces} for instance. All of the fields and values are useful for refining user queries and providing both teachers and students with appropriate information.

Classrooms themselves are structured around units. Each unit represents a contained piece of material to be covered over a period of time, for instance one unit could be a week dedicated to the topic of cell structure in a biology course. Each unit contains a series of lessons (i.e. learning objects), that is, quizzes, guided tutorials, texts and videos. Units are initially inaccessible by students, but the teachers can toggle the availability of units in order to progressively allow students to gain access and work on classroom materials. Optionally, teachers can also make all units available from the outset. The choice depends on the preferences of the teacher and his or her needs for the class. Figure \ref{s2_fig4} demonstrates the relational structure between Classrooms, Units and Lessons without listing all the attributes of each class.


\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/classroom_structure.png}
	\caption{The relational structure between Classrooms, Units and Lessons.}
	\label{s2_fig4}
\end{figure}

Finally, important for the development of the ASAG module is the fact that classrooms come in two flavors, \textit{normal} and \textit{self-study}. Normal classrooms are exactly what one would expect, they have a teacher and a set of students, they start at a particular point in time, last for a semester or so and come to an end. Self-study classrooms on the other hand have no termination and no teachers. These represent MOOCs for which any student can register and work through the classroom units and lessons at his or her own pace. Having self-study classrooms means that an ASAG module must also work when no teacher is available to correct or grade student responses to short text items. 