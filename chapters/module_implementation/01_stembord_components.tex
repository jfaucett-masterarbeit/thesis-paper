\section{Stembord Components}

\subsubsection{Stembord Architecture and System Design}

Stembord is programmed in the Elixir programming language and is built on the Erlang virtual machine, which is designed for highly distributed and fault-tolerant systems \cite{elixir_language}. A MVC based web framework programmed in Elixir called Phoenix is used to implement the backend server API. The frontend GUI of the website is programmed in JavaScript, HTML and CSS.\footnote{It must be noted that Stembord has been developed for years now and multiple programmers have worked on the code presented in this section. It is not the sole work of the author.}

The main components of Stembord as well as those which are important for understanding the implementation of the ASAG module are outlined in this section. Those of most importance for grasping the architecture of the implementation are the system for managing learning objects (LOs) and the node grading subsystem.

The main components of Stembord are shown in Fig. \ref{impl_mainStembordComponents}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/users_roles_and_orgs.png}
	\caption{The Users, Roles and Organization structural components of Stembord.}
	\label{impl_mainStembordComponents}
\end{figure}

Every user of the system can be either a student, a teacher or both, just like in the real-world. This state is modeled by assigning a user a role having a \textit{role\_name} attribute of \textit{teacher}. All users on the platform have access privileges and the abilities of students, making \textit{student} an implicitly defined default role. An organization, for example a university, has many users, some of them are students, some are teachers and some are both. Users with a \textit{teacher} role can create classrooms and lessons for an organization. A classroom has many lessons but lessons can also be unassociated with a classroom and simply belong to the organization, a typical example of this scenario would be a single lecture or tutorial that a teacher would like to give on a particular topic independent of any specific class. Lessons and classrooms can be made publicly accessible and then any student, even those which do not belong to the organization, can study the materials available in these objects for themselves. Further, any teacher can copy a lesson or classroom, change it to fit his or her needs and then reuse it to teach a class or give a lecture.

\subsubsection{Lessons in Stembord}

The lessons module is the locus of core functionality on the Stembord platform, more precisely, it is the module which handles LOs such as quizzes and a myriad of learning content objects, for instance, texts, questions and media such as images or embedded videos. Figure \ref{impl_stembordLearningObjects} demonstrates the class structure of a subportion of this module. 

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/lesson_structure.png}
	\caption{The structure of LOs on Stembord. Lessons are made up of nodes. Lessons have metadata associated with them and a node can have a node question data structure if its \textit{NodeType} attribute designates it as being a question node.}
	\label{impl_stembordLearningObjects}
\end{figure}

Lessons belong to an organization and can be associated with a classroom through a join table making them independent of, but usable by, the classroom system. Lessons have metadata which stores the language of the lesson, for instance English, and a subject, which indicates the topic of the lesson, for instance computer science or biology. Lessons also have a UUID for globally unique access and a boolean field \textit{public} which when true allows other teachers on the platform to copy the lesson and other students to see, read and consume it.

All lessons are made up of a collection of nodes which are linked to one another in an ordered list fashion. Every node has content, which is a JSON formatted string holding the information in a structured machine processable format for rendering in the frontend web application. Nodes also store points which can range from $0$ to $1000$ and can be used by teachers to give grades for answering questions and completing quizzes. Each node has a \textit{NodeType} which designates what data is stored on the node and how the node behaves. There are currently node types for text and questions. A text node type can not only render any formatted text but also most mathematical latex, embedded images and URL links. This makes it flexible as a content type for creating LOs. When a node of type question is created an associated \textit{NodeQuestion} object is instantiated and a corresponding entry is stored in the database and associated with the node. This \textit{NodeQuestion} object can store a question prompt, an explanation of the correct answer, and a JSON formatted content text which varies by question type. The available question types are \textit{multiple-choice}, \textit{true-false}, and, after the deployment of the module developed in this thesis, \textit{short-answer}.

The classes in Fig. \ref{impl_stembordLearningObjects} are sufficient to store, display and consume LOs such as guided tutorials, quizzes and lecture videos. However, in order to provide educators with summative and formative assessment capabilities other structures such as student sessions and grades are necessary. These are shown in Fig. \ref{impl_stem_gradingSystem}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/grading_system.png}
	\caption{The Grading System in Stembord: StudentSessions, StudentSessionNodes and StudentSessionGrades}
	\label{impl_stem_gradingSystem}
\end{figure}

When a student starts working on a lesson, Stembord creates a \textit{StudentSession} object whose state is set to \textit{in-progress}. As the student then works through the lesson and completes nodes such as watching a video node, reading a text node or completing some question nodes, \textit{StudentSessionNodes} are created which store the responses of the student for that particular node in the session. Once the student has finished the lesson, the \textit{StudentSession} object's state is set to \textit{finished} and the object is visible to teachers in an interactive grade editor. Teachers can then review and adjust the student's points on a per node bases, if they so desire, and when they are finished, they mark the student's session as graded by clicking a labeled button in the editor which creates a \textit{StudentSessionGrade} object and stores the student's final grade for the LO in question.

\subsubsection{The Node Grading Subsystem}

Fig. \ref{impl_frontendSessionView} shows the student session view of a multiple-choice question node. When the user presses the check button his answer is submitted to the backend web application where it is graded and then returned to the user displaying the results of his choice, such as whether it is correct and the point total. This section discusses that entire process inside Stembord.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/06_implementation/stembord/stembord_quiz_view.png}
	\caption{The Stembord application view. This is the student's view inside a student session. He is getting ready to submit his answer to a multiple-choice question node by clicking the \textit{Check} button.}
	\label{impl_frontendSessionView}
\end{figure}

There is one endpoint which handles scoring nodes in Stembord, it is accessible via an HTTP POST request to the url \path{/session/nodes/:id/answer}, where \textit{:id} is the identifier of the node object. The frontend of Stembord, as seen in Fig. \ref{impl_frontendSessionView}, makes a request to the backend application server at this URL, sending the ID of the node along with the student's answer in the request body. The Phoenix Framework handles parsing the URL and the parameters and dispatching this request object to a controller method called \textit{answer()}. This method is shown in full in Lst. \ref{impl_student_sessionController_answer}. It fetches the session object and actual node from the database and passes the session, node and student's answer to the \textit{NodeGrader.grade\_item()} method call. The score obtained from this call is returned to the frontend as a JSON response where it is then rendered for the student.

\begin{lstlisting}[language=Elixir,
			   caption={The \textit{answer()} method inside the student session controller.},
               label={impl_student_sessionController_answer}
               ]
defmodule StembordWeb.Students.StudentSessionController do

    def answer(conn, %{"id" => id, "answer" => answer}) do

        # fetch the student user and current lesson
        user = conn.assigns[:user]
        lesson = conn.assigns[:lesson]

        # fetch the session and the node from the database
        with {:ok, %StudentSession{} = student_session} <-
            StudentSessionService.get_session(user, lesson),
            %Node{} = node <- NodeService.get_by(lesson.id, id),
            {:ok, %StudentSession{} = student_session} <-

            # grade the student's answer inside the current session
            NodeGrader.grade_item(student_session, node, answer) do

        # render the result as JSON
        conn
        |> render("show.json", student_session: student_session)

        end
    end
end
\end{lstlisting}

The \textit{NodeGrader.grade\_item()} function always receives three objects, the student's current session, the question node within that session and the answer the student has submitted. Within a database transaction, it then grades the answer, stores the result on the session node, marks the node as completed and returns the updated session object to its caller. This process is shown in Lst. \ref{impl_nodeGraderService}


\begin{lstlisting}[language=Elixir,
			   caption={The function which grades each question node and updates the database entries.},
               label={impl_nodeGraderService}
               ]
defmodule Stembord.Services.NodeGrader do

    def grade_item(
            %StudentSession{id: student_session_id},
            %Node{id: node_id, question: question, points: points},
            user_input
            ) do

        # begin a database transaction
        Repo.transaction(fn ->

            # get the student session node
            student_session_node =
            StudentSessionNode.Api.get_by!(
                student_session_id: student_session_id, 
                node_id: node_id)

            # compute the raw grade
            raw_grade = score_question(question, user_input)

            # store the scaled and raw grades on the 
            # student session node
            StudentSessionNode.Api.update!(student_session_node, %{
                student_completed: true,
                student_grade: raw_grade,
                student_points: Kernel.round(raw_grade * points)
            })

            # fetch the updated student session from the database
            # and return it
            StudentSessionService.get!(student_session_id)
        end)
    end
end
\end{lstlisting}

Finally, the method \textit{score\_question()} needs to be discussed. It uses Elixir's pattern matching capabilities to dispatch to a different function based on the question type of the given node. The code displayed in Lst. \ref{impl_nodeGraderService_TrueFalseExample} shows how this is implemented for the simple true-false question type.

\begin{lstlisting}[language=Elixir,
			   caption={The \textit{score\_question()} function matches on \textit{NodeQuestion} objects which have a \textit{question\_type} attribute of :true\_false. It then passes the parameters down into the \textit{is\_true\_false\_correct()} function, which checks to see if the student's answer is the same as the answer the teacher has set in the question\_content ``answer''\ field. If this is true a perfect $1.0$ is returned. Otherwise a failing $0.0$ is returned.},
               label={impl_nodeGraderService_TrueFalseExample}
               ]
defmodule Stembord.Services.NodeGrader do

    def score_question(
            %NodeQuestion{question_type: :true_false, 
                          question_content: question_content},
            user_input
        ) do
        is_true_false_correct(question_content, user_input)
    end

    # ...

    def is_true_false_correct(question_content, user_input) do
        if question_content["answer"] == user_input do
            1.0
        else
            0.0
        end
    end

end
\end{lstlisting}

The flow of information is the exact same for other question types except that other types have their own corresponding \textit{score\_question()} and implementation functions. There are particular versions for the multiple-choice and short-answer question node types, however, they are much longer and do not provide extra information about the architectural design.

For short-answer, the question type developed in this thesis, its \textit{score\_question()} function makes a call to an external API service written in Python. The reasoning behind this is two-fold. First, Elixir cannot do text processing well and Python is a much better language with many more robust libraries for handling all the text processing needs of such a module. The second reason is that there is a lot of code required to implement short-answer scoring and possibly other complicated question types in future versions, so for the sake of modularity and scalability it was decided to externalize complicated question types to their own API service. This external API service is described in the following section.