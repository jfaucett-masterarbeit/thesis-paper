\section{Quiz API}

An external REST API was programmed in Python using the Django Framework \cite{django_framework} in order to offer ASAG grading services to the Stembord backend application in a decoupled and modular manner. The following sections discuss the layout, design and core classes implemented by this API.\footnote{All of the architecture and code presented in this section is exclusively the author's own work. This is the ASAG module service researched and developed for this thesis.}

\subsection{Rest API Architecture}

At the time of this writing, the REST API offers only two URL Endpoints which are listed below.

\begin{enumerate}
\item \textbf{Short-Answer}: \path{/api/question_types/short\_answer}
\item \textbf{Fill-In-The-Blank}: \path{/api/question_types/fill_in_the_blank}
\end{enumerate}

These two endpoints taken together allow for automatic grading of short answer responses. \textit{Fill-In-The-Blank} is an alternative created to allow teachers to model the very short short-answers discovered during explorative data analysis in subsection \ref{Subsection_Dataset_Annotation}. \textit{Short-Answer} handles all other automatic short-answer response grading.

The quiz API is designed with a modular architecture allowing for easy decoupled development and maintenance. View handlers delegate incoming requests to underlying modules designed to grade specific question types in quizzes, for instance, \textit{short-answer}. Fig. \ref{impl_shortAnswerFlow} shows a conceptualization of the information flow for the \textit{short-answer} endpoint.

\begin{figure}
	\centering
		\includegraphics[scale=0.45]{images/06_implementation/Information_Flow.png}
	\caption{Demonstrates architectural flow of information. The Stembord Backend Client makes an HTTP request to the Quiz API Server in order to grade a \textit{short-answer}. The server dispatches the request to the \textit{ShortAnswerHandler} class which checks and validates the input parameters before sending the information further down the pipeline to the ASAG Module itself. The ASAG Module computes the score given the input and sends the results back to the handler. The handler then sends this information back to the Quiz API and Django handles sending the response JSON object to the calling service, in this example the Stembord Backend Client.}
	\label{impl_shortAnswerFlow}
\end{figure}

The flow of information from request to handlers, to question module and back up the pipeline remains the same for every other endpoint in the API. Each endpoint does, however, require a different set of parameters, specific to the endpoint, at its interface to the calling client. The returned response is always a JSON object consisting of a standardized set of fields. The contents of this returned JSON response object are shown in Lst. \ref{impl_endpoint_responseObject}. The response contains grading scores, feedback and debugging information.

\begin{lstlisting}[language=Python,
			   caption={Quiz API JSON Response. Every endpoint returns three values: a score, split up into raw and scaled scores, a potentially non-empty array containing feedback information such as that the student response contains a spelling mistake, and a log field which holds debugging information.},
			   label={impl_endpoint_responseObject}
			   ]
response = {
    'score': {
        'raw': 0.8,
        'scaled': 16
    },
    'feedback': ['spelling error'],
    'log': []
}
\end{lstlisting}

Since this thesis is about researching and developing an automatic short answer grader, the details of the \textit{Fill-In-The-Blank} module are not discussed further, but instead a focus is made on the \textit{Short-Answer} module. The \textit{ShortAnswerHandler} class requires that its clients send several parameters which are important for further processing in the pipeline. These parameters can be seen in Lst. \ref{impl_handlerParameters}.

\begin{lstlisting}[language=Python,
			   caption={Parameter schema for \textit{ShortAnswerHandler} class. A model answer, student answer, points\_available, and method are required parameters. The \textit{points\_available} parameter holds the points which a student can achieve if he answers 100\% correctly. Its an integer value, for instance, $10$. The \textit{method} allows the caller to choose an ASAG approach, for instance, \textit{Cosine\_Coefficient} could be chosen instead of machine learning. Some of these approaches have various options such as whether to use stemming or lemmatization as a preprocessing step. These are given by the \textit{method\_options} parameter. Finally, the language can be set although it defaults to English and the API currently only offers English.},
			   label={impl_handlerParameters}
			   ]
    s = Schema()
    s.field('model_answer', Type.STRING, required=True)
    s.field('student_answer', Type.STRING, required=True)
    s.field('points_available', Type.INTEGER, required=True, coerce=True)
    s.field('method', Type.STRING, required=True)
    s.field('method_options', Type.STRING)
    s.field('language', Type.STRING)
    s.field('debug', Type.BOOLEAN)
\end{lstlisting} 

The full handler implementation for short-answers is shown in Lst. \ref{impl_handlerImplementation}.

\newpage

\begin{lstlisting}[language=Python,
			   caption={Implementation of the \textit{ShortAnswerHandler} class. First, the language is set, then an ASAG scoring class is created for the given method, for instance, \textit{Cosine\_Coefficient} or \textit{Machine\_Learning}. This scorer then scores the \textit{(model-answer, student-response)} pair returning a raw score. The raw score is then scaled and normalized and a response is constructed as seen in Lst. \ref{impl_endpoint_responseObject}. If the debug parameter has been set, a log of the details of the preprocessing and scoring processes which occur inside the \textit{scorer.score()} call are returned.},
			   label={impl_handlerImplementation}
			   ]
class ShortAnswerHandler(AbstractHandler):

    def call(self):

        # set the correct language
        i18n.set('locale', self.settings['language'])

        # create the scorer given the method
        scorer = create_scorer(self.settings)

        # score the questions
        raw_score = scorer.score()
        
        score_normalizer = ScaleAndRoundScorer(
            self.settings['points_available'])
        scaled_score = score_normalizer.scale(raw_score)

        # build the response
        response = create_response(raw_score, scaled_score)

        # append the log if requested
        if self.in_debug_modus():
            response['log'] = self.get_log()

        return response
\end{lstlisting}

In summary, the client sends an HTTP Post request with the required parameters. These parameters are then validated and parsed by the handler, this information is then passed down to the ASAG module or scorer, which in turn, instantiates the correct class for the given method parameter and its options. The scorer class then computes the scores and returns them to the handler. The next section describes the subprocess which takes place inside the ASAG scorer.

\subsection{ASAG Scorer Module}

The scorer module is instantiated by a \textit{create\_scorer()} function shown in Lst. \ref{impl_scorerClassInstantiation}. This function checks which methodological scoring approach the client has requested and instantiates a class to score the \textit{(model-answer, student-response)} pair using that particular approach. If, however, an invalid approach has been chosen an exception is thrown and an error is displayed to the client indicating that the request was invalid.

\begin{lstlisting}[language=Python,
			   caption={The \textit{create\_scorer()} function. It instantiates an ASAG object which handles scoring short-answer responses in a particular way. In this code example, the two options are the ``cosine\_coefficient'' approach examined in Exp. \ref{Subsection_Word_Overlap_Measures} and the hybrid machine learning model approach from Exp. \ref{Exp_ml_Classification_Scoring}. These are implemented by the \textit{CosineCoefficientModel} and \textit{MachineLearningModel} classes respectively.},
			   label={impl_scorerClassInstantiation}
			   ]
def create_scorer(settings):

    asag_method = settings['method']

    if asag_method == 'cosine_coefficient':
        return CosineCoefficientModel(settings)
    elif asag_method == 'machine_learning_model':
        return MachineLearningModel(settings)
    else:
        raise Exception("unrecognized ASAG method: {}".format(asag_method))
\end{lstlisting}

Each approach has its own set of options it can accept and its own class which handles the internal details of that specific way of doing ASAG. For the purpose of simplicity and illustration the \textit{CosineCoefficientModel} class is examined, however, each of the general steps applies to the other scorer classes as well such as the \textit{MachineLearningModel}.

Every ASAG class inherits basic functionality from the \textit{BasicModel} class and must implement its abstract score function. The CosineCoefficientModel class, which does just this, is shown in detail in Lst. \ref{impl_cosineCoefficientModel}.

\begin{lstlisting}[language=Python,
			   caption={The ASAG score function as implemented for the \textit{cosine\_coefficient} automatic scoring method. The \textit{(model-answer, student-response)} pair is preprocessed, debug information is logged if the debug parameter has been set, the cosine coefficient score is calculated and finally the raw score is returned with $1e-300$ added in the denominator to prevent division by zero exceptions.},
			   label={impl_cosineCoefficientModel}
			   ]
class CosineCoefficientModel(BasicModel):

    def score(self):

        ma = self.params['model_answer']
        sa = self.params['student_answer']

        # preprocess the model answer
        ma_t = word_overlap_preprocess(ma)
        ma_ts = set(ma_t)

        # preprocess the student response
        sa_t = word_overlap_preprocess(sa)
        sa_ts = set(sa_t)

        if self.in_debug_modus():
            self.handler.log_info(
                "[model answer preprocessed]: {}".format(ma_t))
            self.handler.log_info(
                "[student answer preprocessed]: {}".format(sa_t))
            self.handler.log_info(
                "[model answer as set]: {}".format(ma_ts))
            self.handler.log_info(
                "[student answer as set]: {}".format(sa_ts))

        # calculate the cosine coefficient score
        num = len(ma_ts.intersection(sa_ts))
        den = np.sqrt(len(ma_ts)) * np.sqrt(len(sa_ts))

        # prevent ZeroDivisionError
        return num / (den + 1e-300)
\end{lstlisting}


As seen in the handler implementation in Lst. \ref{impl_handlerImplementation} this raw score is then scaled and returned in the JSON response object back to the client caller. 