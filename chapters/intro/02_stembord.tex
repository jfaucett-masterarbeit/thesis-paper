\section{Learning Management Systems (LMS)}

This section forays into the depths of computational learning systems. Over that journey an exploration of concepts in Learning Management Systems (LMS), Intelligent Tutoring Systems (ITS) and the role of artificial intelligence in education is conducted in order to elucidate the functionality of Stembord, an Intelligent Learning System, to argue for the necessity of the chosen methods and to characterize the context imposed by the Stembord system on current and future development of an ASAG module.

\subsection{Definition and Function of LMS Software}

In the 1950s, long before computers began to clutter the pockets and desks of society, computational technology was already being applied in education \cite{watson_2007}. A Learning Management System (LMS) is one approach of many towards the application of computers in the educational domain. Aside from LMSs, there are many other generic terms in the literature for this circumstance, such as computer-based instruction (CBI), computer-assisted instruction (CAI) and computer-assisted learning (CAL), all of which outline various programs for drilling exercises or individualizing instruction \cite{watson_2007}. The distinction between these terms and LMS is that an LMS goes beyond instructional content and offers management, tracking, personalized instruction and integration across the entire system \cite{watson_2007}. A LMS is a framework that handles every facet of the learning process in an organization or institution. It delivers the instructional content, assesses and identifies individual goals and training. LMSs track individual and classroom progress and collect and present interpretable data so that students and educators, through clearer information, can better understand the various states of knowledge and skill acquisition by users of the system \cite{watson_2007}.

There is much confusion in the literature as to the precise nature of an LMS and its distinguishing characteristics from other similar technologies such as Content Management Systems (CMS), for example Blackboard, and Learning Content Management Systems (LCMS). The key difference is that an LMS manages the learning process as a whole, it provides the rules, whereas a CMS and LCMS are devoted to the content and learning objects (LO) themselves such as creating lessons and tutorials - they provide the content \cite{watson_2007}.

Some of the key features which most LMS solutions provide are listed below \cite{coates_2005}.

\begin{enumerate}
\item Asynchronous and synchronous communication (chat, e-email, instant messaging, discussion forums)
\item Content development and delivery (Learning Objects, links to internet resources)
\item Formative and summative assessment (assignment submission, testing, collaborative work and feedback)
\item Class and user management (registration, enrollment, timetables, managing student activities)
\end{enumerate}

These features demonstrate the broad reaching scope of LMSs as these systems are designed to support the productivity and effectiveness of all stakeholders in the educational context: students, teachers and administrators alike. 

\subsubsection{Learning Objects}

Learning objects (LO) are the smallest unit of content within a CMS or LCMS. They are powerful because they offer reusability across systems and contexts \cite{watson_2007}. An example of a learning object could be a lesson on ``Taking square roots'' or a quiz on ``The biological organization of the cell''. Each of these LOs can be used within a classroom, but also shared and used by other teachers and educators within their classrooms, or even be studied solo by a student simply interested in learning about taking the square roots of numbers or understanding the structural organization of a cell. Overall, there are four properties of LOs. They are:

\begin{enumerate}
\item \textbf{Reusability}: a LO once created can be used in any course forever.
\item \textbf{Generativity}: a LO or quiz can be used to create new instructional content.
\item \textbf{Adaptability}: a LO can be given to a student based on his skill level.  
\item \textbf{Scalability}: a LO can scale to meet the needs of small or large audiences without an increase in costs.
\end{enumerate}

It is important to note that LOs are not just lessons but can be quizzes, interactive visualizations and even full exams, that is, they are modular structural units of teaching and learning. They are the building blocks, the grammar which provides the combinatorial productions of the language of learning systems.

\subsubsection{ASAG's relationship and role in LMSs}

In the LMS, LCMS or CMS systems on the market, short-answer responses are, to the best of the author's knowledge, ubiquitously introduced as a further question type which teachers have for designing their LO quizzes and exams. Curricula designers create a question of type \textit{short-answer} and then have an options menu for configuring the question. Some systems such as Moodle allow for automatic grading of the student responses. In Moodle, for instance, the teacher can create a list of regular expressions which are then matched against student input to calculate a score \cite{moodle_asag_2018}. Blackboard, the most widely used CMS system for education in the US, as well as other open source solutions such as OpenOLAT do not provide ASAG, however, Blackboard still requires teachers to enter a reference answer in order to give students formative feedback \cite{blackboard_asag_2018,watson_2007}. From a software design perspective, most of these production systems are closed source which makes it impossible to assess how they incorporate a short-answer module into the architectural design of the fully realized system, however, Moodle is a notable open-source exception which includes a \textit{short-answer} module.

\subsubsection{Reasons for LMS usage and adoption uptake}

Learning Management Systems have seen rapid adoption across the globe despite the complexities, costs and risks involved in the adoption process \cite{coates_2005}. When an institution endeavors to adopt a LMS, it is not partaking in a low-risk decision making process. This resolution involves institutional and technological forecasting, restructuring of complicated administrative, educational and technological issues, a consideration of the interests of a deluge of disparate stakeholders and often a reconsideration of institutional policies and procedures, not to mention new guidelines for accountability and control \cite{coates_2005}.

Despite all of this, many universities have decided to adopt LMS solutions. Clearly, these institutions see potential for increased productivity along some dimensions otherwise they would not take the risks and undergo the hurdles in order to adopt these systems. Broadly speaking, the most commonly listed reasons for adoption are access, cost and quality although these core areas can be subdivided into a plethora of more specific factors \cite{coates_2005}.

A detailed assessment of the varied reasons for LMS adoption is tangential to the subject of this thesis, however, to list just a few, Coates identifies such factors as a means of delivering large-scale resource-based learning programs, flexible course delivery, increased teaching efficiency, reduction in course management overheads, enriched student learning, automatic and adaptive assessment and competitive pressure between institutions to meet demands \cite{coates_2005}. 

Regardless of the causes, LMSs have permeated the educational domain and it appears they will stay for a while, transforming and interacting with the academic landscape into the future.

\subsection{Definition and Function of Intelligent Tutoring Systems}

The goals of Intelligent Tutoring Systems (ITS) are similar to those of an LMS: to provide services which support learning, however, for an ITS these services are specifically tailored for the tutoring context \cite{nkambou_2013}. Research in the field began in the 1960s and '70s with the term ``Intelligent Tutoring Systems'' eventually being coined by Sleeman and Brown in 1982, followed in the same decade by the first ITS conference in 1988. One motivator for the field was research published by Bloom in 1984 which demonstrated that individual tutoring is twice as effective as group teaching \cite{bloom_1984, nkambou_2013}. In the first two decades of its incipient rise, ITS was further driven by visionaries who dreamed that every child would ``have access to ... the personal services of a tutor as well informed as Aristotle'' (Suppes, quoted in Nwana 1990). Into the 90s ITS had begun to establish itself as an engineering design field with scientific foundations to its research \cite{nkambou_2013}.

In order to model tutoring, ITS use a four-component architecture which can be seen in Fig. \ref{lms_ITS_fourComponents}. The fourth component is the user interface and is ignored in this section since its not useful in understanding how intelligent systems function as a whole.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/section_02/four_component_arch.png}
	\caption{The four-component architecture view of ITS.}
	\label{lms_ITS_fourComponents}
\end{figure}

The \textbf{domain model} contains the knowledge base and reasoning faculties. It stores the concepts and rules as well as problem-solving strategies within the domain to be learned and is expected to adapt explanations of its reasoning to the learner. Knowledge elements in the domain model can be linked together as lessons or more loosely as dynamic curricula. There are many different structures for storing this information such as semantic networks, frames, ontologies and production rules \cite{nkambou_2013}.

The \textbf{student model} stores knowledge about the student's affective state, his current knowledge on the topic and his evolution through the learning process. The student model must gather explicit and implicit data about the learner, synthesize that data to create representations of the student's knowledge and state in the learning process, and it must account for the data by performing diagnosis and selecting optimal pedagogical strategies for the presentation of subsequent learning materials to the student \cite{nkambou_2013}.

Lastly, the \textbf{tutoring model} interacts with the student and domain models and determines which actions and tutoring strategies to take. It must make decisions about if and when to intervene, and how and when it should deliver learning content. These can be Socratic dialogues, feedback or hints, but also simulations and visualizations \cite{nkambou_2013}.

\subsection{Stembord Project}

Stembord is a combination of an LMS and an ITS. Its goal is to synergize the concepts of ITS and LMS into a system which provides learners and educators with resources optimized for their educational needs and adapted to their cognitive states. The project was started by the author of this paper and is currently developed and maintained by the author as well as his brother.\footnote{A short piece of prose outlining the Stembord project's vision for the future can be found in the appendix in section \ref{Misc_Stembord_Project_Vision}}

\subsubsection{Immediate and Long-Term goals of Stembord}

Some of the long-term goals of Stembord from a single student's perspective entail such features as adaptive and dynamic lessons, student progress and affective modeling, knowledge construction, natural language dialogue in multiple languages and high availability for students. However, students are not the only stakeholders Stembord is concerned with. Others are parents, teachers, as well as school administrators, each of which would benefit enormously from systems designed with the tenets of cognitive psychology to promote transparency, engagement, mental growth and reduce bias. However, in order to achieve these long-term goals, many diverse building blocks are necessary. Chief among these is a need for quality LOs such as quizzes, guides and interactive simulations in every domain from physics to literature, not to mention labeled knowledge bases and datasets for handling one-on-one student learning sessions, eventually between a Stembord AI tutor and the student. Yet before any query can be made by an AI tutor, there must also exist a database over which to query. Stembord also needs to build student models so that it can discern the affective and progress states of each student, querying and adapting lesson plans and learning pathways to the skill-sets, faculties and needs of each student dynamically.

In order to achieve these far reaching goals, Stembord has decided to focus initially on the educator side of the problem by building tools which allow teachers to design eloquent and semantically labeled LOs and combine them to construct online courses, exams and guided tutorials. From an ITS view, one could see this as a focus on building a broad and deep domain model backed by a robust knowledge base first. Only later, is the goal to then progressively add student modeling, interactive natural language dialogue, the tutor model, as well as school administration capabilities such as enrollment, scheduling, etc. None of these later features are currently implemented in Stembord. However, any new module, particularly the ASAG module, must take these long-term architectural goals into account in order to be an effective component in future versions of the end-product.

In the near future then, Stembord is focused on teachers and their needs in the classroom. One of these needs is quiz and test creation and management functionality. This is where ASAG comes into play. With the current quiz-maker in Stembord, teachers can create multiple-choice and true-false questions and have them automatically graded, yet this capability does not currently exist for short-answer response question types. Therefore, this thesis researches the problem of ASAG and of integration of an ASAG module into this quiz-maker system of Stembord.

Also of importance, is that Stembord should eventually offer all its functionality in as many of the world's languages as possible. The reasoning for this is simple, namely, that the developers of Stembord want anyone regardless of location or native language to be able to acquire knowledge they need to improve their lives with as little friction and as low a cost as possible. Providing Stembord resources, features and functionality which works for a user's native language serves to reduce barriers and lower hurdles which are irrelevant to the user's main task which is acquiring knowledge about some topic, for instance, mathematics or computer programming.