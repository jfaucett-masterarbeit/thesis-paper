\section{Machine Learning}

The purpose of the experiment in this section is to combine all the insights gained in the research up to this point and explore how much ASAG performance can be improved by using a combined and supervised approach. Additionally, insights gained from labeling student responses in the dataset are used in order to determine how much performance could potentially be gained by detecting such attributes of a given student response. For instance, does knowing that a student response misses an important concept - according to the added hand-annotations of the author - increase the ability of a machine learning model to assign a correct score to the student response?

\subsection{Exp. 1: Hybrid Approach with Machine Learning} \label{Exp_ml_Classification_Scoring}

\subsubsection{Exp. 1: Background, Reasoning and Hypotheses}

It has already been established that using multiple model answers, \textit{n}-grams of size \textit{n} up to $3$ and \textit{FastText} word embeddings are all the best ways to model automatic short answer scoring in an unsupervised manner. These methods thus provide information for feature engineering in a hybrid machine learning solution. 

Initially, many different features and learning algorithms were tried but regardless of the approach, six class scoring could not be learned by a machine learning model, it proved far too difficult for the models to differentiate amoung the various scores between correct (5) and wrong (0). Therefore, 3-class classification was attempted for the classes \textit{wrong}, \textit{partially\_correct} and \textit{correct}. This time the data could be learned, however, the models were still not robust enough against valid yet unrelated student responses, since wrong student responses and responses with little to no thematic relation to the question are essentially absent from the dataset. Therefore, the dataset was extended using some unrelated student responses. These were sentences drawn from the Brown corpus. They varied in length and content, but served to simulate a student typing in a sentence or response that is not correct and not thematically related to the topic at hand. Additionally, some wrong answers which were related to the topic were also entered in order to even out the distribution of the dataset. After these measures were taken the trained model seemed to behave reasonably well using sample text inputs from the user. Importantly however, the evaluation statistics and upper bounds change due to this altered dataset. These statistics are given for this 3-class classification problem in the appendix in Tab. \ref{ml_new_upperBounds}.

The following describes the features which are used as input to train the model. These are almost entirely taken from the results of the previous experiments.

\paragraph{Features}

\begin{enumerate}
\item \textbf{N-Gram Entailment}: FastText Word Embeddings using entailment scoring and \textit{n}-grams where $n \in \{1,2,3\}$ as detailed in subsection \ref{Subsection_Word_Embeddings} and shown in Fig. \ref{sem_vsmNGramModelScores}.
\item \textbf{Cosine Coefficient}: The cosine coefficient word overlap was chosen for its stricter interpretation and scoring of surface structure compared to other techniques such as the faucett coefficient. This is taken directly from the results presented in subsection \ref{Subsection_Word_Overlap_Measures}.
\item \textbf{Cosine Coefficient Not}: The same as the above except all negated words are prefixed with a \textit{not\_} until a punctuation break is reached. This is a technique used to extract differences between sentences with negation as in \textit{John is a Sailor} and \textit{John is not a Sailor}.
\item \textbf{Length Difference}: Difference in length between the model and student answers was chosen to inform the relationship between total information in the \textit{model-answer, student-response} pair and to roughly determine whether or not the student response contains enough detail. This feature was suggested in research done by Sultan et al \cite{sultan_salazar_sumner_2016}.
\item \textbf{BoW Count Vector N-Grams}: Bag-of-Words count vectors made of lemmatized \textit{n}-grams where $n \in \{1,2,3\}$ were used to extract repetitive and ordered surface structure missed by the cosine coefficient. 
\item \textbf{Token Edit Distance}: measure utilized to extract word order information, this is the suggested best measurement from the research results of section \ref{Section_Syntactical_Structure}.
\item \textbf{Word Mover Similarity}: used a FastText model to calculate Word Mover Distance, a measure of the amount of distance a student response has to travel to convert its meaning into the meaning of the model answer. Used to measure a numerical similarity score between two sentences or phrases. The \textit{FastText} model is used because of the conclusions drawn from subsection \ref{Subsection_Word_Embeddings} and the distance metric is one of several suggested by research done by Adams et al \cite{distributed_vector_representations_2016}.
\end{enumerate}

These features are chosen based on their correlation with the human assigned score values, the results many of them have offered in previous experiments and what the literature on the topic suggests. Those features which correlate highly with the gold standard scores are chosen. For example, the token edit distance is chosen because it negatively correlated to the highest degree of all the other syntax measurements explored, higher than the tree, POS and dependency parse edit distances.

Additionally, for comparison purposes two further features are added: \textit{Missed Concept} and \textit{Partially Missed Concept}. These features are not practical or capable of being implemented by a machine at this time but are used in order to determine how well a hypothetical system could potentially function given the ability to extract these features. They are drawn from the annotated labels assigned in the process of explorative data analysis discusses in section \ref{Subsubsection_Extra_Annotations} and are described in Tab. \ref{MatMeth_gradeReasonLabels}. The goal is to evaluate how much improvement can be gained by knowing if a student missed a complete concept or just a portion of a concept in a given response.

The models which are evaluated are the Multilayer Perceptron (MLP), Logistic Regression (LR) and Random Forest (RF).

\subsubsection{Exp. 1: Presentation of Results}

\subsubsection{Exp. 1: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

As presented in Fig. \ref{ml_accuracyScores}, one can see that this approach does offer a performance benefit over previous methods. In the plot on the right one can further see that by adding the two features \textit{Missed Concept} and \textit{Partially Missed Concept} the model performs roughly as well as a human grader. Its unfortunate that at the current time there is no sophisticated way - beyond the pattern matching approach - to determine whether a student response has missed a part or the entirety of a concept.

\begin{figure}
	\centering
		\includegraphics[scale=0.3]{images/05_experiments/05_machine_learning/model_comparison_for_class3.png}
	\caption{This compares the results using the regular features across all three models vs. the results obtained by adding the \textit{Missed Concept} and \textit{Partially Missed Concept} features. Both show significant improvements over the previously studied approaches, however, by adding the concept features the models are able to attain human level grading capabilities.}
	\label{ml_accuracyScores}
\end{figure}

\paragraph{TES.II: Time and Space Complexity}

This approach is time and space intensive during the training phase. However, during evaluation it is sufficiently fast since creating the features for one \textit{(model-answer, student-response)} pair can be done in the worst performing time of the constituent features and is polynomial. There is an initial startup lag required to load the word embedding models, however, the model needs only to be loaded once and thus, this lag only affects an application's startup time.    

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

This approach also benefits from using multiple model answers. It was found that a modest boost of around $2\%$ could be had by prompting the content creator to entire multiple model answers. However, this boost is not so significant that it warrents a requirement that teachers entire multiple model answers. Therefore, this approach is largely low effort from the teacher's perspective. It is true, however, that they must first hand-grade a set of student responses and only then can the model be trained. 

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

This approach doesn't give very informative feedback. It is limited in scope like all other approaches except the pattern matching method, in that it can only show the users of the system whether the student response is correct, wrong, or somewhere in-between.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

This approach works excellently for \textit{self-study} classrooms in Stembord. A model can be associated with a MOOC and after that point all short-text student responses can be automatically graded. For classrooms with a teacher, the process is initially more difficult since it can only be offered after a first version of the course has been completed and the teacher has already graded a set of student responses. 

\paragraph{Effectiveness Across Languages}

All of the features used to build this model are easily transferable to other languages, this is mainly because the approach uses word embeddings to extract meaning and does not rely on knowledge based semantic approaches.

\subsubsection{Conclusion}

The hybrid approach shows marked improvement over the unsupervised methods explored in previous experiments although it only works for 3-class automatic scoring (and consequently pass/fail scoring as well). This experiment shows that \textit{missed concepts} play a vital role in the scoring process. Further, it shows that given an ability to recognize \textit{Missed Concept} and \textit{Partially Missed Concept} a machine learning model can, in practice, perform roughly equivalently to a human grader for 3-class scoring. Excluding the ability to distinguish missed concepts, there is still a significant amount of room for improvement in the supervised approach. Overall, the choice of machine learning model does not appear to make much of a significant difference. The Multi-layer Perceptron model works best for the standard features, and the logistic regression model is best when the features are extended to use \textit{Missed Concept}. However, these differences are small percentage points and, in the author's opinion, largely negligible.