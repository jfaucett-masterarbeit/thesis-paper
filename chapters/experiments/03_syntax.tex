\section{Syntactical Structure} \label{Section_Syntactical_Structure}

The previous experiments left two directions open which need more exploration: syntax and semantics. This section explores the former of these two. It is also unique amoung the experiments in that it does not attempt to score short answers but only tries to find correlations between syntactical measurements of \textit{(model-answer, student-response)} pairs and scoring values. This is because assigning a grade based on syntactic similarity alone makes very little sense in the context of ASAG. To clarify this assertion, consider Fig. \ref{syntax_whyNotScores} which shows a hypothetical nonsensical student response and demonstrates how the meaning of two sentences can be completely divorced even though both sentences share the exact same syntactic structure.

\begin{figure}
	\centering
		\includegraphics[scale=0.6]{images/05_experiments/03_syntactic_structure/why_not_scoring2.png}
	\caption{Observe how the highlighted POS (Part-of-Speech) tags are not only identical but also share
    the exact same order in the \textit{(model-answer, student-response)} pair despite the fact that both sentences represent completely unrelated ideas. This demonstrates the lack of usefulness of running experiments which use syntax alone to grade student responses.}
	\label{syntax_whyNotScores}
\end{figure}

\subsection{Exp. 1: Edit Distances and Bleu-Score}

\subsubsection{Exp. 1: Background, Reasoning and Hypotheses}

These experiments compare the edit distance for sentences parsed into lemmas, POS tags and dependency parse tags. The tree edit distance is used as well the bleu score. These represent five distinct ways to extract syntactic meaning and relationships from \textit{(model-answer, student-response)} pairs. The structure of the experiment is similar to previous ones, preprocessing is done for punctuation and stopword removal and whitespace and case normalization. This time, however, instead of stemming, lemmatization using the spacy library is applied since it can more effectively normalize tokens into their root forms than stemming in many cases. After this step, each of the measurements are calculated between model answer and student response for all the $n=2010$ items in the dataset.

\paragraph{Hypotheses}

Due to the fact that the bleu score uses \textit{n}-grams and based on the previous \textit{n}-gram experiments, it is hypothesized that the bleu score will have a positive correlation to the target variable. This is due to the implementation details of the blue score \cite{bleu_score_2001}. The other methods are much more ambiguous, however, it is clear that as the costs increase for the edit distance set of functions so too do the syntactical differences between model and student answers. The second hypothesis is that syntactical difference should correlate negatively with score for these methods, since two sentences which are syntactically very different are likely to be semantically different as well, at least that is the line of thought. These are formalized as follows:

\begin{enumerate}
\item \textbf{$H_{1}$}: The bleu score will correlate positively with the target variable. 
\item \textbf{$H_{2}$}: The edit distance scores, including the tree edit distance, will correlate negatively with the target variable.
\end{enumerate}

The strength of these correlations is entirely unknown and thus determining these strengths, directions and acquiring a deeper understanding of syntax measurements is the main purpose of this experiment.

\subsubsection{Exp. 1: Presentation of Results}

\subsubsection{Exp. 1: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

The results for the bleu score are shown in Fig. \ref{syntax_bleuScore} which shows the histogram distribution of the blue score calculated on the entire dataset plotted against gold standard scores. Fig. \ref{syntax_bleuScore} also shows the same plot but only for a subportion of the dataset made up low scored student responses. It indicates that student reponses which receive lower scores are syntactically less similar to the model answers than student responses with higher scores even though there is still a lot of noise and overlap. 

\begin{figure}
	\centering
		\includegraphics[scale=0.45]{images/05_experiments/03_syntactic_structure/bleu_score_hist_and_scatter.png}
	\caption{\textbf{(A)} the histogram and scatter plot with correlation coefficient $r=0.247$ between the bleu score and target variable for the entire dataset.
    \textbf{(B)} same as \textbf{(A)} except only for the lower scores of the dataset i.e. those with failing grades or very low scores with $r=0.134$. The bleu score tends to be lower for lower scores. This indicates that the structure of lower scored student responses does indeed differ from those responses which receive higher grades.}
	\label{syntax_bleuScore}
\end{figure}

Overall Fig. \ref{syntax_bleuScore} shows that the bleu score could be helpful in modeling some parts of the syntactic structure, however, as the blue score decreases so does the correlation with scoring, which means that highly divergent sentences are not differentiated well using this method. $H_{1}$ is not rejected, however, since the bleu score does positively correlate with the target variable.

The correlation scores for the set of edit distance functions are shown in Fig. \ref{syntax_editDistanceCorrelations}. The confusion matrix indicates that higher edit distance scores for all the measures correlate with lower human assigned grades. This provides no evidence to reject $H_{2}$ and is consistent with the claim that student responses which syntactically diverge more from the model answers tend to receive lower scores.

\begin{figure}
	\centering
		\includegraphics[scale=0.45]{images/05_experiments/03_syntactic_structure/edit_distance_correlations.png}
	\caption{The token based edit distance (token\_based\_ED) and tree edit distance (Tree\_ED) provide the highest negative correlations with scoring. The token based and tree edit distances correlate the least with one another, indicating that they are exctracting different pieces of information from the underlying student responses. These two are likely good potential feature candidates for features extracting syntactic information in a machine learning model.}
	\label{syntax_editDistanceCorrelations}
\end{figure}

In the dataset, question 12.3 deals explicity with word ordering making it an ideal candidate to inspect for determining how well and to what degree these measurements function. Therefore, the token and tree edit distances in the context word order importance for grading is shown in Fig. \ref{syntax_editDistance_OrderExample} by using hand-picked examples out of question 12.3 to demonstrate the effectiveness of these methods. This figure demonstrates how higher edit distance measurements are indicative of poorer human assigned grades and how this relates to the fact that student responses altough containing the correct words have the words in the wrong order.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/03_syntactic_structure/edit_distance_order_example2.png}
	\caption{Hand picked examples from question 12.3 from the Mohler v2.0 dataset. \textbf{(A)} perfect score and perfect syntactical match between student response and model answer; the Edit Distances (ED) are both $0.0$. \textbf{(B)} The ordering of the student answer differs slightly from the model answer, which forces the edit distances to increase in order to transform the student response into the model answer and this transformation cost is indicative of a lower human assigned grade, here $2.0$. \textit{(C)} An example where the student response is utterly different from the model answer and consequently has the highest token-based and tree edit distances.}
	\label{syntax_editDistance_OrderExample}
\end{figure}

\paragraph{TES.II: Time and Space Complexity}

Computing the bleu score has essentially the same time and space characteristics of the \textit{n}-gram approach and therefore runs in quadratic $O(n^2)$ time. Each of the other methods vary significantly. The token-based Edit Distance can run in $O(n\cdot m)$ where $n$ and $m$ are the two input sentences. The Tree Edit Distance is a dynamic algorithm and, using the Zhang and Shasha implementation runs in the worst case in $O({\lvert T_{1} \rvert}^2 {\lvert T_{2} \rvert}^2)$ \cite{tree_edit_distance_survey_2005}. The other approaches require running a POS tagger or dependency parser over the sentence initially which has a run time dependent on underlying implementation of the POS tagger or dependency parser. The edit distance portion run afterwards has a run-time complexity of $O(n\cdot m)$ just like the token-based approach. Overall, these methods are somewhat computationally expensive but still solveable in low polynomial time and hence effective and scalable for online grading systems.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

Extracting syntactic information requires no additional effort or expertise from educators other than the input of a single model answer. Therefore, in terms of teacher effort, the syntactical features examined here have zero costs. 

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Effectiveness Across Languages}

The methods here were not tested against other languages, however, the techniques themselves are capable of being applied to any language. Overall, there is no reason to expect that the effects seen here such as greater synactic divergence between model and student responses correlating with lower scores should be any different across languages.

\subsubsection{Conclusion}

The main insights gained are that the bleu score can identify the structure of correct answers which are syntactically similar to model answers and that the Tree Edit Distance and Token-Based Edit Distances are more effective than POS and Dependency Parse Edit Distances at extracting information about the syntax structure and relationship between model answer and student response. This is useful information for feature engineering.