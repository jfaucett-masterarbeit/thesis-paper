\section{Semantics} \label{Section_Semantics}

Having examined and discovered features for extrapolating syntactical relationships in the previous experiment, its now time to turn to the next problem area - semantics. As discussed in Ch. \ref{Material_and_Method}, there are, generally speaking, two ways of modeling synonymy and these are knowledge and corpus based approaches and distributed vector representations. The ASAG literature is full of many explorations in the former and that is where the experiments in semantics begin.

\subsection{Exp. 1: Knowledge and Corpus Based Meaning} \label{Subsection_Knowledge_and_Corpus_Based_Meaning}

\subsubsection{Exp. 1: Background, Reasoning and Hypotheses}

This experiment represents the first exploration of meaning beyond the surface orthographic structure of tokens. Its goal is to determine which knowledge and corpus-based semantic measurements, using \textit{WordNet}, are most effective at capturing the meaning relationships between words in different \textit{(model-answer, student-response)} pairs. In order to achieve this, this experiment compares knowledge and corpus-based similarity techniques outlined in Ch. \ref{Material_and_Method}, subsection \ref{MatMeth_Subsection_Semantics}. 

Importantly, the only other language besides English for which \textit{WordNet} is available, at least in this research, is Spanish. This means the knowledge and corpus-based approaches will not be extendable to other languages on the Stembord platform besides these two. Because of this, a legitimate question would be: why even attempt to assess these methods? The answer is three-fold. First, Stembord is currently only available in English and will remain so into the near future. Further, since its target market is the USA the next language to add is Spanish. Second, if these methods prove highly effective they could serve as an interim solution until more generalizable methods can be found. Additionally, obtaining insights from knowledge and corpus-based methods should help evaluate the effectiveness of other semantic approaches. Finally, it could be the case that the knowledge and corpus-based methods tested in this experiment model meaning in subtly different ways than other approaches and therefore provide additional context and information which could serve as a feature to boost the performance for the English and Spanish language versions of the ASAG module. Although this performance boost would not be available for other languages, it would still provide a better user experience for the main target audience for Stembord, at least currently and into the near future.

In terms of the setup, the Brown corpus\footnote{The Brown corpus is one of the datasets used by NLTK and can be downloaded at \url{https://www.nltk.org/nltk_data/}.} is used to calculate the Information Content for those measurements which require it (Resnik, Jiang \& Conrath, Lin). The preprocessing pipeline removes stopwords and punctuation, carries out whitespace and case normalization and performs POS-tagging which removes all tokens except nouns, adjectives and verbs. This last preprocessing step, is a prerequisite only because of the way \textit{WordNet} is structured, i.e. \textit{WordNet} only contains detailed graph hierarchy information on nouns and verbs and to a lesser extent adjectives and adverbs. Adverbs were left out since adding them had little to a slightly negative impact on the overall scores when assessed in preliminary experiments.

\paragraph{Hypotheses}

Based on the literature comparing knowledge and corpus-based measurements for the ASAG task, specifically Mohler 2009, it is hypothesized that the Jiang and Conrath extension of the Resnik similarity will perform the best on the dataset \cite{Mohler_2009}. However, this experiment runs the measurements made by Mohler 2009 against a new and extended dataset, as well as one that has been pruned of easy to automatically score short answer responses, so the results could prove different when run over this different data. The second hypothesis is that adding up to three model answers will significantly increase the accuracy of the scoring methods. This is based off of previous results seen in the \textit{n}-gram comparisons experiment. There, using three model answers instead of one increased the overall accuracy by almost $10\%$. Initially, there is no reason to expect anything different in this experiment, therefore, $H_{2}$ states this expectation. Formally, the hypotheses are:

\begin{enumerate}
\item \textbf{$H_{1}$}: The Jiang and Conrath similarity will be the best similarity measure followed by the Shortest Path, Lin, and Wu and Palmer similarity scores.
\item \textbf{$H_{2}$}: Using $3$ model answers instead of $1$ will increase model accuracy by nearly $10\%$.
\item \textbf{$H_{3}$}: Correct student responses which use synonyms and are phrased differently than the model answer will be detected, more so than in lexical or \textit{n}-gram techniques.
\end{enumerate}

\subsubsection{Exp. 1: Presentation of Results}

\subsubsection{Exp. 1: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

Fig. \ref{sem_kbAccuracyScores} shows the accuracy scores for the various measurements. Interestingly, the Wu and Palmer (WP) similarity score performed the best of the bunch. However, the measure with the highest correlation coefficient is the Jiang and Conrath, as shown in Tab. \ref{sem_kb_correlationCoefficient}, followed closely by the shortest path which is consistent with $H_{1}$.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/04_semantics/kb/class6_accuracy_scores.png}
	\caption{Accuracy scores for Shortest Path (SP), Leacock \& Chodorow (LCH), Wu \& Palmer (WP), Resnik (RES), Jiang \& Conrath (JCH) and Lin (LIN). The Lin technique, which uses a combined approach of both the \textit{WordNet} knowledge base and Information Content from a text corpus, was more lenient than JCH at the cost of some precision. Overall, the WP measure provides the best balance between precision and recall for the ASAG task on this dataset.}
	\label{sem_kbAccuracyScores}
\end{figure}

These results are fascinating because they give some deeper insight beyond what Mohler (2009) describes.
For instance, the scores in Fig. \ref{sem_kbAccuracyScores} and correlation coefficients in Tab. \ref{sem_kb_correlationCoefficient} indicate that of the class of techniques which only make use of \textit{WordNet} and ignore statistical corpora, Wu and Palmer (WP) performs the best, sacrificing some precision compared to the Shortest Path (SP) in order to not falsely give a student response a lower score than it deserves. This makes sense as well, because the WP method uses a common concept (Least Common Subsumer) shared by two subconcepts which makes the measurement slightly fuzzy compared to the shortest path technique which uses node distances between concepts only.


\begin{table}
\begin{small}
\centering
    \begin{tabular}{ | l | l | }
    \hline
     \textbf{Measure} & \textbf{R} \\
    \hline
    \textbf{Shortest Path} & $0.28$ \\
    \hline
    \textbf{Leacock \& Chodorow} & $0.06$ \\
    \hline
    \textbf{Wu \& Palmer} & $0.13$ \\
    \hline
    \textbf{Resnik} & $0.16$ \\
    \hline
    \textbf{Jiang \& Conrath} & $0.32$ \\
    \hline
    \textbf{Lin} & $0.13$ \\
    \hline
    \end{tabular}
    \caption{Correlation coefficients for each knowledge and corpus-based similarity measure.}
    \label{sem_kb_correlationCoefficient}
\centering
\end{small}
\end{table}

Of the next class of methods which use both \textit{WordNet} and statistical corpora and Information Content (IC), Jiang \& Conrath (JCH) has by far the highest precision and correlation coefficient. Its lower recall scores are indicative of the cost of increased ability to score a student response closer to the gold standard. Since both the LIN and JCH measurements attempt to decrease the coarseness of the Resnik measurement, it appears that on this dataset and for the application of ASAG, JCH is more effective at increasing precision at a cost of recall, whereas Lin augments all scores minimally over the Resnik.

All of the measures have a high correlation among themselves, indicating unsurprisingly that each is extracting similar information from the student responses. However, the lowest correlations are between WP and JCH which are in many ways the best methods from their respective measurement categories. This means these two are likely the best candidates for features in a machine learning pipeline.

$H_{2}$ states that a $10\%$ gain should be seen by using three model answers instead of one. This hypothesis is drawn from the lexical structure experiments which showed almost $10\%$ gains by using three model answers. The results are displayed in Fig. \ref{sem_kbAccuracyScores_ma3} which provides enough information to reject the hypothesis. Gains were seen, however, these are in the range of $4$ to $8$ percentage points, depend on the measurement, and improve recall scores much more than precision. The latter makes sense because having multiple model answers provides broader coverage of possible correct response formulations which, in turn, allows models to better match correct student responses than when there is only one source of truth.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/04_semantics/kb/class6_accuracy_scores_ma3.png}
	\caption{Accuracy scores for Shortest Path (SP), Leacock \& Chodorow (LCH), Wu \& Palmer (WP), Resnik (RES), Jiang \& Conrath (JCH) and Lin (LIN). Overall, a boost is obtained by using three Model Answers (MA=3), however, it was less than the hypothesized $10\%$ and had a positive impact much more on recall than on precision}
	\label{sem_kbAccuracyScores_ma3}
\end{figure}


Finally, upon inspecting some the scored results, it is apparent that the higher scores are due to these methods being able to pick up more detail from synonyms than approaches examined up to this point, providing no reason to reject $H_{3}$. Fig. \ref{sem_synonymExample} demonstrates a situation, which receives a higher score using this approach, precisely because synonyms and related words are being taken into account.

\begin{figure}
	\centering
		\includegraphics[scale=0.54]{images/05_experiments/04_semantics/kb/jch_synonyms.png}
	\caption{Example of a predicted score from the dataset using the Jiang and Conrath method. \textbf{(A)} shows that the phrases \textit{Extra space} and \textit{more memory} are detected as related as well as the words \textit{back} and \textit{previous}, which all then contribute to the predicted score.}
	\label{sem_synonymExample}
\end{figure}


\paragraph{TES.II: Time and Space Complexity}

These methods are computationally more complex than other techniques previously explored. Each word in the model answer $m$ must be compared with every word in the student response $n$ which is an $O(m\cot n)$ complexity. However, it doesn't stop there. For each word-pair formed in this process, a synset for each word must be found in \textit{WordNet} and then for every sense in each synset the two senses which are most similar are taken and their similarity score is calculated. This results in the algorithm running in the higher polynomial time of $O(n^4)$, which means it is slower than other methods seen up to now, although it is still a polynomial time algorithm and useable for automatic scoring of thousands of student responses and, consequently, practical.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

This approach requires only that a teacher enter in one or more model answers, no knowledge expertise is required beyond the subject matter of the course the teacher is creating. Therefore, this approach to extracting meaning can be considered very simple from a teacher's perspective.

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

This method does not provide an effective means of scoring feedback other than the ability to save a map of highest similarity word correlations between student response and model answer. This could then be color-coded to provide teachers insight into how a particular student response received its automatic score, however, this would require some training on the part of teachers and students to understand the maps and it is further not apparent that this information is helpful beyond providing more transparency into the inner workings of the scoring algorithm.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

Fig. \ref{sem_kbPassFailAccuracyScores} shows the results of the experiments run for the pass or fail automatic grading.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/04_semantics/kb/class2_accuracy_scores.png}
	\caption{Pass/Fail Accuracy Scores for Shortest Path (SP), Leacock \& Chodorow (LCH), Wu \& Palmer (WP), Resnik (RES), Jiang \& Conrath (JCH) and Lin (LIN).}
	\label{sem_kbPassFailAccuracyScores}
\end{figure}

For self-study courses, using the WP measure would effectively mean that $7$ times out of $10$ when a student response is correct that it also receives a passing grade. It also means that $7$ times out of $10$ when a student response is incorrect that the automatic scorer assigns it a failing grade. This alone is significantly better than the regular expression approach and a large step forwards in terms of the user experience, although still probably somewhat frustrating from a user perspective when implemented in an online MOOC due to the false negatives.

\paragraph{Effectiveness Across Languages}

As discussed in the introduction to this experiment, these techniques are largely not applicable to other languages beyond English and Spanish. However, because NLTK comes with a Spanish version of \textit{WordNet}, the tests were run for all measurements which do not require Information Content, that is the SP, LCH, and WP measurements. The results are abysmal and shown in Tab .\ref{sem_kbSpanishLanguageScores} in the appendix. They indicate how vitally important the extent, size and professional quality of human created knowledge bases are when it comes to applying these techniques to identify semantic content. It also shows that for practical purposes a Spanish language version is not feasible.

\subsubsection{Conclusion}

The bottom line is that this experiment shows that the Wu \& Palmer and Jiang \& Conrath measurements are most different from one another and most effective at extracting meaning from words in sentences on this dataset. These methods are not broadly applicable to languages outside of English, however, they can be used to automatically grade an English language ASAG module. Finally, as seen in previous experiments, using three model answers instead of one provided a significant $4-8\%$ boost in precision and recall scores. 


%==========================================
% Next: Word Embeddings
%==========================================


\subsection{Exp. 2: Word Embeddings} \label{Subsection_Word_Embeddings}

\subsubsection{Exp. 2: Background, Reasoning and Hypotheses}

In the previous experiment, it was observed that analyzing the semantics of words using knowledge and corpus-based approaches can provide significant performance gains over simple orthographic approaches in which only the surface form of the word is taken into account. However, there are several disadvantages of using knowledge bases, primarly the fact that they depend heavily on the quality and size of the human created knowledge base. This was demonstrated by the poor performance of Spanish \textit{WordNet} using the same methods as the English \textit{WordNet} in the previous experiment. Therefore, this set of experiments analyzes an approach using word embeddings for the task of automatic short answer grading. The goal is to compare two different word embedding models \textit{Word2Vec} and \textit{FastText} to determine which performs best at capturing the underlying semantics of words. The experiments look at the effects of \textit{n}-gram sizes, number of model answers, length penalization. They also make model comparison across languages. A final objective of these experiments is to determine how well word embeddings perform compared to knowledge-based approaches and what, if any, significant differences or contributions they provide to gaining semantic information from \textit{(model-answer, student-response)} pairs.

The preprocessing techniques performed are whitespace and case normalization, punctuation and stopword removal and lemmatization.

\paragraph{Hypotheses}

There are five hypotheses each related to \textit{n}-grams, word embedding model, length penalization, model answers, and cross language modeling capability and are derived from previous experiments. Formally, they are:

\begin{enumerate}
\item \textbf{$H_{1}$}: Increasing \textit{n}-grams will increase accuracy measures and correlation up to $n=3$.
\item \textbf{$H_{2}$}: \textit{FastText} models for learning word embeddings will capture more meaning than \textit{Word2Vec} because \textit{FastText} takes into account word morphology.  
\item \textbf{$H_{3}$}: Increasing model answers will increase accuracy measures and correlation with diminishing returns after $n>1$.
\item \textbf{$H_{4}$}: Penalizing student responses for their length will result in increased precision and lower recall scores since student responses which more closely resemble length and word semantics of the model answer will be better detected, however, this will be at the cost of failing to assign good scores to longer more elaborate student responses.
\item \textbf{$H_{5}$}: Word embedding models will have better measures than knowledge-based approaches across languages since they are not dependent on manual human construction like knowledge-based approaches. 
\end{enumerate}

An open question is whether word embeddings will perform better than knowledge based approaches at the task of ASAG.

\subsubsection{Exp. 2: Presentation of Results}

\subsubsection{Exp. 2: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

The answer the $H_{1}$ and $H_{2}$ as well as the open question of word embedding performance compared to knowledge-based approaches is shown in Fig. \ref{sem_vsmNGramModelScores}. This clearly demonstrates that \textit{FastText} models outperform \textit{Word2Vec} models on the task of ASAG, which is likely due to the fact that the \textit{FastText} method takes word morphology into account. This finding represents an extension to research by Adams et al. which compared only \textit{Word2Vec} when using word embeddings for ASAG \cite{distributed_vector_representations_2016}. It has been shown here that \textit{FastText} performs better at the ASAG task compared to \textit{Word2Vec}. 

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/04_semantics/vsm/ngrams_w2v_ft_accuracy_scores.png}
	\caption{Displays accuracy scores for \textit{Word2Vec (w2v)} and \textit{FastText (ft)} models for \textit{n}-gram sizes from $n=1$ to $n=3$. At every level \textit{FastText} outperforms \textit{Word2Vec} and using more \textit{n}-grams increases both precision and recall.}
	\label{sem_vsmNGramModelScores}
\end{figure}

Previous experiments in this thesis have shown that increasing the number of model answers significantly increases the performance of a given technique at ASAG, however, with diminishing returns after $n>1$ for each new model answer added. $H_{3}$ explicity stated an expectation that this behavior would continue for word embeddings and Fig. \ref{sem_vsmModelAnswerScores} demonstrates that there is no evidence to reject $H_{3}$.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/04_semantics/vsm/ma_w2v_ft.png}
	\caption{Accuracy Scores for using multiple model answers (MA) \textit{Word2Vec (w2v)} embeddings and \textit{FastText (ft)}embeddings. This demonstrates that increasing model answers improves ASAG performance significantly, albeit with diminishing returns.}
	\label{sem_vsmModelAnswerScores}
\end{figure}

Both of these experiment results provide evidence that word embeddings perform better than knowledge and corpus-based approaches at modeling semantics and using semantic understanding for the task of ASAG. This provides an answer to the open question posed at the outset of the experiment, namely, it is the case that word embeddings do outperform knowledge-based approaches for the ASAG task.

For length penalization, the experiment results overwhelming demonstrate that $H_{4}$ should be rejected. Penalizing student responses for containing excess words dropped every scoring performance metric significantly. For instance, precision, recall and R scores all fell off precipitiously, to around $52.5\%$, $19.0\%$ and $0.10$ for \textit{FastText} using the $n=3$ \textit{n}-grams. These same patterns were observed for \textit{Word2Vec} as well. The question is then, why was this pattern observed? One would expect that longer student responses which do not contain words related to the model answer(s) could provide more opportunity for entering false or contradictory information. However, after examining the annotated dataset, it appears that the vast majority of student responses which contain the extra information also receive high scores. This is typically because the extra information is neither contradictory to statements already made by the student which relate to the model answer nor false in anyway, but simply represents additional details that although irrelevant for receiving a high score, are nonetheless correct or at least simply worded or elaborated in more detail than the model answer. For instance, there are $100$ items in the dataset which have the label $extra\_info$ and of these $79$ have a score $s > 3$, this shows that almost $4$ out of every $5$ student responses which contain additional details or extra information in some way receive high scores. Therefore, at least on this dataset, it makes little sense to penalize student responses simply for length alone.  

\paragraph{TES.II: Time and Space Complexity}

Computationally, word embedding techniques are faster than the graph searches used by knowledge-based approaches because they can use efficient matrix operations to determine the similarity between word vectors as opposed to having to search throughout a graph structure to find node distances. However, word embedding models do take up a significantly larger portion of memory and although these word embeddings can be automatically trained and learned, this process requires a massive amount of text data in order to learn accurate word semantics. Neither of these limitations are concerning for Stembord, however, they are nonetheless attributes which must be taken into account when developing an ASAG solution using word embedding approaches.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

Word embedding approaches to ASAG require as little or as much effort as all approaches which only demand that a teacher write in one or more model answers.

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

Word embeddings have the same potential feedback mechanism as all approaches which map from words in a model answer to words in a student response and score based on the semantic similarity of those mappings, namely, that an interface could visually display these mappings and their associated scores. However, although this would increase transparency and understanding of the underlying scoring architecture for users of the system, it does not, in the author's opinion, provide adequate informative feedback for students wishing to understand why they received a particular score since it requires far too much knowledge about technical details.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

The best performing approach using only word embeddings for the task of pass/fail grading achieves a $74\%$ precision and $77\%$ recall, the details of various measures using different models are shown in the appendix in Tab. \ref{sem_vsmPassFailScores}. This represents an improvement over the knowledge-based approaches and means that almost $3$ out of $4$ student responses which are passed by the model should indeed pass. Further, almost $4$ out $5$ student responses which are failed are bad responses which indeed should have receiving a failing score. This approaches a level which is almost useable in Stembord without being too frustrating to users of the platform.

\paragraph{Effectiveness Across Languages}

The word embedding experiments were run for the Spanish language and showed significant improvements over the knowledge-based approaches. However, they did perform worse than the English language word embedding models and there could be numerous reasons for this. The first, comes from the overall lower performance and robustness of NLP tooling in languages other than English for all of the preprocessing tasks. The second, is that the author is not a native speaker of Spanish yet the original dataset was translated from English into Spanish using Google Translate followed by the author correcting some mistakes on his own. This allows errors to enter both from Google and from the human translator. In order to control for all of these variables in the future, the actual percentage performance differences in each NLP tool per language such as POS-Taggers, dependency parsers, etc. would need to be measured and a dataset of questions and model answers would need to be created by a native speaker in the given non-English language. Then the students would need to also be native speakers and write their responses in the non-English language. This is important information to know going forward, since when Stembord decides to implement multiple languages, these factors will have to be controlled for.

\subsubsection{Conclusion}

The key insight obtained in this experiment is that word embeddings outperform knowledge-based approaches at extracting semantic meaning from text and that, of the word embedding approaches, \textit{FastText} performs significantly better than \textit{Word2Vec} for the task of ASAG. Additionally, it has been reiteratively confirmed that using \textit{n}-grams of up to size $n=3$ and increasing model answers are both ways to improve the performance of unsupervised automatic scoring by providing broader coverage of the semantic space. Finally, a key observation is that length penalization is detrimental to automatic scoring performance since it penalizes too much for extra information provided in student responses and this information, at least on this dataset, has a high probability of not containing any errors or reasons for which a human grader would assign a lower score.