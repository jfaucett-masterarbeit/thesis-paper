\section{Pattern Matching}

As discussed in the introductory chapter, the idea of using patterns to find concepts in student responses is one of the oldest in ASAG. In fact, at least one LMS software product on the market, Moodle, has ASAG in the product offering \cite{moodle_asag_2018}. In the ASAG module in Moodle, teachers are allowed to create a list of regular expressions per question which are then matched against each students' response. Teachers can additionally set weights for the grade a student should receive based on a particular match, for instance, a question item may have three regular expressions associated with it such as \textit{``fuel*oxygen''} with a score of $100\%$ and \textit{``*fuel*''} with a score of $50\%$ and \textit{``*oxygen*''} with a score of $50\%$. Given two student responses ``fuel and oxygen'' and ``It contains oxygen'', the former would receive $100\%$ of the points for that item whereas the later only $50\%$ \cite{moodle_asag_2018}.

Since pattern or concept matching is one of the well-established techniques developed in ASAG and it is a solution already on the market, it was decided to start the research with an evaluation of this technique in order to set a baseline for further evaluations.

\subsection[Experiment (Exp. 1): Teacher Created Patterns]{Experiment 1: Teacher Created Patterns}

\subsubsection{Background, Reasoning and Hypotheses}

The goal of this experiment is to evaluate the effectiveness of teacher created patterns in the form of regular expressions as a means for allowing professors to automatically grade students' short answer responses. Since this method is already being used in at least one other LMS software product on the market, the results can serve to illuminate key problem areas in an ASAG approach in real-world software products. This experiment probes the problem space in order to hopefully uncover broad insights, key problems and to obtain a rough idea of what a market solution for ASAG is capable of achieving.

Version 1.0 of the Mohler dataset is used as it contains 21 instead of 80 unique question prompts. Each of these unique question prompts was extracted and entered as rows in a CSV file which contained two additional columns for allowing test participants to enter in a list of regular expressions for matching against student responses. Participants received this file and were instructed to fill it out with regular expressions to match potential student responses to model questions. An excerpt from one of the participants completed CSV files is shown in Fig. \ref{Regex_Exp_example_response}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/01_regex_experiments/regex_test_file3.png}
	\caption{Respondent CSV File: notice that in the \textit{Full Credit Matches} column, as instructed, the respondent has created hash maps containing the name of a concept as the key, for example, \textit{name\_of\_function}, and a list of patterns (regular expressions) to match for that concept as the value.}
	\label{Regex_Exp_example_response}
\end{figure}

This CSV file together with a short description of how to fill in the empty cells for \textit{Full Credit Regex} and \textit{Partial Credit Regex}, as well as a link to a tutorial on how to create regular expressions to match student responses, was sent to a software developer and a high school teacher. The author of this paper also filled out one of the CSV files, making the sample size $n=3$.\footnote{Clearly, this is too small a sample size to make any definitive assertions, however, the goal is to gain insight and understanding and it is hoped that $n=3$ is enough to achieve that end.} Additionally, after running all the experiments the author also attempted to squeeze as much accuracy as possible from the technique by filling out a fourth CSV file, this took many hours over the course of several days, was done by looking at student responses, and had purely the aim to see what the maximum performance was that this technique could achieve. This last attempt is referred to as \textit{SE1\_ext} (Software Engineer One Extended) throughout this experiment, because the author is a software engineer and the first test participant.

After the respondents returned the filled in CSV files, these were evaluated by using a function, the algorithm of which is shown in the algorithms appendix at Alg. \ref{Experiments_Regex_Exp_alg1}. This algorithm iterates over all $630$ student responses in the Mohler v1.0 dataset and scores each response by using the appropriate regular expressions for that question item in the respondents CSV file.

The maximum score obtainable in Alg. \ref{Experiments_Regex_Exp_alg1} is $1.0$ when all regular expression concepts are matched. Conversely, the minimum possible score is $0.0$ which occurs when no match can be found. If, after running all the full credit matching regular expressions, the student doesn't have a perfect score, then the partial credit regular expressions are used to check if that student can receive extra points for matching partial ideas and concepts for which the regular expressions are trying to check. For example, if a student enters \textit{``functions have a name''} as a response to the prompt \textit{``What does a function signature included?''}, the full credit regular expressions might search for both functions having a name as well as parameters. None of these will match. Therefore, the student's score is $0.0$ after a full match search is complete. However, this also means that if the teacher has created partial credit regular expressions they will then be searched for a match using the same process. In this example, perhaps the teacher has created a regular expression of the form \textit{functions(.*)name} which will match and boost the student's overall score by the amount of partial credit weight the teacher has chosen. In all the experiments, the partial credit weight used is $0.5$, which is exactly one-half of the full credit value.

\paragraph{Hypotheses}

Overall, a primary goal was to assess how long it took a teacher to create regular expressions for a question item, and what accuracy could be expected from this approach under unsupervised situations i.e. when the teacher cannot view student responses but must imagine how they might potentially look.  Much like the hand-grading task, this experiment is designed mainly as a baseline for evaluating later experiments. Both the author of this paper and the software developer test participant have intimate knowledge of how regular expressions work and so it was hypothesized that they would have a distinct advantage over the high school teacher and would likely have higher accuracy and correlation scores because of this.

A second and related goal was to determine what the maximum performance possible is given a supervised situation i.e. where the teacher can look at and inspect student responses. Being able to look at student responses should increase the accuracy measurements, since the teacher can directly see the answers which are correct and write patterns to match for them.  

An additional expectation going into this experiment was that accuracy of this method would heavily depend on the quality and robustness of the regular expressions. For instance, if a student response contains ``types of arguments'' and the only regular expression is ``types(.*)parameters?'' then that regular expression will not match the student response. In this case, although the response is correct and should consequently receive a high score it will receive a low score or fail instead. These ideas are formalized in the following hypotheses.

\begin{enumerate}
\item \textbf{$H_{1}$}: People who already know how regular expressions work will create regular expressions with higher precision and recall scores when matching against student response items than those who have to learn regular expressions for the first time in order to automatically grade responses.
\item \textbf{$H_{2}$}: Creating patterns in tandem with viewing many student responses will increase accuracy measurements.
\item \textbf{$H_{3}$}: Precision scores will be relatively high related to other scores because when a pattern matches it will tend to correctly identify a concept, however, due to the variety of language, these patterns should not match very often and consequently the recall and accuracy scores should be lower than precision.
\end{enumerate}

\subsubsection{Exp. 1: Presentation of Results}

Each respondent self-reported that it took them approximately one hour to create the regular expressions for each of the 21 question items. This means that, roughly speaking, three minutes per question were required to create a set of regular expressions for a given question prompt. The high school teacher reported that he required an additional hour to go through the regular expressions tutorial and learn the fundamentals of how regular expressions work. This means that for this sample and a given 5 question quiz a teacher would likely need around 15 minutes to build the regular expression pattern matchers, which seems reasonable for low-stakes tasks but is clearly an additional burden compared to solely writing model answers. Additionally, teachers unaware of how regular expressions work would need time to learn them, the teacher in this study needed one hour to learn enough in order to create matchers for 21 questions.

\subsubsection{Exp. 1: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

The accuracy measures for each of the three experiment participants are given in Fig. \ref{Regexp_accuracyScores}. The rightmost and fourth entry includes the author's hand-crafted time extensive version \textit{SE1\_ext}, for those interested in more statistical details in tabular format see Tab. \ref{Regexp_respondentScoresTable} in the appendix. The other important measurements, namely, the R, Kappa and MAE are given in Fig. \ref{Regexp_correlationScores}. 

\begin{figure}
	\centering
		\includegraphics[scale=0.38]{images/05_experiments/01_regex_experiments/accuracy_summary_per_respondent2.png}
	\caption{The image shows the precision, recall and F1 scores per respondent for 6-class classification. SE1 is the author of this paper who is a software engineer, SE2 is another software engineer, HST is a high school teacher, and SE1\_ext is the version where this paper's author spent hours over the course of multiple days hand-crafting patterns by looking at student responses in order to get the best performance possible. The SE2 scores are lower than those of HST, providing evidence against $H_{1}$.}
	\label{Regexp_accuracyScores}
\end{figure}


\begin{figure}
	\centering
		\includegraphics[scale=0.52]{images/05_experiments/01_regex_experiments/correlations_per_respondent.png}
	\caption{Shows the R, Kappa, and MAE scores for each participant. Kappa shows very little association, R is around 35 for the best performers and the MAE is around 3. All of these indicate a lot of room for improvement. When comparing the best performing entry (the hand-crafted time extensive solution \textbf{SE1\_ext}) to the maximums calculated after hand-grading the dataset there is still a lot of room for improvement.}
	\label{Regexp_correlationScores}
\end{figure}

Probably the most surprising result is that the high school teacher's pattern had better performance than those of one of the software engineers and were on par with those of the other. This provides direct counter evidence for the initial hypothesis that those already acquainted with regular expressions would build better regular expression matchers than those who first had to learn what regular expressions are and how they work before constructing matchers. This is significant in that it shows that after about one hour of training this high school teacher was able to build matchers on par with two software engineers well accustomed to using regular expressions in their daily work and says something about the relative ease of learning and applying regular expressions.

Far less suprising and providing no reason to reject $H_{2}$, is the fact that when the author built patterns in tandem with viewing student responses the accuract measurements increased substantially, albeit with an enormous amount of extra time and effort.

As can be seen in Fig. \ref{Regexp_accuracyScores} recall is quite low, indicating that the regular expressions are assigning many failing scores to correct responses. This is due, in some part, to the way the regular expressions matching implementation works and is consistent with the expectations made by $H_{3}$. If a teacher only has one concept and a list of regular expressions which can match for that concept, for example, the concept \textit{function has a name and parameters}, then either the student responses match or they do not thus the final score can only be $1$ or $0$ unless the teacher has also added partial credit regular expressions, in which case a score could also possibly achieve a $0.5$. Note that these are only three possible scores - before scaling for evaluation - yet for any given question the human graded scores for student responses range freely from $0$ to $5$ over $6$ classes ($0-5$ in steps of $1$). This creates a lot of space for error when running comparisons but might not necessarily be indicative of a poor underlying method for scoring student responses in general. However, this difficulty of finely grading responses is part of any method which tries to grade based on a list of concepts on an all or nothing basis. Looking at the binary classification problem mitigates against some of these errors and is examined shortly.   

As for the measurements, precision is quite high, because when an expression matches it tends to be a good indicator that the underlying concept is being expressed in the student response. This is also consistent with $H_{2}$. However, recall is quite low and upon examining the results there is one main reasons for this which is shown in Fig. \ref{Regexp_typicalExample_miss}. The figure shows a typical example for which the regular expression method completely missed a correct response due to its inability to generalize beyond its fixed set of patterns. In this instance, the human grader gave the response a perfect score but the automated regular expression approach missed, falsely giving a failing grade of zero.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/01_regex_experiments/typical_example_of_miss2.png}
	\caption{Example illustrating the overall brittleness of the regular expression approach due to its full reliance on human created patterns. \textbf{(A)} the correct concept should have something to do with unlimited or very many. \textbf{(B)} The student response can be inferred to create very many or infinite constructors by its wording i.e. it is correct. \textbf{(C)} There are regular expressions which are worded quite closely to the student's portion of the response that contains the concept, however, none of them actually match, therefore the algorithm falsely assigns a score of $0$.}
	\label{Regexp_typicalExample_miss}
\end{figure}


Finally, this approach was evaluated for its merits at simple binary pass/fail scoring. The results for binary classification of pass/fail scoring are shown in Fig. \ref{Regexp_accuracyScores_binary} with additional details shown in the appendix in Tab. \ref{Regexp_respondentScoresTable_binary}. One can see that the best performing approach \textbf{SE1\_ext} required many hours of hand-crafted regular expressions and represents the maximum achievable performance of regular expression pass / fail ability for the dataset. As in Fig. \ref{Regexp_correlationScores}, recall is still quite low, which would most certainly lead to frustration by end-users of an ASAG system graded with this approach. For instance, in the best case hand-tweaked scenario $2$ out of every $5$ correct student responses are misclassified as failures. Still, when this approach does label a student response as passing it is correct almost $9$ times out of $10$.

\begin{figure}
	\centering
		\includegraphics[scale=0.38]{images/05_experiments/01_regex_experiments/accuracy_summary_per_respondent_pass_fail2.png}
	\caption{Precision, recall and F1 scores per respondent for the task of binary classification. The precision is relatively good - over 85\% in all cases, however, this method classifies roughly 7 out of 10 answers which are correct as failing. This would be extremely frustrating for students in an online course. Even the hand-crafted elite version fails on 2 out of 5 correct student responses - though when it does decide to pass a student response it is correct in doing so 9 out of 10 times.}
	\label{Regexp_accuracyScores_binary}
\end{figure}


Overall, these numbers provide solid baselines and an interesting perspective on a current system available on the market for ASAG. If these results were to hold up consistently given larger test subjects $n$ then one could say that the current software is probably quite frustrating and not very effective at ASAG for its users. 

\paragraph{TES.II: Time and Space Complexity}

The algorithm shown Alg. \ref{Experiments_Regex_Exp_alg1} in the appendix runs in $\mathcal{O}(n^{2})$ time, whereas the inner iteration, which is the call to grade a single question/response item pair runs in $\mathcal{O}(n)$, where $n$ is the number of patterns the teacher has defined. More precisely, this means the code for grading a single response item has a quadratic time complexity of $\mathcal{O}(n^{2})$ in general terms, or $\mathcal{O}(f\cdot n + p\cdot n)$ where $f$ is the number of full credit regular expressions, $p$ is the number of partial credit regular expressions and $n$ is the length of the given regular expression. From a space complexity perspective, this technique only requires the memory necessary to store the set of regular expressions and the student response and can, therefore, be considered quite efficicent and practical easily being able to run even on old hardware.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

This approach requires that teachers and other not necessarily technical persons learn technical details about regular expressions and the intricacies of how they work in order to create matchers against potential student responses. The overall effort seems minimal, at least in this tiny sample - one hour to learn how regular expressions function - but it could be a deterrent and impediment to teacher's wanting to automate the process of grading themselves, since they may not have the hour or determination to devote to the task and learning an entirely new subject.

Further, this method does automate the short answer grading process to a large extent, although not completely. A teacher must spend time and effort creating a set of regular expressions, which in the case of this study took on average about 3 minutes per question. However, once the teacher has gone through the effort to create regular expressions and assign a weight for partial credit, the teacher does not need to concern himself further with that question item. Additionally, any student responses for the question item can be automatically scored without human interaction for as long as students are taking that particular quiz.

However, it is very important to note that for the unsupervised approach the results are essentially abysmal, barely better than random chance or the lower bounds that were established in section \ref{Upper_and_Lower_Bounds}. Only for the supervised approach, which took an order of magnitude more time ($10^{0} $ vs $10^{1}$), and consequently provides a very negative user experience, did the results begin approaching reasonably decent performance.

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

Providing formative feedback is probably the most shining characteristic of the pattern matching based approach. Because regular expressions match a substring within a text sequence, each match can store a reference to the start and end indices of the matching substring within the student response. This makes the approach highly transparent for both teachers and students. As seen previously and demonstrated in Fig. \ref{Regex_Exp_example_response}, teachers create a label for each concept to be matched and a list of patterns for finding student responses which contain that concept. This allows a system to automatically show the teacher and student which concepts matched and whether the concepts received partial or full credit. A hypothetical GUI rendering of a system using this method is shown in Fig. \ref{s4_fig_formative_feedback}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/01_regex_experiments/Student_Feedback2.png}
	\caption{Rendering of an automatically graded student response after feedback has been shown.
	A user of such a system can easily and transparently observe which concepts were detected by the algorithm, how many points were received per concept and whether a concept was a complete or partial match.}
	\label{s4_fig_formative_feedback}
\end{figure}

Because indices of regular expression matches within substrings can be stored, the substring which matched a particular concept can be highlighted for the student in their response. This makes the grading and scoring very transparent. This feedback feature is available in any technique which can use both concept labels and store references to substrings which match those labels in the student response.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

This technique would work for both \textit{teacher-guided} and \textit{self-study} classrooms. A teacher would create a self-study classroom and set of lessons and quizzes. In each quiz with a short answer question item the teacher would create a set of concepts and bags of regular expressions which match for text which contains those concepts, optionally adding partial credit regular expressions. After this, the teacher could publish the classroom and any student would be automatically graded by the regular expressions when responding to one of the short answers on a quiz.

\paragraph{Effectiveness Across Languages}

This method requires that the teacher create a new set of specific regular expressions for each language ensuring that it is, in the sense of pattern reusage, not generalizable to another language. However, given any new language and a teacher who can understand that language and regular expressions, they could create the automatic grading items themselves for the course without time delays or having to train or use machine learning models. So, in principal, this method works for any language, however, it does not generalize from one language to another, that is, an entirely new set of regular expressions must be created per language.

\subsubsection{Conclusion}

Overall, the pattern matching approach backed by regular expressions is brittle and not effective at unsupervised ASAG. The measurements in the previous experiments often bordered or dipped below the lower bounds for acceptable performance on the automatic grade assignment task. Additionally, this technique requires an initial time investment by teachers to acquire enough expertise, which although not very extensive, is still a hurdle to those users. However, the regular expression technique does have two distinct advantages. The first is that it provides the potential for detailed and transparent feedback at a concept level and, the second, is that it is also extremely efficient and simple to implement. Finally, in order for this approach to be effective, it requires a large list of student responses and constructing patterns in tandem with inspection of these responses. This means that it must be supervised, which makes this method untenable for unsupervised scoring. Additionally, to achieve higher perfomance an enormous time investment on the part of teachers in pattern development is required which makes this approach not very user friendly when viewed from the holistic perspective of an entire LMS system.\footnote{It must be reiterated that these statements should be taken with a grain of salt as the sample size is $n=3$.}