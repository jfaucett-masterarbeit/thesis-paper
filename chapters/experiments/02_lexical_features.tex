\section{Lexical Structure} \label{Section_Lexical_Structure}

After examining pattern matching as an approach for ASAG, its been established to some degree what a current ASAG module in a piece of software on the market can achieve. Now its time to begin the explorative journey of extrapolating meaning from student responses using other techniques in the research literature. This is where experiments in lexical structure come in. They explore ways in which the similarity of two sentences can be assessed without knowing the semantic contents of words and without looking at the syntacitcal relationships among those words. It can be thought of as a bare bones baseline method on top of which other more sophisticated approaches can be examined.

\subsection{Exp. 1: Word Overlap Measures} \label{Subsection_Word_Overlap_Measures}

\subsubsection{Exp. 1: Background, Reasoning and Hypotheses}

The formulas for the various word overlap measurements were expressed in detail in Ch. \ref{Material_and_Method}. These methods view sentences as sets of tokens and attempt to measure how many tokens two sentences share in common in order to determine their relatedness or similarity. The reason for conducting this experiment is primarily to find out how much accuracy can be obtained from a zero-knowledge approach i.e. a method which does not require any semantic or syntactic understanding and processing. An additional goal is to evaluate the effectiveness of the variously proposed word overlap techniques and to determine which ones perform the best and why. Finally, it is hoped that examining where lexical structure techniques fail could provide hints for directions to take in semantics and syntax for additional scoring improvements.

This experiment uses the filtered version 2.0 of the Mohler dataset which contains $n=2010$ entries. The evaluation method iterates over each student response for a given model answer, performs preprocessing which includes case normalization, punctuation and stopword removal, tokenization and stemming and applies the word overlap function to automatically score the given student response and model answer pair. The word overlap function is $f(M,S)$ as explained in the problem definition outlined Ch. \ref{Motivation_and_Problem_Definition_Chapter}.

In the experiment results the jaccard coefficient, dice coefficient, cosine coefficient and the custom developed faucett coefficient are all run and compared to discover the best performing measurement for sentence similarity.

\paragraph{Hypotheses}

The first hypothesis is that this approach, much like the regular expression experiment, will have a high precision. This will be due to the fact that student responses which contain most or all of the words of the model answer will tend to be correct. However, because of the fact that a perfect score is only obtainable when the student response is exactly the same as the model answer, the second hypothesis states that the predicted scores will consistently be less than the actual scores given by human graders since noise can come in from a lot of places. Origins for noise can be words which may have the same meaning but different surface forms since this approach, for instance, views the two words \textit{parameters} and \textit{arguments} just as distinctly as the words \textit{apple} and \textit{orange}. Difficulties for this approach are expected to also arise from misspellings and different wordings between model answer and student response. Formally, these are expressed as:

\begin{enumerate}
\item \textbf{$H_{1}$}: Student responses which are correct and contain a high percentage of the words of the model answer will be correctly scored.
\item \textbf{$H_{2}$}: This method will perform poorly on correct responses which use different wordings than the model answer.
\item \textbf{$H_{3}$}: The cosine coefficient will be the best performing method at the ASAG task since other research has suggested this as well \cite{Pribadi_2017}.
\end{enumerate}

\subsubsection{Exp. 1: Presentation of Results}

\subsubsection{Exp. 1: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

The comparative accuracy measurement results of the experiments are shown in Fig. \ref{WordOverlap_accuracyScores} and Fig. \ref{WordOverlap_correlationScores}. These results show that the method with the highest precision is the jaccard, and the method with the highest recall is the custom faucett overlap technique. Striking a balance between those two are the dice and cosine coefficients of which the dice coefficient is minimally better. This does shed some light on $H_{3}$, namely, that there are trade-offs between each one of these techniques and if one needs precision jaccard might be a better choice, however, for the ASAG task in online MOOCs, which requires high recall scores, the custom faucett is probably a better choice at the cost of some precision. The reason for the higher recall score in the custom technique lies in the fact that it boosts lower word overlaps, effectively being more lenient for phrases which share fewer words. This also leads to lower precision, however, because some of those answers with lower word overlaps are simply wrong and the faucett coefficient incorrectly identifies them as being correct and grants them higher scores than they should otherwise have.

\begin{figure}
	\centering
		\includegraphics[scale=0.32]{images/05_experiments/02_lexical_structure/accuracy_summary_per_word_overlap_method.png}
	\caption{The chart compares the scores of each word overlap technique. There are trade-offs for each method, with the jaccard coefficient having the highest precision and lowest recall scores and the faucett coefficient allowing for lower precision while increasing recall. Overall, however, the recall and F1 scores of all of these methods are far too low to be effective for ASAG.}
	\label{WordOverlap_accuracyScores}
\end{figure}


\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/02_lexical_structure/correlations_per_word_overlap_method.png}
	\caption{The correlation coefficients and mean absolute errors are somewhat better than the regular expression experiment, providing a hint that the number of shared tokens between two sentences does correlate with their assigned human score.}
	\label{WordOverlap_correlationScores}
\end{figure}

Turning to $H_{1}$, the item to assess is whether questions which are correct and contain a high percentage of word overlap between model answer and student response are identified and correctly scored using the word overlap methods. The hypothesis in $H_{1}$ is that this would be the case and, indeed, as the confusion matrices in Fig. \ref{WordOverlap_h1_detectionOfCorrect} show, the majority of \textit{(model-answer, student-response)} pairs which have over $0.75\%$ overlap after running the overlap function either correctly predict the outcome score or get very close, predicting a score of $4.0$ instead of $5.0$. Therefore, there is no reason to reject $H_{1}$.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/02_lexical_structure/method_h1_cm.png}
	\caption{The coefficient methods for only those \textit{(model-answer, student-response)} pairs which have an overlap $x > 75\%$. There the methods tend to mispredict the perfect score class 5.0 as 4.0. This is due to the fact that any differences between two sentences demote a score, so even highly similar sentences will not have a perfect score unless they are identical or close to it. Still, for sentences with a high token overlap this approach seems pretty effective.}
	\label{WordOverlap_h1_detectionOfCorrect}
\end{figure}

In addition, upon inspecting the misclassifications of each of the word overlap techniques for high overlap frequencies i.e. those missclassified subportions of the problem some of which are shown in Fig. \ref{WordOverlap_h1_detectionOfCorrect}, an interesting discovery was made, namely, that false negatives are a big problem with this approach. This means $H_{2}$ is not rejected. The word overlap techniques have no concept of word similarity beyond surface orthographic form, and the low recall scores are indicative of this. One such example is displayed in Fig. \ref{WordOverlap_h2_falseNegative} and shows a typical student response illustrating this situtation.

\begin{figure}
	\centering
		\includegraphics[scale=0.54]{images/05_experiments/02_lexical_structure/word_overlap_false_neg2.png}
	\caption{\textbf{(A)} the wording of the model answer and \textbf{(B)} the corresponding and correct yet different wording of the student answer. None of the tokens in the \textit{Model} and \textit{Student Answer Set}s are shared. This is one of the fundamental problems of the Word Overlap techniques; the inability to recognize correct but differently phrased student responses.}
	\label{WordOverlap_h2_falseNegative}
\end{figure}

\paragraph{TES.II: Time and Space Complexity}

From an algorithmic standpoint word overlap methods are quite efficient, but perhaps even just as important they are extremely simple to implement. First, each sentence $S$ is converted into a list of unique tokens i.e. a set of tokens for that sentence. This can be done in $O(n)$ where $n$ is the length of the sentence in tokens. The methods additionally require calculating the intersection and/or union of these sets which runs in $O(n)$; this means that the performance of these methods is effectively linear i.e. $O(n)$ after removing constant operation costs.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

These techniques are also simple when it comes to the amount of expertise and effort demanded of teachers, especially compared to the pattern matching experiment. A teacher needs only enter a model answer and they are done. This takes mere seconds which is much less than the 3 minute average of the pattern matching approach.

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

Unfortunately for this approach, there is little way to provide diagnostic and informative feedback to users of the system as to why a student response received a lower or failing score. For diagnostic feedback, the only feasible approach would be to show the users the token sets of the model answer and student response and provide a statistic about the amount of overlap between the two. For formative feedback, the model answer could be shown to the student and teacher and then they would have to use their own reasoning to infer why the response received and high or low grade.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

The method chosen in this thesis for evaluating whether a particular technique is effective for self-study classrooms in Stembord, is to check its capabilities at all or nothing or pass/fail scoring. The results of word overlap techniques for this binary classification task are shown in Fig. \ref{WordOverlap_binaryClassification}.

\begin{figure}
	\centering
		\includegraphics[scale=0.32]{images/05_experiments/02_lexical_structure/accuracy_summary_per_word_overlap_method_binary.png}
	\caption{For self-study courses which use a pass/fail scoring architecture the word overlap methods are
    decent, although still probably too frustrating to be usable. For instance, the best performing faucett approach fails 1 out of every 2 correct student responses and when it passes a student response that response is only correct 3 out of 4 times.}
	\label{WordOverlap_binaryClassification}
\end{figure}

Fig. \ref{WordOverlap_binaryClassification} shows that word overlap techniques have some benefits for self-study classrooms over the pattern matching technique (when unsupervised), since word overlap techniques have much higher recall scores which consequently means less frustration for end-users. This is potentially interesting information for current implementers of software systems such as Moodle which have pattern matching implementations.

\paragraph{Effectiveness Across Languages}

One of the shining characteristics of the word overlap approach is that it generalizes very well to any number of languages, if it was effective at the general ASAG task this would be of great benefit. Essentially, the jaccard, cosine, faucett or dice function can be programmed once and applied to any pair of sentences regardless of the language of origin and give comparable results. For those interested in the results of this approach applied to the German and Spanish versions of the dataset see Tab. \ref{WordOverlap_languageScores_appendix} in the tables appendix.

\subsubsection{Conclusion}

There are several key insights gained from the word overlap experiments. The first, is that these approaches do fairly well at scoring \textit{(model-answer, student-response)} pairs which have a lot of shared words and are correct. They model this situation effectively except for special cases where many words are shared but the student response is still wrong. The second key insight is that these approaches utterly fail when students use synonyms or other phrasing in correct responses since this leads to low or nonextant word overlap. Fundamentally, which such low recall scores the biggest problem are false negatives for word overlap techniques, and in the context of an online LMS with a heavy focus on the user experience this is simply not acceptable because the user frustration levels would be far too high, one need only imagine answering a question correctly and being told one is wrong $3$ out of $5$ times to see this.

Finally, for the task of ASAG and on this dataset, details about the trade-offs between the various coefficients were determined with jaccard being the best choice for higher precision modeling and faucett the best choice for higher recalls. This precision/recall trade-off is also broadly interpreted as grading leniency for the overlap techniques. This information could be useful to other domains beyond ASAG, for instance, the more general task of short text similarity (STS).   

%==========================================
% Next: N-Gram Comparisons
%==========================================

\subsection{Exp. 2: N-gram Comparisons}

There are two other well-known lexical structure approaches in the natural language processing literature. These are using \textit{n}-grams to incorporate phrasal similarities beyond simple word-to-word comparisons as well as bag-of-words (BoW) vectors which allow for counting the numbers of words or using various normalization techniques to weight the importance of certain words differently. Both are these approaches were examined and experiments were run for each, however, the results and conclusions are quite similar for both, therefore, this section only discusses the \textit{n}-gram experiment since it highlights all the same points as the BoW experiment and provides some additional insights into the problem space over BoW.

\subsubsection{Exp. 2: Background, Reasoning and Hypotheses}

The \textit{n}-gram comparison experiment approaches word overlap and surface structure from a slightly different angle than the coefficient equations based on set theory from the previous experiment. That word overlap experiment demonstrates that synonyms cannot be detected well at a surface level. Using \textit{n}-grams should not be able to alleviate this problem. However, because \textit{n}-grams take into account some minimal ordering between words - at least in consecutive sequences of two, three and four tokens - they should potentially perform better at automatic grade assignment for \textit{(model-answer, student-response)} pairs by both taking into account some degree of word order and by looking specifically at word phrases to gain more detailed insight than just words alone in isolation. This last bit of reasoning is founded on the fact that \textit{n}-grams take into account more than one word in a sequence and should, therefore, be able to to better detect common phrasing between sentences and not be as confused by student responses which simply contain some correct words. 

This experiment attempts to discover which \textit{n} is best when using \textit{n}-gram overlap sequences for ASAG. It is an extension of the previous experiment and also explores whether having additional model answers is helpful, and if so, to what extent. The first hypothesis made for this experiment is that having more model answers should increase performance, especially, since multiple model answers could use synonyms and various other wordings and phrases which, it is hypothesized, can help alleviate the major weak point of the surface structure approaches in general. A second hypothesis states that the increase in performance from looking at similarities across \textit{n}-grams will fade away as \textit{n}-grams with $n > 3$ are added. This hypothesis is based off of two observations: 1) that after preprocessing removes punctuation and function words it becomes much less likely that \textit{n}-grams will be shared between two sentences making overlaping sequences of $n > 3$ extremely rare and 2) other research has shown $n < 4$ to be an effective number for other related \textit{n}-gram measurement tasks \cite{bleu_score_2001}. Finally, it is hypothesized that using \textit{n}-grams will be more effective at modeling \textit{(model-question, student-reponse)} pairs in which word-order is important than the other word overlap approaches, simply because \textit{n}-grams store word order to some extent and do not view sentences as sets of tokens hence throwing information such as multiple word occurance away, instead they use all the information within a sentence. 

These notions are formalized in the following hypotheses:

\begin{enumerate}
\item \textbf{$H_{1}$}: Having more model answers will increase the scoring accuracy of ASAG using \textit{n}-grams.
\item \textbf{$H_{2}$}: Using \textit{n}-grams for $n>3$ will provide little or no increase in performance.
\item \textbf{$H_{3}$}: Using \textit{n}-grams models automatically scores word order better than word overlap techniques which use sets.
\end{enumerate}

The scoring function for the \textit{n}-gram evaluation of a single \textit{(model-answer, student-response)} pair is given in Eq. \ref{ngrams_ScoringEquation}.

\begin{equation}
S(x,w) = \min{( \sum_{i=1}^{\lvert x \rvert} x_{i}^{w_{i}}, 1.0)}	
\label{ngrams_ScoringEquation}
\end{equation}

where $x$ is a vector of fractional values representing the ratio of \textit{n}-gram overlaps between the two sentences and the \textit{i}-th index reprents the size of the \textit{n}-grams. For instance, $x_{1}$ is the unigram overlap ratio, $x_{2}$ the bigram overlap ratio, $x_{3}$ the trigram overlap ratio, etc. The variable $w$ is a vector of weights with heavier weights for \textit{n}-gram ratios of higher \textit{n}. For instance, $w_{1}$ could be $1$, $w_{2}$ perhaps $0.4$ and $w_{3}$ $0.2$ (note that values approaching zero represent \textit{heavier} weights since the \textit{n}-gram ratios in $x$ are all fractional values). Finally, the equation takes the minimum between the summed score and $1.0$, which clips the range to $[0,1]$ while still allowing for higher \textit{n}-grams to boost the score. In the experiments the actual choice of weights was $1/n$ where $n$ is the size of the \textit{n}-gram used for the corresponding ratio value in $x$.

Preprocessing utilizes case normalization, stopword removal, punctuation removal and stemming.

\subsubsection{Exp. 2: Presentation of Results}

\subsubsection{Exp. 2: TES.I-IV}

\paragraph{TES.I: Automatic Grade Assignment}

As can be seen in Fig. \ref{ngrams_NSizeComparison}. \textit{n}-grams are much better than single word overlap techniques at ASAG scoring, representing almost a two-fold increase over the best performing word-overlap approaches. This is also the first time that an approach is signifanctly better than the lower bounds. Fig. \ref{ngrams_NSizeComparison} also demonstrates that increasing $n$ as hypothesized, improves grading performance, however, this increase in performance peaks at $n=2$ which is one $n$ smaller than hypothesized in $H_{2}$. Further, only a very small boost from $n=2$ to $n=3$ is provided and better score modeling flatlines showing no improvement from $n=3$ to $n=4$. This provides more insight to $H_{2}$, namely, that the hypothesis was wrong that \textit{n}-grams of size $n>3$ provide little benefit and instead this starts one full \textit{n}-gram earlier. There is hardly a difference between $n=2$ and $n=3$ and one could prefer $n=2$ over the later for an  its consequent increase in algorithm speed and performance for extremely minimal decrease in recall. This would have virtually no perceivable automatic scoring difference. $H_{3}$ is not rejected, however, as the \textit{n}-gram approach models scoring better than all the word overlap methods, indicating that using all the information of a sentence and taking some small amount of order into account is a better approach for ASAG than viewing sentences as sets of tokens completely divorced of word order and word occurance counts.

\begin{figure}
	\centering
		\includegraphics[scale=0.4]{images/05_experiments/02_lexical_structure/accuracy_summary_ngrams.png}
	\caption{Notice the increase in precision and recall at the transition from unigrams (N1) to bigrams (N2). Just as important is the very minimal increase from N2 to N3, which shows that trigram modeling is roughly equivalent to bigrams for this approach and perhaps doesn't warrent the increased algorithmic complexity for the tiny gain in score modeling.}
	\label{ngrams_NSizeComparison}
\end{figure}

Overall, not viewing sentences as sets of words but allowing for multiple word occurances and using \textit{n}-gram overlaps to model the similarity between two sentences models scoring almost three fold better than unsupervised word overlap approaches, however, it still fails at sentences for which order is important and at recognizing synonyms. The previous experiment, demonstrated an example where synonym fails and although this approach performs better than word overlap since it can draw on phrases instead of just single words, it fails ust as bad at synonymy as well. This time, however, an example of word order problems is inspected to demonstrate syntactic difficulties of the lexical based approaches. This can be seen in Fig. \ref{ngrams_wordOrderMiss}.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/02_lexical_structure/ngram_missed_answer_ordering2.png}
	\caption{The \textit{n}-gram approach falsely predicts the student response as having a perfect score, whereas the human grader gave a $2.0$. This is because almost all the created \textit{n}-grams overlap, however, the ordering between sentences is different and significant. This information is not capture by the \textit{n}-gram approach or word overlap or BoW approaches in general.}
	\label{ngrams_wordOrderMiss}
\end{figure}

Fig. \ref{ngrams_wordOrderMiss} indicates that explicitly modeling the syntactical structure and ordering of words in a sentence is necessary if such questions are to be correctly graded. This leads into the subject of the next set of experiments: syntacial structure.

\paragraph{TES.II: Time and Space Complexity}

Constructing the \textit{n}-grams for a sentence can be done in $O(n)$ linear time, since the algorithm walks through the tokens of a sentence and stores the token and the next $n-1$ tokens in a tuple and appends this tuple to a list containing all \textit{n}-grams for the sentence. Finding the overlaps between two \textit{n}-gram lists is a quadratic time $O(n^2)$ operation. Therefore, the \textit{n}-gram approach used in these experiments, like the other word overlap or surface structure approaches, is quite efficient, easy to implement and scalable for MOOCs and scenarios where even hundreds or thousands of students need to be graded in a matter of seconds.

\paragraph{TES.III: Amount of Teacher Expertise and Effort Required}

This approach requires comparable time effort from teachers to that of word overlap techniques. If, however, a more robust scoring mechanism is used such as those tested in this experiment which utilize multiple model answers, then the teacher must invest the additional time in order to write and construct additional model answers which can then account for much more variety in student responses. This additional time and effort grows linearly with the number of model answers the teacher decides to write. Fig. \ref{ngrams_multipleModelAnswers} shows how ASAG scoring improves as the model answer count $m$ progresses from $1$ to $3$; this is consistent with $H_{1}$ and due to the fact that this approach allows a student response to match against the best model answer of many, essentially increasing the acceptable phrases and constructions used in correct responses. The correlation measures which also increase with the number of model answers can be found in the appendix in Tab. \ref{ngrams_correlationScores_table}. Notice in Fig. \ref{ngrams_multipleModelAnswers} how scoring performance does not grow linearly which indicates a fast approaching trade-off equilibrium point at which the extra teacher time and effort expended is not worth the minimal boost in performance by adding a model answer.

\begin{figure}
	\centering
		\includegraphics[scale=0.38]{images/05_experiments/02_lexical_structure/model_answers_accuracy.png}
	\caption{Score modeling increases as the number of model answers (MA) grows from $1$ to $3$. Recall improves dramatically because adding more model answers allows the scoring algorithm to find correct student responses which are worded or phrased differently.}
	\label{ngrams_multipleModelAnswers}
\end{figure}

\paragraph{TES.IV: Fulfillment of Desired Characteristics for Stembord}

\paragraph{Formative Feedback}

Unfortunately, like the the other word overlap approaches, the \textit{n}-gram method does not lend itself well to informative feedback. When multiple model answers are employed a feedback mechanism could show teachers which model answer a particular student response matched with. For students, the matching model answer as well as a possible alternative could be shown in order to let the student see other potential correct approaches to the problem. However, a detailed informative feedback mechanism like that seen in the pattern matching approach is not feasible.

\paragraph{Works for Self-Study and Teacher-Guided Classrooms}

Fig. \ref{ngrams_passFailMatrix} shows the confusion matrix for pass/fail scoring of the best-performing \textit{n}-gram model (MA = 3, N=3). It has a $86\%$ precision for passing scores which means when it predicts a student response as being correct, the model is correct almost $9$ times out of $10$. Where the model performs poorly is in precisely identifying incorrect scores, there its precision is only $38\%$ indicating the in $5$ out of $8$ student responses which it classifies as failing, the student response should have actually received a passing grade. This is probably not acceptable for self-study courses and would lead to high levels of frustration among users.

\begin{figure}
	\centering
		\includegraphics[scale=0.5]{images/05_experiments/02_lexical_structure/ngrams_pass_fail_cm_bw.png}
	\caption{Notice how many correct ($1$) student responses are predicted to fail and scored with a $0$ by the \textit{n}-gram model, this would lead to high end-user frustration making it difficult to apply this approach for self-study classrooms.}
	\label{ngrams_passFailMatrix}
\end{figure}

\paragraph{Effectiveness Across Languages}

The \textit{n}-gram approach performed comparatively across both the Spanish and German language datasets although both performed worse than their English counterpart and German somewhat more so. The details can be see in appendix in Tab. \ref{ngrams_languageScores_table}. Overall, however, this indicates that a \textit{n}-gram based scoring approach could easily and effortlessly scale to cover at least all languages which use alphabets, although some considerations would likely need to be made for left to right scripts such as Arabic and a new experiment would need to be made in order to assess whether \textit{n}-grams are effective at all for non-alphabetic scripts such as Chinese or hybrid scripts such as Japanese.

\subsubsection{Conclusion}

Several aspects of the ASAG problem were discovered in the \textit{n}-gram experiment. First, bigrams perform, for all practical purposes, just as well as $n=3$ and higher \textit{n}-grams for scoring short answers and they mark a significant improvement over word overlap approaches, this is likely due to taking all tokens in the sentences into account as well as common phrase patterns and simple word ordering between model and student answers. Additionally, incrementing the model answer count provides increased ability of \textit{n}-gram approaches to score short answer responses, and this is most likely generalizable to other methods beyond \textit{n}-grams as well.

There are, however, still many ways in which \textit{n}-grams are lacking. First, they do not take enough syntax and word order into account to correctly grade short answer responses which necessitate understanding of ordering. Second, \textit{n}-grams still have no concept of meaning or synonymy beyond surface orthographic structure, which is one reason they probably showed so much improvement when using additional model answers. The first of these two problems, namely syntax, is tackled in the next set of experiments.   