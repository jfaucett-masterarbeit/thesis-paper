\newcommand{\etalchar}[1]{$^{#1}$}
\begin{thebibliography}{SGAM{\etalchar{+}}15}

\bibitem[ARK16]{distributed_vector_representations_2016}
Oliver Adams, Shourya Roy, and Raghuram Krishnapuram.
\newblock Distributed vector representations for unsupervised automatic short
  answer grading.
\newblock In {\em Proceedings of the 3rd Workshop on Natural Language
  Processing Techniques for Educational Applications}, pages 20--29. The COLING
  2016 Organizing Committee, 2016.

\bibitem[BGJM16]{fastText_2016}
Piotr Bojanowski, Edouard Grave, Armand Joulin, and Tomas Mikolov.
\newblock Enriching word vectors with subword information, 2016.

\bibitem[BGS14]{burrows_gurevych_stein_2014}
Steven Burrows, Iryna Gurevych, and Benno Stein.
\newblock The eras and trends of automatic short answer grading.
\newblock {\em International Journal of Artificial Intelligence in Education},
  25(1):60–117, 2014.

\bibitem[Bil05]{tree_edit_distance_survey_2005}
Philip Bille.
\newblock A survey on tree edit distance and related problems.
\newblock {\em Theoretical Computer Science}, 337(1-3):217–239, 2005.

\bibitem[Blo84]{bloom_1984}
Benjamin~S. Bloom.
\newblock The 2 sigma problem: The search for methods of group instruction as
  effective as one-to-one tutoring.
\newblock {\em Educational Researcher}, 13(6):4, 1984.

\bibitem[BWL99]{burstein_1996}
Jill Burstein, Susanne Wolff, and Chi Lu.
\newblock Using lexical semantic techniques to classify free-responses.
\newblock In Viegas Evelyne, editor, {\em Breadth and Depth of Semantic
  Lexicons. Text, Speech and Language Technology}, volume~10, pages 227--244.
  Springer, Dordrecht, 1999.

\bibitem[CF17]{cheng_fox_2017}
Liying Cheng and Janna~D. Fox.
\newblock {\em Assessment in the language classroom: teachers supporting
  student learning}.
\newblock Palgrave in the UK is an imprint of Macmillan Publishers Limited,
  2017.

\bibitem[CJB05]{coates_2005}
Hamish Coates, Richard James, and Gabrielle Baldwin.
\newblock A critical examination of the effects of learning management systems
  on university teaching and learning.
\newblock {\em Tertiary Education and Management}, 11(1):19--36, March 2005.

\bibitem[Coh60]{cohen_1960}
Jacob Cohen.
\newblock A coefficient of agreement for nominal scales.
\newblock {\em Educational and Psychological Measurement}, 20(1):37--46, 1960.

\bibitem[DNB{\etalchar{+}}13]{dzikovska_2013}
Myroslava Dzikovska, Rodney Nielsen, Chris Brew, Claudia Leacock, Danilo
  Giampiccolo, Luisa Bentivogli, Peter Clark, Ido Dagan, and Hoa~Trang Dang.
\newblock Semeval-2013 task 7: The joint student response analysis and 8th
  recognizing textual entailment challenge.
\newblock In {\em Second Joint Conference on Lexical and Computational
  Semantics (*SEM), Volume 2: Proceedings of the Seventh International Workshop
  on Semantic Evaluation (SemEval 2013)}, pages 263--274. Association for
  Computational Linguistics, 2013.

\bibitem[FLCP03]{fleiss_2003}
Joseph~L. Fleiss, Bruce Levin, and Myunghee Cho~Paik.
\newblock {\em Statistical Methods for Rates and Proportions}.
\newblock John Wiley and Sons, Inc, 3 edition, 2003.

\bibitem[Fou18]{django_framework}
Django~Software Foundation.
\newblock Django, 2018.

\bibitem[Fri08]{friedl_2008}
Jeffrey E.~F. Friedl.
\newblock {\em Mastering regular expressions}.
\newblock OReilly, 2008.

\bibitem[GCHO05]{graesser_chipman_haynes_olney_2005}
A.C. Graesser, P.~Chipman, B.C. Haynes, and A.~Olney.
\newblock Autotutor: An intelligent tutoring system with mixed-initiative
  dialogue.
\newblock {\em IEEE Transactions on Education}, 48(4):612–618, 2005.

\bibitem[GHW12]{gomaa_fahmy_2012}
Fahmy A.~Aly Gomaa H.~Wael.
\newblock Short answer grading using string similarity and corpus-based
  similarity.
\newblock {\em International Journal of Advanced Computer Science and
  Applications}, 3(11), 2012.

\bibitem[GPJ18]{deep_learning_for_nlp_2018}
Palash Goyal, Sumit Pandey, and Karan Jain.
\newblock {\em Deep learning for natural language processing: creating neural
  networks with Python}.
\newblock Apress, 2018.

\bibitem[Hea00]{hearst_2000}
M.A. Hearst.
\newblock The debate on automated essay grading.
\newblock {\em IEEE Intelligent Systems and their Applications}, 15(5):22–37,
  2000.

\bibitem[Hyn18]{hyndman_2018}
Brendon Hyndman.
\newblock Ten reasons teachers can struggle to use technology in the classroom,
  9 2018.

\bibitem[Inc18a]{blackboard_asag_2018}
Blackboard Inc.
\newblock Short answer questions | blackboard help, 2018.

\bibitem[Inc18b]{datacamp_2018}
DataCamp Inc.
\newblock Learn r, python and data science online | datacamp, 2018.

\bibitem[Inc18c]{hp_foundation_sas}
Kaggle Inc., 2018.

\bibitem[Inc18d]{khanacademy_2018}
Khan~Academy Inc.
\newblock {Khan Academy} free online courses, lessons and practice, 2018.

\bibitem[JC97]{jiang_conrath_similarity_1997}
Jay Jiang and David Conrath.
\newblock Semantic similarity based on corpus statistics and lexical taxonomy.
\newblock In {\em Proceedings of International Conference Research on
  Computational Linguistics}. Association for Computational Linguistics, 1997.

\bibitem[JM09]{jordan_mitchell_2009}
Sally Jordan and Tom Mitchell.
\newblock e-assessment for learning? the potential of short-answer free-text
  questions with tailored feedback.
\newblock {\em British Journal of Educational Technology}, 40(2):371–385,
  2009.

\bibitem[JT16]{jobbitt_donaldsen_heit_2015}
Heit~Jamey Jobbitt~Todd, Donaldson~Robin.
\newblock The impact of automated assessment on student outcomes: A hangkuk
  university case study, 2016.

\bibitem[Jur18]{SLPv3}
Dan Jurafsky.
\newblock Regular expressions, text normalization, edit distance, 2018.

\bibitem[Koh99]{kohn_1999}
Alfie Kohn.
\newblock From degrading to de-grading.
\newblock {\em High School Magazine}, March 1999.

\bibitem[KSKW15]{wmd_2015}
Matt~J. Kusner, Yu~Sun, Nicholas~I. Kolkin, and Kilian~Q. Weinberger.
\newblock From word embeddings to document distances.
\newblock In {\em Proceedings of the 32Nd International Conference on
  International Conference on Machine Learning - Volume 37}, ICML'15, pages
  957--966. JMLR.org, 2015.

\bibitem[Kuk]{kukich_92}
Karen Kukich.
\newblock Techniques for automatically correcting words in text.

\bibitem[LC98]{leacock_and_chodorow_similarity_1998}
Claudia Leacock and Martin Chodorow.
\newblock Combining local context and wordnet similarity for word sense
  identification.
\newblock In {\em WordNet, An Electronic Lexical Database}, chapter~11, pages
  265--284. The MIT Press, 1998.

\bibitem[LS04]{leacock_2004}
Claudia Leacock and Educational~Testing Service.
\newblock Scoring free-responses automatically: A case study of a large-scale
  assessment.
\newblock {\em Examens}, 2004.

\bibitem[MBM11]{mohler_bunescu_mihalcea_2011}
Michael Mohler, Razvan. Bunescu, and Rada Mihalcea.
\newblock Learning to grade short answer questions using semantic similarity
  measures and dependency graph alignments.
\newblock In {\em Proceedings of the 49th Annual Meeting of the Association for
  Computational Linguistics: Human Language Technologies}, EdAppsNLP 05, pages
  752--762. Association for Computational Linguistics, 2011.

\bibitem[MCCD13]{word2vec_2013}
Tomas Mikolov, Kai Chen, Greg Corrado, and Jeffrey Dean.
\newblock Efficient estimation of word representations in vector space, 2013.

\bibitem[MM09]{Mohler_2009}
Michael Mohler and Rada Mihalcea.
\newblock Text-to-text semantic similarity for automatic short answer grading.
\newblock In {\em Proceedings of the 12th Conference of the European Chapter of
  the Association for Computational Linguistics}, EACL '09, pages 567--575,
  Stroudsburg, PA, USA, 2009. Association for Computational Linguistics.

\bibitem[Moo18]{moodle_asag_2018}
Moodle.
\newblock Short-answer question type - moodledocs, 2018.

\bibitem[MRBA02]{mitchell_2002}
Tom Mitchell, Terry Russell, Peter Broomhead, and Nicola Aldridge.
\newblock Towards robust computerised marking of free-text responses.
\newblock {\em Proceedings of the 6th CAA Conference}, 2002.

\bibitem[MS08]{manning_schutze_2008}
Christopher~D. Manning and Hinrich Schutze.
\newblock {\em Foundations of statistical natural language processing}.
\newblock MIT, 2008.

\bibitem[MZOK11]{Meurers_2011a}
Detmar Meurers, Ramon Ziai, Niels Ott, and Janina Kopp.
\newblock Evaluating answers to reading comprehension questions in context:
  Results for german and the role of information structure.
\newblock In {\em Proceedings of the TextInfer 2011 Workshop on Textual
  Entailment}, TIWTE '11, pages 1--9, Stroudsburg, PA, USA, 2011. Association
  for Computational Linguistics.

\bibitem[NMB13]{nkambou_2013}
Roger Nkambou, Riichiro Mizoguchi, and Jacqueline Bourdeau.
\newblock {\em Advances in Intelligent Tutoring Systems}.
\newblock Springer Berlin, 2013.

\bibitem[oT18]{germanet_2018}
University of~Tübingen.
\newblock License agreements for germanet, 2018.

\bibitem[Pag66]{page_1966}
Ellis~B Page.
\newblock The imminence of grading essays by computer.
\newblock {\em The Phi Delta Kappan}, 47(5):238–243, January 1966.

\bibitem[PAP{\etalchar{+}}17]{Pribadi_2017}
Feddy Pribadi, Teguh Adji, Adhistya Permanasari, Anggraini Mulwinda, and Aryo
  Baskoro.
\newblock Automatic short answer scoring using words overlapping methods.
\newblock In {\em AIP Conference Proceedings}. American Institute of Physics,
  2017.

\bibitem[Pea18]{pearsall_2018}
Glen Pearsall.
\newblock {\em Fast and effective assessment: how to reduce your workload
  andimprove student learning}.
\newblock ASCD, 2018.

\bibitem[Ped10]{information_content_measures_2010}
Ted Pedersen.
\newblock Information content measures of semantic similarity perform better
  without sense-tagged text.
\newblock In {\em Human Language Technologies: The 2010 Annual Conference of
  the North American Chapter of the Association for Computational Linguistics},
  pages 329--332, Stroudsburg, PA, USA, 2010. Association for Computational
  Linguistics.

\bibitem[Pla18]{elixir_language}
Plataformatec.
\newblock Elixir, 2018.

\bibitem[PRWZ01]{bleu_score_2001}
Kishore Papineni, Salim Roukos, Todd Ward, and Wei-Jing Zhu.
\newblock Bleu: a method for automatic evaluation of machine translation.
\newblock {\em Proceedings of the 40th Annual Meeting on Association for
  Computational Linguistics - ACL 02}, 2001.

\bibitem[PS05]{pulman_2005}
Stephen~G. Pulman and Jana~Z. Sukkarieh.
\newblock Automatic short answer marking.
\newblock In {\em Proceedings of the Second Workshop on Building Educational
  Applications Using NLP}, EdAppsNLP 05, pages 9--16, Stroudsburg, PA, USA,
  2005. Association for Computational Linguistics.

\bibitem[PVG{\etalchar{+}}11]{scikit_learn}
F.~Pedregosa, G.~Varoquaux, A.~Gramfort, V.~Michel, B.~Thirion, O.~Grisel,
  M.~Blondel, P.~Prettenhofer, R.~Weiss, V.~Dubourg, J.~Vanderplas, A.~Passos,
  D.~Cournapeau, M.~Brucher, M.~Perrot, and E.~Duchesnay.
\newblock Scikit-learn: Machine learning in python.
\newblock {\em Journal of Machine Learning Research}, 12:2825--2830, 2011.

\bibitem[RND15]{roy_narahari_deshmukh_2015}
Shourya Roy, Y.~Narahari, and Om~D. Deshmukh.
\newblock A perspective on computer assisted assessment techniques for short
  free-text answers.
\newblock {\em Computer Assisted Assessment. Research into E-Assessment
  Communications in Computer and Information Science}, page 96–109, 2015.

\bibitem[SGAM{\etalchar{+}}15]{tree_edit_distance_2015}
Grigori Sidorov, Helena Gomez-Adorno, Ilia Markov, David Pinto, and Nahun Loya.
\newblock Computing text similarity using tree edit distance.
\newblock {\em 2015 Annual Conference of the North American Fuzzy Information
  Processing Society (NAFIPS) held jointly with 2015 5th World Conference on
  Soft Computing (WConSC)}, 2015.

\bibitem[SSS16]{sultan_salazar_sumner_2016}
Md~Arafat Sultan, Cristobal Salazar, and Tamara Sumner.
\newblock Fast and easy short answer grading with high accuracy.
\newblock {\em Proceedings of the 2016 Conference of the North American Chapter
  of the Association for Computational Linguistics: Human Language
  Technologies}, 2016.

\bibitem[WP94]{wu_palmer_similarity_1994}
Zhibiao Wu and Martha Palmer.
\newblock Verb semantics and lexical selectio.
\newblock In {\em Proceedings of the 32nd Annual Meeting of the Association for
  Computational Linguistics}. Association for Computational Linguistics, 1994.

\bibitem[WRW07]{watson_2007}
Sunnie Lee~Watson. William R.~Watson.
\newblock An argument for clarity: what are learning management systems, what
  are they not, and what should they become?.
\newblock {\em TechTrends}, 48(4):28--34, 2007.

\bibitem[YB06]{attali_2006}
Attali Yigal and Jill Burstein.
\newblock Automated essay scoring with e-rater® v.2.
\newblock {\em Journal of Technology, Learning, and Assessment}, 4, 2006.

\bibitem[YHZ{\etalchar{+}}18]{yang_huang_zhuang_zhang_yu_2018}
Xi~Yang, Yuwei Huang, Fuzhen Zhuang, Lishan Zhang, and Shengquan Yu.
\newblock Automatic chinese short answer grading with deep autoencoder.
\newblock {\em Lecture Notes in Computer Science Artificial Intelligence in
  Education}, page 399–404, 2018.

\end{thebibliography}
