\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Automatic Short Answer Grading (ASAG)}{2}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Structure and Features}{2}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Importance and Associated Risks}{3}{subsection.1.1.2}
\contentsline {subsubsection}{Arguments for ASAG}{3}{section*.5}
\contentsline {subsubsection}{Arguments against ASAG}{4}{section*.6}
\contentsline {subsubsection}{Synthesis}{5}{section*.7}
\contentsline {subsection}{\numberline {1.1.3}History and Place in the Learning Technology Landscape}{6}{subsection.1.1.3}
\contentsline {subsubsection}{Historical Eras of ASAG}{7}{section*.8}
\contentsline {paragraph}{The Era of Concept Mapping}{7}{section*.9}
\contentsline {paragraph}{The Era of Information Extraction}{8}{section*.10}
\contentsline {paragraph}{The Era of Corpus Based Methods}{9}{section*.11}
\contentsline {paragraph}{The Era of Machine Learning}{9}{section*.12}
\contentsline {paragraph}{The Era of Evaluation}{10}{section*.13}
\contentsline {paragraph}{Summary of the historical development of ASAG}{10}{section*.14}
\contentsline {subsection}{\numberline {1.1.4}Key Problems and Issues}{10}{subsection.1.1.4}
\contentsline {subsubsection}{Text Input Correction and Normalization}{11}{section*.15}
\contentsline {subsubsection}{Natural Language Understanding}{12}{section*.16}
\contentsline {subsubsection}{Feedback Generation}{13}{section*.17}
\contentsline {subsubsection}{Item Scoring}{14}{section*.18}
\contentsline {subsubsection}{Language Independence}{15}{section*.19}
\contentsline {subsection}{\numberline {1.1.5}Current State-of-the-Art}{15}{subsection.1.1.5}
\contentsline {section}{\numberline {1.2}Learning Management Systems (LMS)}{16}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Definition and Function of LMS Software}{16}{subsection.1.2.1}
\contentsline {subsubsection}{Learning Objects}{17}{section*.20}
\contentsline {subsubsection}{ASAG's relationship and role in LMSs}{17}{section*.21}
\contentsline {subsubsection}{Reasons for LMS usage and adoption uptake}{18}{section*.22}
\contentsline {subsection}{\numberline {1.2.2}Definition and Function of Intelligent Tutoring Systems}{18}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Stembord Project}{19}{subsection.1.2.3}
\contentsline {subsubsection}{Immediate and Long-Term goals of Stembord}{19}{section*.23}
\contentsline {chapter}{\numberline {2}Motivation and Problem Definition}{21}{chapter.2}
\contentsline {section}{\numberline {2.1}Problem Definition}{21}{section.2.1}
\contentsline {section}{\numberline {2.2}Motivation}{23}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Motivation from ASAG}{23}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Motivation from Stembord}{24}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Definition Specific Motivations}{25}{subsection.2.2.3}
\contentsline {chapter}{\numberline {3}Material and Method}{26}{chapter.3}
\contentsline {section}{\numberline {3.1}Datasets}{27}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Mohler Dataset Version 1.0}{27}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Mohler Dataset Version 2.0}{29}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Annotation and Extension of Mohler Dataset Version 2.0}{30}{subsection.3.1.3}
\contentsline {subsubsection}{Discoveries made by Hand-Grading the Dataset from Scratch}{30}{section*.24}
\contentsline {paragraph}{Grader Behavior and Student Response Classes}{30}{section*.25}
\contentsline {paragraph}{The Grading Process and Score Distribution}{33}{section*.26}
\contentsline {subsubsection}{Student Response Structures}{33}{section*.27}
\contentsline {subsubsection}{Dataset Pruning}{35}{section*.28}
\contentsline {subsection}{\numberline {3.1.4}Translated Datasets}{35}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Other Datasets Considered}{37}{subsection.3.1.5}
\contentsline {section}{\numberline {3.2}Framing Performance}{38}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Lower Bounds}{38}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Upper Bounds}{38}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Methods}{40}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}General Preprocessing}{40}{subsection.3.3.1}
\contentsline {subsubsection}{Stopword Removal}{40}{section*.29}
\contentsline {subsubsection}{Case and Whitespace Normalization}{40}{section*.30}
\contentsline {subsubsection}{Stemming}{41}{section*.31}
\contentsline {subsubsection}{Lemmatization}{41}{section*.32}
\contentsline {subsubsection}{Punctuation Removal}{42}{section*.33}
\contentsline {subsubsection}{Unexplored Preprocessing Techniques}{42}{section*.34}
\contentsline {subsection}{\numberline {3.3.2}Regular Expressions}{42}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Lexical Structure}{43}{subsection.3.3.3}
\contentsline {subsubsection}{Word Overlap Equations}{44}{section*.35}
\contentsline {paragraph}{Dice Coefficient}{44}{section*.36}
\contentsline {paragraph}{Jaccard Coefficient}{44}{section*.37}
\contentsline {paragraph}{Cosine Coefficient}{44}{section*.38}
\contentsline {paragraph}{Faucett Coefficient}{45}{section*.39}
\contentsline {subsubsection}{Bag-of-Words (BoW) and N-Grams}{45}{section*.40}
\contentsline {subsection}{\numberline {3.3.4}Syntactical Structure}{45}{subsection.3.3.4}
\contentsline {subsubsection}{Edit Distance}{46}{section*.41}
\contentsline {subsubsection}{Bleu Score}{47}{section*.42}
\contentsline {subsubsection}{Tree Edit Distance}{48}{section*.43}
\contentsline {subsection}{\numberline {3.3.5}Semantics}{48}{subsection.3.3.5}
\contentsline {subsubsection}{Knowledge and Corpus Based Word Meaning}{48}{section*.44}
\contentsline {subsubsection}{Shortest Path Similarity}{50}{section*.45}
\contentsline {subsubsection}{Leacock and Chodorow Similarity}{50}{section*.46}
\contentsline {subsubsection}{Wu and Palmer Similarity}{50}{section*.47}
\contentsline {subsubsection}{WordNet and Information Content Similarity Measures}{51}{section*.48}
\contentsline {subsubsection}{Word Embeddings}{52}{section*.49}
\contentsline {paragraph}{Word Mover's Distance (WMD)}{53}{section*.50}
\contentsline {subsection}{\numberline {3.3.6}Scoring Equation}{53}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Machine Learning}{55}{subsection.3.3.7}
\contentsline {subsubsection}{Multi-layer Perceptron}{55}{section*.51}
\contentsline {subsubsection}{Logistic Regression}{56}{section*.52}
\contentsline {subsubsection}{Random Forest}{56}{section*.53}
\contentsline {section}{\numberline {3.4}Statistical Evaluation Measures}{57}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Predictions: Accuracy, Precision, Recall and F1}{57}{subsection.3.4.1}
\contentsline {paragraph}{Accuracy}{57}{section*.54}
\contentsline {paragraph}{Precision}{58}{section*.55}
\contentsline {paragraph}{Recall}{58}{section*.56}
\contentsline {paragraph}{F1 Score}{59}{section*.57}
\contentsline {subsection}{\numberline {3.4.2}Predicted Error Differences: Mean Absolute Error (MAE)}{59}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Prediction Agreement: Cohen's Kappa and Pearson's R}{59}{subsection.3.4.3}
\contentsline {paragraph}{Pearson Correlation Coefficient (PCC or Pearson's R)}{60}{section*.58}
\contentsline {paragraph}{Cohen's Kappa}{60}{section*.59}
\contentsline {section}{\numberline {3.5}Technical Evaluation Sections (TES) for Evaluating Experiment Results}{61}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}TES.I: Effectiveness at Automatic Grade Assignment}{61}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}TES.II: Time and Space Complexity}{61}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}TES.III: Amount of Teacher Expertise and Effort Required}{61}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}TES.IV: Fulfillment of Desired Characteristics for Stembord}{61}{subsection.3.5.4}
\contentsline {subsubsection}{Summary}{61}{section*.60}
\contentsline {chapter}{\numberline {4}Experiments}{62}{chapter.4}
\contentsline {section}{\numberline {4.1}Pattern Matching}{63}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Experiment (Exp. 1): Teacher Created Patterns}{63}{subsection.4.1.1}
\contentsline {subsubsection}{Background, Reasoning and Hypotheses}{63}{section*.61}
\contentsline {paragraph}{Hypotheses}{64}{section*.62}
\contentsline {subsubsection}{Exp. 1: Presentation of Results}{65}{section*.63}
\contentsline {subsubsection}{Exp. 1: TES.I-IV}{66}{section*.64}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{66}{section*.65}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{69}{section*.66}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{70}{section*.67}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{70}{section*.68}
\contentsline {paragraph}{Formative Feedback}{70}{section*.69}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{71}{section*.70}
\contentsline {paragraph}{Effectiveness Across Languages}{71}{section*.71}
\contentsline {subsubsection}{Conclusion}{71}{section*.72}
\contentsline {section}{\numberline {4.2}Lexical Structure}{73}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Exp. 1: Word Overlap Measures}{73}{subsection.4.2.1}
\contentsline {subsubsection}{Exp. 1: Background, Reasoning and Hypotheses}{73}{section*.73}
\contentsline {paragraph}{Hypotheses}{73}{section*.74}
\contentsline {subsubsection}{Exp. 1: Presentation of Results}{74}{section*.75}
\contentsline {subsubsection}{Exp. 1: TES.I-IV}{74}{section*.76}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{74}{section*.77}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{75}{section*.78}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{77}{section*.79}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{77}{section*.80}
\contentsline {paragraph}{Formative Feedback}{77}{section*.81}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{77}{section*.82}
\contentsline {paragraph}{Effectiveness Across Languages}{77}{section*.83}
\contentsline {subsubsection}{Conclusion}{78}{section*.84}
\contentsline {subsection}{\numberline {4.2.2}Exp. 2: N-gram Comparisons}{79}{subsection.4.2.2}
\contentsline {subsubsection}{Exp. 2: Background, Reasoning and Hypotheses}{79}{section*.85}
\contentsline {subsubsection}{Exp. 2: Presentation of Results}{80}{section*.86}
\contentsline {subsubsection}{Exp. 2: TES.I-IV}{80}{section*.87}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{80}{section*.88}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{82}{section*.89}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{82}{section*.90}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{83}{section*.91}
\contentsline {paragraph}{Formative Feedback}{83}{section*.92}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{83}{section*.93}
\contentsline {paragraph}{Effectiveness Across Languages}{84}{section*.94}
\contentsline {subsubsection}{Conclusion}{84}{section*.95}
\contentsline {section}{\numberline {4.3}Syntactical Structure}{85}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Exp. 1: Edit Distances and Bleu-Score}{85}{subsection.4.3.1}
\contentsline {subsubsection}{Exp. 1: Background, Reasoning and Hypotheses}{85}{section*.96}
\contentsline {paragraph}{Hypotheses}{85}{section*.97}
\contentsline {subsubsection}{Exp. 1: Presentation of Results}{86}{section*.98}
\contentsline {subsubsection}{Exp. 1: TES.I-IV}{86}{section*.99}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{86}{section*.100}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{87}{section*.101}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{88}{section*.102}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{88}{section*.103}
\contentsline {paragraph}{Effectiveness Across Languages}{88}{section*.104}
\contentsline {subsubsection}{Conclusion}{88}{section*.105}
\contentsline {section}{\numberline {4.4}Semantics}{90}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Exp. 1: Knowledge and Corpus Based Meaning}{90}{subsection.4.4.1}
\contentsline {subsubsection}{Exp. 1: Background, Reasoning and Hypotheses}{90}{section*.106}
\contentsline {paragraph}{Hypotheses}{91}{section*.107}
\contentsline {subsubsection}{Exp. 1: Presentation of Results}{91}{section*.108}
\contentsline {subsubsection}{Exp. 1: TES.I-IV}{91}{section*.109}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{91}{section*.110}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{94}{section*.111}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{94}{section*.112}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{94}{section*.113}
\contentsline {paragraph}{Formative Feedback}{94}{section*.114}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{95}{section*.115}
\contentsline {paragraph}{Effectiveness Across Languages}{95}{section*.116}
\contentsline {subsubsection}{Conclusion}{96}{section*.117}
\contentsline {subsection}{\numberline {4.4.2}Exp. 2: Word Embeddings}{96}{subsection.4.4.2}
\contentsline {subsubsection}{Exp. 2: Background, Reasoning and Hypotheses}{96}{section*.118}
\contentsline {paragraph}{Hypotheses}{96}{section*.119}
\contentsline {subsubsection}{Exp. 2: Presentation of Results}{97}{section*.120}
\contentsline {subsubsection}{Exp. 2: TES.I-IV}{97}{section*.121}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{97}{section*.122}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{99}{section*.123}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{99}{section*.124}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{100}{section*.125}
\contentsline {paragraph}{Formative Feedback}{100}{section*.126}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{100}{section*.127}
\contentsline {paragraph}{Effectiveness Across Languages}{100}{section*.128}
\contentsline {subsubsection}{Conclusion}{101}{section*.129}
\contentsline {section}{\numberline {4.5}Machine Learning}{102}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Exp. 1: Hybrid Approach with Machine Learning}{102}{subsection.4.5.1}
\contentsline {subsubsection}{Exp. 1: Background, Reasoning and Hypotheses}{102}{section*.130}
\contentsline {paragraph}{Features}{102}{section*.131}
\contentsline {subsubsection}{Exp. 1: Presentation of Results}{104}{section*.132}
\contentsline {subsubsection}{Exp. 1: TES.I-IV}{104}{section*.133}
\contentsline {paragraph}{TES.I: Automatic Grade Assignment}{104}{section*.134}
\contentsline {paragraph}{TES.II: Time and Space Complexity}{104}{section*.135}
\contentsline {paragraph}{TES.III: Amount of Teacher Expertise and Effort Required}{104}{section*.136}
\contentsline {paragraph}{TES.IV: Fulfillment of Desired Characteristics for Stembord}{105}{section*.137}
\contentsline {paragraph}{Formative Feedback}{105}{section*.138}
\contentsline {paragraph}{Works for Self-Study and Teacher-Guided Classrooms}{105}{section*.139}
\contentsline {paragraph}{Effectiveness Across Languages}{105}{section*.140}
\contentsline {subsubsection}{Conclusion}{105}{section*.141}
\contentsline {section}{\numberline {4.6}Conclusions and Research Informed Development Decisions}{106}{section.4.6}
\contentsline {chapter}{\numberline {5}Software Module Implementation}{108}{chapter.5}
\contentsline {section}{\numberline {5.1}Stembord Components}{109}{section.5.1}
\contentsline {subsubsection}{Stembord Architecture and System Design}{109}{section*.142}
\contentsline {subsubsection}{Lessons in Stembord}{110}{section*.143}
\contentsline {subsubsection}{The Node Grading Subsystem}{111}{section*.144}
\contentsline {section}{\numberline {5.2}Quiz API}{115}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Rest API Architecture}{115}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}ASAG Scorer Module}{117}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Outlook}{120}{chapter.6}
\contentsline {chapter}{References}{123}{chapter*.145}
\contentsline {chapter}{Appendix}{128}{appendix.A}
\contentsline {section}{\numberline {A.1}Tables}{129}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Datasets}{129}{subsection.A.1.1}
\contentsline {subsubsection}{Labels}{129}{section*.147}
\contentsline {subsection}{\numberline {A.1.2}Experiments}{129}{subsection.A.1.2}
\contentsline {subsubsection}{Regular Expressions}{129}{section*.148}
\contentsline {paragraph}{Experiment 1: Teacher Created Regular Expressions}{129}{section*.149}
\contentsline {subsubsection}{Lexical Structure}{130}{section*.150}
\contentsline {paragraph}{Experiment 1: Word Overlap Measures}{130}{section*.151}
\contentsline {paragraph}{Experiment 2: N-Gram Comparisons}{130}{section*.152}
\contentsline {subsubsection}{Semantics}{131}{section*.153}
\contentsline {paragraph}{Exp. 1: Knowledge and Corpus Based Meaning}{131}{section*.154}
\contentsline {paragraph}{Exp. 2: Word Embeddings}{131}{section*.155}
\contentsline {subsubsection}{Machine Learning}{132}{section*.156}
\contentsline {paragraph}{Exp. 1: Hybrid Approach with Machine Learning}{132}{section*.157}
\contentsline {section}{\numberline {A.2}Algorithms}{133}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Regular Expression Algorithm}{133}{subsection.A.2.1}
\contentsline {section}{\numberline {A.3}Miscellaneous}{134}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}Stembord Project Vision for the Future}{134}{subsection.A.3.1}
\contentsline {section}{\numberline {A.4}Glossary}{136}{section.A.4}
\contentsline {chapter}{Erkl\IeC {\"a}rung der Kandidatin / des Kandidaten}{137}{appendix.B}
