

import re

text = "The methods enqueue and dequeue are used when working with queues"

# define the rules of the language
rules = "((:?en|de)que(:?ue)?)"

# compile the rules into a FSM
regular_expression = re.compile(rules)

# apply the string as input to the FSM to discover all matching substrings
result = regular_expression.findall(text)
print(result)
# prints ['enqueue', 'dequeue']
