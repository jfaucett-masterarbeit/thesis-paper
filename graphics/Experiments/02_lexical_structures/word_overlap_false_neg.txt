

Prompt:             What is the main disadvantage of a 
                    doubly-linked list over a basic linked list? 
Score: 5.0
Pred-Score: 0.0
Model Answer:       Extra space required to store the back pointers. 
Model Answer Set:   {'space', 'requir', 'pointer', 'extra', 
                     'back', 'store'}
Student Answer:     a node in a doubly linked list takes up more 
                    memory than a node in a singly linked list. 
Student Answer Set: {'take', 'link', 'doubli', 'list', 'memori', 
                     'node', 'singl'}

